package com.wissen.interview.service;


import java.util.ArrayList;
//import org.assertj.core.util.Arrays;

import java.util.Collection;
//import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.wissen.interview.domain.AdminUser;
import com.wissen.interview.repository.AdminUserRepository;
import com.wissen.interview.security.AuthoritiesConstants;
import com.wissen.interview.web.rest.AccountResource;

@RunWith(SpringRunner.class)
public class AuthorityAssignmentServiceTest {

	@Autowired
	private AdminUserRepository adminUserRepository;
	
    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

	@Test
	@Transactional
	public void testAssignAuthority() {
		AdminUser adminUser=null;
		Collection<String>groups=new ArrayList<>();
		groups.add(AuthoritiesConstants.USER);
		String email="sam";
	
		if (email != null)
			adminUser = adminUserRepository.findByEmailId(email);
		if (adminUser != null) {
			groups.add(AuthoritiesConstants.ADMIN);
		}
		assert(groups.size()==2);
		log.debug(groups.toString());
		log.debug(email);
		
		assert(groups.contains(AuthoritiesConstants.USER));
		assert(groups.contains(AuthoritiesConstants.ADMIN));
		
	}
}
