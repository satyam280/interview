package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.SoftSkillsFeedback;
import com.wissen.interview.repository.SoftSkillsFeedbackRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.wissen.interview.domain.enumeration.CommunicationSkills;
import com.wissen.interview.domain.enumeration.MotivationLevel;
/**
 * Test class for the SoftSkillsFeedbackResource REST controller.
 *
 * @see SoftSkillsFeedbackResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class SoftSkillsFeedbackResourceIntTest {

    private static final CommunicationSkills DEFAULT_COMMUNICATION = CommunicationSkills.POOR;
    private static final CommunicationSkills UPDATED_COMMUNICATION = CommunicationSkills.SATISFACTORY;

    private static final MotivationLevel DEFAULT_MOTIVATION_LEVEL = MotivationLevel.LOW;
    private static final MotivationLevel UPDATED_MOTIVATION_LEVEL = MotivationLevel.AVERAGE;

    @Autowired
    private SoftSkillsFeedbackRepository softSkillsFeedbackRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSoftSkillsFeedbackMockMvc;

    private SoftSkillsFeedback softSkillsFeedback;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SoftSkillsFeedbackResource softSkillsFeedbackResource = new SoftSkillsFeedbackResource(softSkillsFeedbackRepository);
        this.restSoftSkillsFeedbackMockMvc = MockMvcBuilders.standaloneSetup(softSkillsFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SoftSkillsFeedback createEntity(EntityManager em) {
        SoftSkillsFeedback softSkillsFeedback = new SoftSkillsFeedback()
            .communication(DEFAULT_COMMUNICATION)
            .motivationLevel(DEFAULT_MOTIVATION_LEVEL);
        return softSkillsFeedback;
    }

    @Before
    public void initTest() {
        softSkillsFeedback = createEntity(em);
    }

    @Test
    @Transactional
    public void createSoftSkillsFeedback() throws Exception {
        int databaseSizeBeforeCreate = softSkillsFeedbackRepository.findAll().size();

        // Create the SoftSkillsFeedback
        restSoftSkillsFeedbackMockMvc.perform(post("/api/soft-skills-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(softSkillsFeedback)))
            .andExpect(status().isCreated());

        // Validate the SoftSkillsFeedback in the database
        List<SoftSkillsFeedback> softSkillsFeedbackList = softSkillsFeedbackRepository.findAll();
        assertThat(softSkillsFeedbackList).hasSize(databaseSizeBeforeCreate + 1);
        SoftSkillsFeedback testSoftSkillsFeedback = softSkillsFeedbackList.get(softSkillsFeedbackList.size() - 1);
        assertThat(testSoftSkillsFeedback.getCommunication()).isEqualTo(DEFAULT_COMMUNICATION);
        assertThat(testSoftSkillsFeedback.getMotivationLevel()).isEqualTo(DEFAULT_MOTIVATION_LEVEL);
    }

    @Test
    @Transactional
    public void createSoftSkillsFeedbackWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = softSkillsFeedbackRepository.findAll().size();

        // Create the SoftSkillsFeedback with an existing ID
        softSkillsFeedback.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSoftSkillsFeedbackMockMvc.perform(post("/api/soft-skills-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(softSkillsFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the SoftSkillsFeedback in the database
        List<SoftSkillsFeedback> softSkillsFeedbackList = softSkillsFeedbackRepository.findAll();
        assertThat(softSkillsFeedbackList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSoftSkillsFeedbacks() throws Exception {
        // Initialize the database
        softSkillsFeedbackRepository.saveAndFlush(softSkillsFeedback);

        // Get all the softSkillsFeedbackList
        restSoftSkillsFeedbackMockMvc.perform(get("/api/soft-skills-feedbacks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(softSkillsFeedback.getId().intValue())))
            .andExpect(jsonPath("$.[*].communication").value(hasItem(DEFAULT_COMMUNICATION.toString())))
            .andExpect(jsonPath("$.[*].motivationLevel").value(hasItem(DEFAULT_MOTIVATION_LEVEL.toString())));
    }
    
    @Test
    @Transactional
    public void getSoftSkillsFeedback() throws Exception {
        // Initialize the database
        softSkillsFeedbackRepository.saveAndFlush(softSkillsFeedback);

        // Get the softSkillsFeedback
        restSoftSkillsFeedbackMockMvc.perform(get("/api/soft-skills-feedbacks/{id}", softSkillsFeedback.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(softSkillsFeedback.getId().intValue()))
            .andExpect(jsonPath("$.communication").value(DEFAULT_COMMUNICATION.toString()))
            .andExpect(jsonPath("$.motivationLevel").value(DEFAULT_MOTIVATION_LEVEL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSoftSkillsFeedback() throws Exception {
        // Get the softSkillsFeedback
        restSoftSkillsFeedbackMockMvc.perform(get("/api/soft-skills-feedbacks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSoftSkillsFeedback() throws Exception {
        // Initialize the database
        softSkillsFeedbackRepository.saveAndFlush(softSkillsFeedback);

        int databaseSizeBeforeUpdate = softSkillsFeedbackRepository.findAll().size();

        // Update the softSkillsFeedback
        SoftSkillsFeedback updatedSoftSkillsFeedback = softSkillsFeedbackRepository.findById(softSkillsFeedback.getId()).get();
        // Disconnect from session so that the updates on updatedSoftSkillsFeedback are not directly saved in db
        em.detach(updatedSoftSkillsFeedback);
        updatedSoftSkillsFeedback
            .communication(UPDATED_COMMUNICATION)
            .motivationLevel(UPDATED_MOTIVATION_LEVEL);

        restSoftSkillsFeedbackMockMvc.perform(put("/api/soft-skills-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSoftSkillsFeedback)))
            .andExpect(status().isOk());

        // Validate the SoftSkillsFeedback in the database
        List<SoftSkillsFeedback> softSkillsFeedbackList = softSkillsFeedbackRepository.findAll();
        assertThat(softSkillsFeedbackList).hasSize(databaseSizeBeforeUpdate);
        SoftSkillsFeedback testSoftSkillsFeedback = softSkillsFeedbackList.get(softSkillsFeedbackList.size() - 1);
        assertThat(testSoftSkillsFeedback.getCommunication()).isEqualTo(UPDATED_COMMUNICATION);
        assertThat(testSoftSkillsFeedback.getMotivationLevel()).isEqualTo(UPDATED_MOTIVATION_LEVEL);
    }

    @Test
    @Transactional
    public void updateNonExistingSoftSkillsFeedback() throws Exception {
        int databaseSizeBeforeUpdate = softSkillsFeedbackRepository.findAll().size();

        // Create the SoftSkillsFeedback

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSoftSkillsFeedbackMockMvc.perform(put("/api/soft-skills-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(softSkillsFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the SoftSkillsFeedback in the database
        List<SoftSkillsFeedback> softSkillsFeedbackList = softSkillsFeedbackRepository.findAll();
        assertThat(softSkillsFeedbackList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSoftSkillsFeedback() throws Exception {
        // Initialize the database
        softSkillsFeedbackRepository.saveAndFlush(softSkillsFeedback);

        int databaseSizeBeforeDelete = softSkillsFeedbackRepository.findAll().size();

        // Delete the softSkillsFeedback
        restSoftSkillsFeedbackMockMvc.perform(delete("/api/soft-skills-feedbacks/{id}", softSkillsFeedback.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SoftSkillsFeedback> softSkillsFeedbackList = softSkillsFeedbackRepository.findAll();
        assertThat(softSkillsFeedbackList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SoftSkillsFeedback.class);
        SoftSkillsFeedback softSkillsFeedback1 = new SoftSkillsFeedback();
        softSkillsFeedback1.setId(1L);
        SoftSkillsFeedback softSkillsFeedback2 = new SoftSkillsFeedback();
        softSkillsFeedback2.setId(softSkillsFeedback1.getId());
        assertThat(softSkillsFeedback1).isEqualTo(softSkillsFeedback2);
        softSkillsFeedback2.setId(2L);
        assertThat(softSkillsFeedback1).isNotEqualTo(softSkillsFeedback2);
        softSkillsFeedback1.setId(null);
        assertThat(softSkillsFeedback1).isNotEqualTo(softSkillsFeedback2);
    }
}
