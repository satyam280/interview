package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.AnswerCheckEvalAreaMap;
import com.wissen.interview.repository.AnswerCheckEvalAreaMapRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AnswerCheckEvalAreaMapResource REST controller.
 *
 * @see AnswerCheckEvalAreaMapResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class AnswerCheckEvalAreaMapResourceIntTest {

    @Autowired
    private AnswerCheckEvalAreaMapRepository answerCheckEvalAreaMapRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAnswerCheckEvalAreaMapMockMvc;

    private AnswerCheckEvalAreaMap answerCheckEvalAreaMap;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AnswerCheckEvalAreaMapResource answerCheckEvalAreaMapResource = new AnswerCheckEvalAreaMapResource(answerCheckEvalAreaMapRepository);
        this.restAnswerCheckEvalAreaMapMockMvc = MockMvcBuilders.standaloneSetup(answerCheckEvalAreaMapResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnswerCheckEvalAreaMap createEntity(EntityManager em) {
        AnswerCheckEvalAreaMap answerCheckEvalAreaMap = new AnswerCheckEvalAreaMap();
        return answerCheckEvalAreaMap;
    }

    @Before
    public void initTest() {
        answerCheckEvalAreaMap = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnswerCheckEvalAreaMap() throws Exception {
        int databaseSizeBeforeCreate = answerCheckEvalAreaMapRepository.findAll().size();

        // Create the AnswerCheckEvalAreaMap
        restAnswerCheckEvalAreaMapMockMvc.perform(post("/api/answer-check-eval-area-maps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheckEvalAreaMap)))
            .andExpect(status().isCreated());

        // Validate the AnswerCheckEvalAreaMap in the database
        List<AnswerCheckEvalAreaMap> answerCheckEvalAreaMapList = answerCheckEvalAreaMapRepository.findAll();
        assertThat(answerCheckEvalAreaMapList).hasSize(databaseSizeBeforeCreate + 1);
        AnswerCheckEvalAreaMap testAnswerCheckEvalAreaMap = answerCheckEvalAreaMapList.get(answerCheckEvalAreaMapList.size() - 1);
    }

    @Test
    @Transactional
    public void createAnswerCheckEvalAreaMapWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = answerCheckEvalAreaMapRepository.findAll().size();

        // Create the AnswerCheckEvalAreaMap with an existing ID
        answerCheckEvalAreaMap.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnswerCheckEvalAreaMapMockMvc.perform(post("/api/answer-check-eval-area-maps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheckEvalAreaMap)))
            .andExpect(status().isBadRequest());

        // Validate the AnswerCheckEvalAreaMap in the database
        List<AnswerCheckEvalAreaMap> answerCheckEvalAreaMapList = answerCheckEvalAreaMapRepository.findAll();
        assertThat(answerCheckEvalAreaMapList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAnswerCheckEvalAreaMaps() throws Exception {
        // Initialize the database
        answerCheckEvalAreaMapRepository.saveAndFlush(answerCheckEvalAreaMap);

        // Get all the answerCheckEvalAreaMapList
        restAnswerCheckEvalAreaMapMockMvc.perform(get("/api/answer-check-eval-area-maps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(answerCheckEvalAreaMap.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getAnswerCheckEvalAreaMap() throws Exception {
        // Initialize the database
        answerCheckEvalAreaMapRepository.saveAndFlush(answerCheckEvalAreaMap);

        // Get the answerCheckEvalAreaMap
        restAnswerCheckEvalAreaMapMockMvc.perform(get("/api/answer-check-eval-area-maps/{id}", answerCheckEvalAreaMap.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(answerCheckEvalAreaMap.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAnswerCheckEvalAreaMap() throws Exception {
        // Get the answerCheckEvalAreaMap
        restAnswerCheckEvalAreaMapMockMvc.perform(get("/api/answer-check-eval-area-maps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnswerCheckEvalAreaMap() throws Exception {
        // Initialize the database
        answerCheckEvalAreaMapRepository.saveAndFlush(answerCheckEvalAreaMap);

        int databaseSizeBeforeUpdate = answerCheckEvalAreaMapRepository.findAll().size();

        // Update the answerCheckEvalAreaMap
        AnswerCheckEvalAreaMap updatedAnswerCheckEvalAreaMap = answerCheckEvalAreaMapRepository.findById(answerCheckEvalAreaMap.getId()).get();
        // Disconnect from session so that the updates on updatedAnswerCheckEvalAreaMap are not directly saved in db
        em.detach(updatedAnswerCheckEvalAreaMap);

        restAnswerCheckEvalAreaMapMockMvc.perform(put("/api/answer-check-eval-area-maps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnswerCheckEvalAreaMap)))
            .andExpect(status().isOk());

        // Validate the AnswerCheckEvalAreaMap in the database
        List<AnswerCheckEvalAreaMap> answerCheckEvalAreaMapList = answerCheckEvalAreaMapRepository.findAll();
        assertThat(answerCheckEvalAreaMapList).hasSize(databaseSizeBeforeUpdate);
        AnswerCheckEvalAreaMap testAnswerCheckEvalAreaMap = answerCheckEvalAreaMapList.get(answerCheckEvalAreaMapList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingAnswerCheckEvalAreaMap() throws Exception {
        int databaseSizeBeforeUpdate = answerCheckEvalAreaMapRepository.findAll().size();

        // Create the AnswerCheckEvalAreaMap

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnswerCheckEvalAreaMapMockMvc.perform(put("/api/answer-check-eval-area-maps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheckEvalAreaMap)))
            .andExpect(status().isBadRequest());

        // Validate the AnswerCheckEvalAreaMap in the database
        List<AnswerCheckEvalAreaMap> answerCheckEvalAreaMapList = answerCheckEvalAreaMapRepository.findAll();
        assertThat(answerCheckEvalAreaMapList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnswerCheckEvalAreaMap() throws Exception {
        // Initialize the database
        answerCheckEvalAreaMapRepository.saveAndFlush(answerCheckEvalAreaMap);

        int databaseSizeBeforeDelete = answerCheckEvalAreaMapRepository.findAll().size();

        // Delete the answerCheckEvalAreaMap
        restAnswerCheckEvalAreaMapMockMvc.perform(delete("/api/answer-check-eval-area-maps/{id}", answerCheckEvalAreaMap.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AnswerCheckEvalAreaMap> answerCheckEvalAreaMapList = answerCheckEvalAreaMapRepository.findAll();
        assertThat(answerCheckEvalAreaMapList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnswerCheckEvalAreaMap.class);
        AnswerCheckEvalAreaMap answerCheckEvalAreaMap1 = new AnswerCheckEvalAreaMap();
        answerCheckEvalAreaMap1.setId(1L);
        AnswerCheckEvalAreaMap answerCheckEvalAreaMap2 = new AnswerCheckEvalAreaMap();
        answerCheckEvalAreaMap2.setId(answerCheckEvalAreaMap1.getId());
        assertThat(answerCheckEvalAreaMap1).isEqualTo(answerCheckEvalAreaMap2);
        answerCheckEvalAreaMap2.setId(2L);
        assertThat(answerCheckEvalAreaMap1).isNotEqualTo(answerCheckEvalAreaMap2);
        answerCheckEvalAreaMap1.setId(null);
        assertThat(answerCheckEvalAreaMap1).isNotEqualTo(answerCheckEvalAreaMap2);
    }
}
