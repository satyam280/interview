package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;
import com.wissen.interview.domain.SkillUser;
import com.wissen.interview.repository.SkillUserRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SkillUserResource} REST controller.
 */
@SpringBootTest(classes = InterviewApp.class)
public class SkillUserResourceIT {

    private static final String DEFAULT_USER_EMAIL_ID = "r>@S.?c";
    private static final String UPDATED_USER_EMAIL_ID = "L@x.w6";

    @Autowired
    private SkillUserRepository skillUserRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSkillUserMockMvc;

    private SkillUser skillUser;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SkillUserResource skillUserResource = new SkillUserResource(skillUserRepository);
        this.restSkillUserMockMvc = MockMvcBuilders.standaloneSetup(skillUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SkillUser createEntity(EntityManager em) {
        SkillUser skillUser = new SkillUser()
            .userEmailId(DEFAULT_USER_EMAIL_ID);
        return skillUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SkillUser createUpdatedEntity(EntityManager em) {
        SkillUser skillUser = new SkillUser()
            .userEmailId(UPDATED_USER_EMAIL_ID);
        return skillUser;
    }

    @BeforeEach
    public void initTest() {
        skillUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createSkillUser() throws Exception {
        int databaseSizeBeforeCreate = skillUserRepository.findAll().size();

        // Create the SkillUser
        restSkillUserMockMvc.perform(post("/api/skill-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skillUser)))
            .andExpect(status().isCreated());

        // Validate the SkillUser in the database
        List<SkillUser> skillUserList = skillUserRepository.findAll();
        assertThat(skillUserList).hasSize(databaseSizeBeforeCreate + 1);
        SkillUser testSkillUser = skillUserList.get(skillUserList.size() - 1);
        assertThat(testSkillUser.getUserEmailId()).isEqualTo(DEFAULT_USER_EMAIL_ID);
    }

    @Test
    @Transactional
    public void createSkillUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = skillUserRepository.findAll().size();

        // Create the SkillUser with an existing ID
        skillUser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSkillUserMockMvc.perform(post("/api/skill-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skillUser)))
            .andExpect(status().isBadRequest());

        // Validate the SkillUser in the database
        List<SkillUser> skillUserList = skillUserRepository.findAll();
        assertThat(skillUserList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUserEmailIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = skillUserRepository.findAll().size();
        // set the field null
        skillUser.setUserEmailId(null);

        // Create the SkillUser, which fails.

        restSkillUserMockMvc.perform(post("/api/skill-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skillUser)))
            .andExpect(status().isBadRequest());

        List<SkillUser> skillUserList = skillUserRepository.findAll();
        assertThat(skillUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSkillUsers() throws Exception {
        // Initialize the database
        skillUserRepository.saveAndFlush(skillUser);

        // Get all the skillUserList
        restSkillUserMockMvc.perform(get("/api/skill-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(skillUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userEmailId").value(hasItem(DEFAULT_USER_EMAIL_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getSkillUser() throws Exception {
        // Initialize the database
        skillUserRepository.saveAndFlush(skillUser);

        // Get the skillUser
        restSkillUserMockMvc.perform(get("/api/skill-users/{id}", skillUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(skillUser.getId().intValue()))
            .andExpect(jsonPath("$.userEmailId").value(DEFAULT_USER_EMAIL_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSkillUser() throws Exception {
        // Get the skillUser
        restSkillUserMockMvc.perform(get("/api/skill-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSkillUser() throws Exception {
        // Initialize the database
        skillUserRepository.saveAndFlush(skillUser);

        int databaseSizeBeforeUpdate = skillUserRepository.findAll().size();

        // Update the skillUser
        SkillUser updatedSkillUser = skillUserRepository.findById(skillUser.getId()).get();
        // Disconnect from session so that the updates on updatedSkillUser are not directly saved in db
        em.detach(updatedSkillUser);
        updatedSkillUser
            .userEmailId(UPDATED_USER_EMAIL_ID);

        restSkillUserMockMvc.perform(put("/api/skill-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSkillUser)))
            .andExpect(status().isOk());

        // Validate the SkillUser in the database
        List<SkillUser> skillUserList = skillUserRepository.findAll();
        assertThat(skillUserList).hasSize(databaseSizeBeforeUpdate);
        SkillUser testSkillUser = skillUserList.get(skillUserList.size() - 1);
        assertThat(testSkillUser.getUserEmailId()).isEqualTo(UPDATED_USER_EMAIL_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingSkillUser() throws Exception {
        int databaseSizeBeforeUpdate = skillUserRepository.findAll().size();

        // Create the SkillUser

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSkillUserMockMvc.perform(put("/api/skill-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skillUser)))
            .andExpect(status().isBadRequest());

        // Validate the SkillUser in the database
        List<SkillUser> skillUserList = skillUserRepository.findAll();
        assertThat(skillUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSkillUser() throws Exception {
        // Initialize the database
        skillUserRepository.saveAndFlush(skillUser);

        int databaseSizeBeforeDelete = skillUserRepository.findAll().size();

        // Delete the skillUser
        restSkillUserMockMvc.perform(delete("/api/skill-users/{id}", skillUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SkillUser> skillUserList = skillUserRepository.findAll();
        assertThat(skillUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SkillUser.class);
        SkillUser skillUser1 = new SkillUser();
        skillUser1.setId(1L);
        SkillUser skillUser2 = new SkillUser();
        skillUser2.setId(skillUser1.getId());
        assertThat(skillUser1).isEqualTo(skillUser2);
        skillUser2.setId(2L);
        assertThat(skillUser1).isNotEqualTo(skillUser2);
        skillUser1.setId(null);
        assertThat(skillUser1).isNotEqualTo(skillUser2);
    }
}
