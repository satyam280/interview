package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.AnswerCheckFeedback;
import com.wissen.interview.repository.AnswerCheckFeedbackRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.wissen.interview.domain.enumeration.AnswerCheckStatus;
/**
 * Test class for the AnswerCheckFeedbackResource REST controller.
 *
 * @see AnswerCheckFeedbackResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class AnswerCheckFeedbackResourceIntTest {

    private static final AnswerCheckStatus DEFAULT_ANSWER_QUALITY = AnswerCheckStatus.PASS;
    private static final AnswerCheckStatus UPDATED_ANSWER_QUALITY = AnswerCheckStatus.FAIL;

    @Autowired
    private AnswerCheckFeedbackRepository answerCheckFeedbackRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAnswerCheckFeedbackMockMvc;

    private AnswerCheckFeedback answerCheckFeedback;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AnswerCheckFeedbackResource answerCheckFeedbackResource = new AnswerCheckFeedbackResource(answerCheckFeedbackRepository);
        this.restAnswerCheckFeedbackMockMvc = MockMvcBuilders.standaloneSetup(answerCheckFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnswerCheckFeedback createEntity(EntityManager em) {
        AnswerCheckFeedback answerCheckFeedback = new AnswerCheckFeedback()
            .answer_quality(DEFAULT_ANSWER_QUALITY);
        return answerCheckFeedback;
    }

    @Before
    public void initTest() {
        answerCheckFeedback = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnswerCheckFeedback() throws Exception {
        int databaseSizeBeforeCreate = answerCheckFeedbackRepository.findAll().size();

        // Create the AnswerCheckFeedback
        restAnswerCheckFeedbackMockMvc.perform(post("/api/answer-check-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheckFeedback)))
            .andExpect(status().isCreated());

        // Validate the AnswerCheckFeedback in the database
        List<AnswerCheckFeedback> answerCheckFeedbackList = answerCheckFeedbackRepository.findAll();
        assertThat(answerCheckFeedbackList).hasSize(databaseSizeBeforeCreate + 1);
        AnswerCheckFeedback testAnswerCheckFeedback = answerCheckFeedbackList.get(answerCheckFeedbackList.size() - 1);
        assertThat(testAnswerCheckFeedback.getAnswer_quality()).isEqualTo(DEFAULT_ANSWER_QUALITY);
    }

    @Test
    @Transactional
    public void createAnswerCheckFeedbackWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = answerCheckFeedbackRepository.findAll().size();

        // Create the AnswerCheckFeedback with an existing ID
        answerCheckFeedback.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnswerCheckFeedbackMockMvc.perform(post("/api/answer-check-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheckFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the AnswerCheckFeedback in the database
        List<AnswerCheckFeedback> answerCheckFeedbackList = answerCheckFeedbackRepository.findAll();
        assertThat(answerCheckFeedbackList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAnswerCheckFeedbacks() throws Exception {
        // Initialize the database
        answerCheckFeedbackRepository.saveAndFlush(answerCheckFeedback);

        // Get all the answerCheckFeedbackList
        restAnswerCheckFeedbackMockMvc.perform(get("/api/answer-check-feedbacks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(answerCheckFeedback.getId().intValue())))
            .andExpect(jsonPath("$.[*].answer_quality").value(hasItem(DEFAULT_ANSWER_QUALITY.toString())));
    }
    
    @Test
    @Transactional
    public void getAnswerCheckFeedback() throws Exception {
        // Initialize the database
        answerCheckFeedbackRepository.saveAndFlush(answerCheckFeedback);

        // Get the answerCheckFeedback
        restAnswerCheckFeedbackMockMvc.perform(get("/api/answer-check-feedbacks/{id}", answerCheckFeedback.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(answerCheckFeedback.getId().intValue()))
            .andExpect(jsonPath("$.answer_quality").value(DEFAULT_ANSWER_QUALITY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnswerCheckFeedback() throws Exception {
        // Get the answerCheckFeedback
        restAnswerCheckFeedbackMockMvc.perform(get("/api/answer-check-feedbacks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnswerCheckFeedback() throws Exception {
        // Initialize the database
        answerCheckFeedbackRepository.saveAndFlush(answerCheckFeedback);

        int databaseSizeBeforeUpdate = answerCheckFeedbackRepository.findAll().size();

        // Update the answerCheckFeedback
        AnswerCheckFeedback updatedAnswerCheckFeedback = answerCheckFeedbackRepository.findById(answerCheckFeedback.getId()).get();
        // Disconnect from session so that the updates on updatedAnswerCheckFeedback are not directly saved in db
        em.detach(updatedAnswerCheckFeedback);
        updatedAnswerCheckFeedback
            .answer_quality(UPDATED_ANSWER_QUALITY);

        restAnswerCheckFeedbackMockMvc.perform(put("/api/answer-check-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnswerCheckFeedback)))
            .andExpect(status().isOk());

        // Validate the AnswerCheckFeedback in the database
        List<AnswerCheckFeedback> answerCheckFeedbackList = answerCheckFeedbackRepository.findAll();
        assertThat(answerCheckFeedbackList).hasSize(databaseSizeBeforeUpdate);
        AnswerCheckFeedback testAnswerCheckFeedback = answerCheckFeedbackList.get(answerCheckFeedbackList.size() - 1);
        assertThat(testAnswerCheckFeedback.getAnswer_quality()).isEqualTo(UPDATED_ANSWER_QUALITY);
    }

    @Test
    @Transactional
    public void updateNonExistingAnswerCheckFeedback() throws Exception {
        int databaseSizeBeforeUpdate = answerCheckFeedbackRepository.findAll().size();

        // Create the AnswerCheckFeedback

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnswerCheckFeedbackMockMvc.perform(put("/api/answer-check-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheckFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the AnswerCheckFeedback in the database
        List<AnswerCheckFeedback> answerCheckFeedbackList = answerCheckFeedbackRepository.findAll();
        assertThat(answerCheckFeedbackList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnswerCheckFeedback() throws Exception {
        // Initialize the database
        answerCheckFeedbackRepository.saveAndFlush(answerCheckFeedback);

        int databaseSizeBeforeDelete = answerCheckFeedbackRepository.findAll().size();

        // Delete the answerCheckFeedback
        restAnswerCheckFeedbackMockMvc.perform(delete("/api/answer-check-feedbacks/{id}", answerCheckFeedback.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AnswerCheckFeedback> answerCheckFeedbackList = answerCheckFeedbackRepository.findAll();
        assertThat(answerCheckFeedbackList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnswerCheckFeedback.class);
        AnswerCheckFeedback answerCheckFeedback1 = new AnswerCheckFeedback();
        answerCheckFeedback1.setId(1L);
        AnswerCheckFeedback answerCheckFeedback2 = new AnswerCheckFeedback();
        answerCheckFeedback2.setId(answerCheckFeedback1.getId());
        assertThat(answerCheckFeedback1).isEqualTo(answerCheckFeedback2);
        answerCheckFeedback2.setId(2L);
        assertThat(answerCheckFeedback1).isNotEqualTo(answerCheckFeedback2);
        answerCheckFeedback1.setId(null);
        assertThat(answerCheckFeedback1).isNotEqualTo(answerCheckFeedback2);
    }
}
