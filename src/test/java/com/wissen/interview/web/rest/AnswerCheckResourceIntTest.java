package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.AnswerCheck;
import com.wissen.interview.repository.AnswerCheckRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AnswerCheckResource REST controller.
 *
 * @see AnswerCheckResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class AnswerCheckResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private AnswerCheckRepository answerCheckRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAnswerCheckMockMvc;

    private AnswerCheck answerCheck;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AnswerCheckResource answerCheckResource = new AnswerCheckResource(answerCheckRepository);
        this.restAnswerCheckMockMvc = MockMvcBuilders.standaloneSetup(answerCheckResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnswerCheck createEntity(EntityManager em) {
        AnswerCheck answerCheck = new AnswerCheck()
            .name(DEFAULT_NAME);
        return answerCheck;
    }

    @Before
    public void initTest() {
        answerCheck = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnswerCheck() throws Exception {
        int databaseSizeBeforeCreate = answerCheckRepository.findAll().size();

        // Create the AnswerCheck
        restAnswerCheckMockMvc.perform(post("/api/answer-checks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheck)))
            .andExpect(status().isCreated());

        // Validate the AnswerCheck in the database
        List<AnswerCheck> answerCheckList = answerCheckRepository.findAll();
        assertThat(answerCheckList).hasSize(databaseSizeBeforeCreate + 1);
        AnswerCheck testAnswerCheck = answerCheckList.get(answerCheckList.size() - 1);
        assertThat(testAnswerCheck.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createAnswerCheckWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = answerCheckRepository.findAll().size();

        // Create the AnswerCheck with an existing ID
        answerCheck.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnswerCheckMockMvc.perform(post("/api/answer-checks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheck)))
            .andExpect(status().isBadRequest());

        // Validate the AnswerCheck in the database
        List<AnswerCheck> answerCheckList = answerCheckRepository.findAll();
        assertThat(answerCheckList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAnswerChecks() throws Exception {
        // Initialize the database
        answerCheckRepository.saveAndFlush(answerCheck);

        // Get all the answerCheckList
        restAnswerCheckMockMvc.perform(get("/api/answer-checks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(answerCheck.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getAnswerCheck() throws Exception {
        // Initialize the database
        answerCheckRepository.saveAndFlush(answerCheck);

        // Get the answerCheck
        restAnswerCheckMockMvc.perform(get("/api/answer-checks/{id}", answerCheck.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(answerCheck.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnswerCheck() throws Exception {
        // Get the answerCheck
        restAnswerCheckMockMvc.perform(get("/api/answer-checks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnswerCheck() throws Exception {
        // Initialize the database
        answerCheckRepository.saveAndFlush(answerCheck);

        int databaseSizeBeforeUpdate = answerCheckRepository.findAll().size();

        // Update the answerCheck
        AnswerCheck updatedAnswerCheck = answerCheckRepository.findById(answerCheck.getId()).get();
        // Disconnect from session so that the updates on updatedAnswerCheck are not directly saved in db
        em.detach(updatedAnswerCheck);
        updatedAnswerCheck
            .name(UPDATED_NAME);

        restAnswerCheckMockMvc.perform(put("/api/answer-checks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnswerCheck)))
            .andExpect(status().isOk());

        // Validate the AnswerCheck in the database
        List<AnswerCheck> answerCheckList = answerCheckRepository.findAll();
        assertThat(answerCheckList).hasSize(databaseSizeBeforeUpdate);
        AnswerCheck testAnswerCheck = answerCheckList.get(answerCheckList.size() - 1);
        assertThat(testAnswerCheck.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingAnswerCheck() throws Exception {
        int databaseSizeBeforeUpdate = answerCheckRepository.findAll().size();

        // Create the AnswerCheck

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnswerCheckMockMvc.perform(put("/api/answer-checks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answerCheck)))
            .andExpect(status().isBadRequest());

        // Validate the AnswerCheck in the database
        List<AnswerCheck> answerCheckList = answerCheckRepository.findAll();
        assertThat(answerCheckList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnswerCheck() throws Exception {
        // Initialize the database
        answerCheckRepository.saveAndFlush(answerCheck);

        int databaseSizeBeforeDelete = answerCheckRepository.findAll().size();

        // Delete the answerCheck
        restAnswerCheckMockMvc.perform(delete("/api/answer-checks/{id}", answerCheck.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AnswerCheck> answerCheckList = answerCheckRepository.findAll();
        assertThat(answerCheckList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnswerCheck.class);
        AnswerCheck answerCheck1 = new AnswerCheck();
        answerCheck1.setId(1L);
        AnswerCheck answerCheck2 = new AnswerCheck();
        answerCheck2.setId(answerCheck1.getId());
        assertThat(answerCheck1).isEqualTo(answerCheck2);
        answerCheck2.setId(2L);
        assertThat(answerCheck1).isNotEqualTo(answerCheck2);
        answerCheck1.setId(null);
        assertThat(answerCheck1).isNotEqualTo(answerCheck2);
    }
}
