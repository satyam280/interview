package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.CustomQuestion;
import com.wissen.interview.repository.CustomQuestionRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.sameInstant;
import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.wissen.interview.domain.enumeration.AnswerQuality;
/**
 * Test class for the CustomQuestionResource REST controller.
 *
 * @see CustomQuestionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class CustomQuestionResourceIntTest {

    private static final String DEFAULT_QUESTION_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION_TEXT = "BBBBBBBBBB";

    private static final AnswerQuality DEFAULT_ANSWER_QUALITY = AnswerQuality.EXCELLENT;
    private static final AnswerQuality UPDATED_ANSWER_QUALITY = AnswerQuality.SATISFACTORY;

    private static final String DEFAULT_ANSWER_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER_TEXT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_STARTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISHED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISHED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private CustomQuestionRepository customQuestionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCustomQuestionMockMvc;

    private CustomQuestion customQuestion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CustomQuestionResource customQuestionResource = new CustomQuestionResource(customQuestionRepository);
        this.restCustomQuestionMockMvc = MockMvcBuilders.standaloneSetup(customQuestionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomQuestion createEntity(EntityManager em) {
        CustomQuestion customQuestion = new CustomQuestion()
            .question_text(DEFAULT_QUESTION_TEXT)
            .answerQuality(DEFAULT_ANSWER_QUALITY)
            .answerText(DEFAULT_ANSWER_TEXT)
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT);
        return customQuestion;
    }

    @Before
    public void initTest() {
        customQuestion = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomQuestion() throws Exception {
        int databaseSizeBeforeCreate = customQuestionRepository.findAll().size();

        // Create the CustomQuestion
        restCustomQuestionMockMvc.perform(post("/api/custom-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customQuestion)))
            .andExpect(status().isCreated());

        // Validate the CustomQuestion in the database
        List<CustomQuestion> customQuestionList = customQuestionRepository.findAll();
        assertThat(customQuestionList).hasSize(databaseSizeBeforeCreate + 1);
        CustomQuestion testCustomQuestion = customQuestionList.get(customQuestionList.size() - 1);
        assertThat(testCustomQuestion.getQuestion_text()).isEqualTo(DEFAULT_QUESTION_TEXT);
        assertThat(testCustomQuestion.getAnswerQuality()).isEqualTo(DEFAULT_ANSWER_QUALITY);
        assertThat(testCustomQuestion.getAnswerText()).isEqualTo(DEFAULT_ANSWER_TEXT);
        assertThat(testCustomQuestion.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testCustomQuestion.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
    }

    @Test
    @Transactional
    public void createCustomQuestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customQuestionRepository.findAll().size();

        // Create the CustomQuestion with an existing ID
        customQuestion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomQuestionMockMvc.perform(post("/api/custom-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customQuestion)))
            .andExpect(status().isBadRequest());

        // Validate the CustomQuestion in the database
        List<CustomQuestion> customQuestionList = customQuestionRepository.findAll();
        assertThat(customQuestionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCustomQuestions() throws Exception {
        // Initialize the database
        customQuestionRepository.saveAndFlush(customQuestion);

        // Get all the customQuestionList
        restCustomQuestionMockMvc.perform(get("/api/custom-questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].question_text").value(hasItem(DEFAULT_QUESTION_TEXT.toString())))
            .andExpect(jsonPath("$.[*].answerQuality").value(hasItem(DEFAULT_ANSWER_QUALITY.toString())))
            .andExpect(jsonPath("$.[*].answerText").value(hasItem(DEFAULT_ANSWER_TEXT.toString())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))));
    }
    
    @Test
    @Transactional
    public void getCustomQuestion() throws Exception {
        // Initialize the database
        customQuestionRepository.saveAndFlush(customQuestion);

        // Get the customQuestion
        restCustomQuestionMockMvc.perform(get("/api/custom-questions/{id}", customQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customQuestion.getId().intValue()))
            .andExpect(jsonPath("$.question_text").value(DEFAULT_QUESTION_TEXT.toString()))
            .andExpect(jsonPath("$.answerQuality").value(DEFAULT_ANSWER_QUALITY.toString()))
            .andExpect(jsonPath("$.answerText").value(DEFAULT_ANSWER_TEXT.toString()))
            .andExpect(jsonPath("$.startedAt").value(sameInstant(DEFAULT_STARTED_AT)))
            .andExpect(jsonPath("$.finishedAt").value(sameInstant(DEFAULT_FINISHED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingCustomQuestion() throws Exception {
        // Get the customQuestion
        restCustomQuestionMockMvc.perform(get("/api/custom-questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomQuestion() throws Exception {
        // Initialize the database
        customQuestionRepository.saveAndFlush(customQuestion);

        int databaseSizeBeforeUpdate = customQuestionRepository.findAll().size();

        // Update the customQuestion
        CustomQuestion updatedCustomQuestion = customQuestionRepository.findById(customQuestion.getId()).get();
        // Disconnect from session so that the updates on updatedCustomQuestion are not directly saved in db
        em.detach(updatedCustomQuestion);
        updatedCustomQuestion
            .question_text(UPDATED_QUESTION_TEXT)
            .answerQuality(UPDATED_ANSWER_QUALITY)
            .answerText(UPDATED_ANSWER_TEXT)
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT);

        restCustomQuestionMockMvc.perform(put("/api/custom-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCustomQuestion)))
            .andExpect(status().isOk());

        // Validate the CustomQuestion in the database
        List<CustomQuestion> customQuestionList = customQuestionRepository.findAll();
        assertThat(customQuestionList).hasSize(databaseSizeBeforeUpdate);
        CustomQuestion testCustomQuestion = customQuestionList.get(customQuestionList.size() - 1);
        assertThat(testCustomQuestion.getQuestion_text()).isEqualTo(UPDATED_QUESTION_TEXT);
        assertThat(testCustomQuestion.getAnswerQuality()).isEqualTo(UPDATED_ANSWER_QUALITY);
        assertThat(testCustomQuestion.getAnswerText()).isEqualTo(UPDATED_ANSWER_TEXT);
        assertThat(testCustomQuestion.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testCustomQuestion.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomQuestion() throws Exception {
        int databaseSizeBeforeUpdate = customQuestionRepository.findAll().size();

        // Create the CustomQuestion

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomQuestionMockMvc.perform(put("/api/custom-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customQuestion)))
            .andExpect(status().isBadRequest());

        // Validate the CustomQuestion in the database
        List<CustomQuestion> customQuestionList = customQuestionRepository.findAll();
        assertThat(customQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCustomQuestion() throws Exception {
        // Initialize the database
        customQuestionRepository.saveAndFlush(customQuestion);

        int databaseSizeBeforeDelete = customQuestionRepository.findAll().size();

        // Delete the customQuestion
        restCustomQuestionMockMvc.perform(delete("/api/custom-questions/{id}", customQuestion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CustomQuestion> customQuestionList = customQuestionRepository.findAll();
        assertThat(customQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomQuestion.class);
        CustomQuestion customQuestion1 = new CustomQuestion();
        customQuestion1.setId(1L);
        CustomQuestion customQuestion2 = new CustomQuestion();
        customQuestion2.setId(customQuestion1.getId());
        assertThat(customQuestion1).isEqualTo(customQuestion2);
        customQuestion2.setId(2L);
        assertThat(customQuestion1).isNotEqualTo(customQuestion2);
        customQuestion1.setId(null);
        assertThat(customQuestion1).isNotEqualTo(customQuestion2);
    }
}
