package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.QuestionFeedback;
import com.wissen.interview.repository.QuestionFeedbackRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.sameInstant;
import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.wissen.interview.domain.enumeration.AnswerQuality;
/**
 * Test class for the QuestionFeedbackResource REST controller.
 *
 * @see QuestionFeedbackResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class QuestionFeedbackResourceIntTest {

    private static final AnswerQuality DEFAULT_ANSWER_QUALITY = AnswerQuality.EXCELLENT;
    private static final AnswerQuality UPDATED_ANSWER_QUALITY = AnswerQuality.SATISFACTORY;

    private static final ZonedDateTime DEFAULT_STARTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISHED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISHED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_DURATION = 1L;
    private static final Long UPDATED_DURATION = 2L;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private QuestionFeedbackRepository questionFeedbackRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restQuestionFeedbackMockMvc;

    private QuestionFeedback questionFeedback;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final QuestionFeedbackResource questionFeedbackResource = new QuestionFeedbackResource(questionFeedbackRepository);
        this.restQuestionFeedbackMockMvc = MockMvcBuilders.standaloneSetup(questionFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuestionFeedback createEntity(EntityManager em) {
        QuestionFeedback questionFeedback = new QuestionFeedback()
            .answer_quality(DEFAULT_ANSWER_QUALITY)
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT)
            .duration(DEFAULT_DURATION)
            .description(DEFAULT_DESCRIPTION);
        return questionFeedback;
    }

    @Before
    public void initTest() {
        questionFeedback = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuestionFeedback() throws Exception {
        int databaseSizeBeforeCreate = questionFeedbackRepository.findAll().size();

        // Create the QuestionFeedback
        restQuestionFeedbackMockMvc.perform(post("/api/question-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionFeedback)))
            .andExpect(status().isCreated());

        // Validate the QuestionFeedback in the database
        List<QuestionFeedback> questionFeedbackList = questionFeedbackRepository.findAll();
        assertThat(questionFeedbackList).hasSize(databaseSizeBeforeCreate + 1);
        QuestionFeedback testQuestionFeedback = questionFeedbackList.get(questionFeedbackList.size() - 1);
        assertThat(testQuestionFeedback.getAnswer_quality()).isEqualTo(DEFAULT_ANSWER_QUALITY);
        assertThat(testQuestionFeedback.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testQuestionFeedback.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
        assertThat(testQuestionFeedback.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testQuestionFeedback.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createQuestionFeedbackWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = questionFeedbackRepository.findAll().size();

        // Create the QuestionFeedback with an existing ID
        questionFeedback.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuestionFeedbackMockMvc.perform(post("/api/question-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the QuestionFeedback in the database
        List<QuestionFeedback> questionFeedbackList = questionFeedbackRepository.findAll();
        assertThat(questionFeedbackList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllQuestionFeedbacks() throws Exception {
        // Initialize the database
        questionFeedbackRepository.saveAndFlush(questionFeedback);

        // Get all the questionFeedbackList
        restQuestionFeedbackMockMvc.perform(get("/api/question-feedbacks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(questionFeedback.getId().intValue())))
            .andExpect(jsonPath("$.[*].answer_quality").value(hasItem(DEFAULT_ANSWER_QUALITY.toString())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getQuestionFeedback() throws Exception {
        // Initialize the database
        questionFeedbackRepository.saveAndFlush(questionFeedback);

        // Get the questionFeedback
        restQuestionFeedbackMockMvc.perform(get("/api/question-feedbacks/{id}", questionFeedback.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(questionFeedback.getId().intValue()))
            .andExpect(jsonPath("$.answer_quality").value(DEFAULT_ANSWER_QUALITY.toString()))
            .andExpect(jsonPath("$.startedAt").value(sameInstant(DEFAULT_STARTED_AT)))
            .andExpect(jsonPath("$.finishedAt").value(sameInstant(DEFAULT_FINISHED_AT)))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingQuestionFeedback() throws Exception {
        // Get the questionFeedback
        restQuestionFeedbackMockMvc.perform(get("/api/question-feedbacks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuestionFeedback() throws Exception {
        // Initialize the database
        questionFeedbackRepository.saveAndFlush(questionFeedback);

        int databaseSizeBeforeUpdate = questionFeedbackRepository.findAll().size();

        // Update the questionFeedback
        QuestionFeedback updatedQuestionFeedback = questionFeedbackRepository.findById(questionFeedback.getId()).get();
        // Disconnect from session so that the updates on updatedQuestionFeedback are not directly saved in db
        em.detach(updatedQuestionFeedback);
        updatedQuestionFeedback
            .answer_quality(UPDATED_ANSWER_QUALITY)
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .duration(UPDATED_DURATION)
            .description(UPDATED_DESCRIPTION);

        restQuestionFeedbackMockMvc.perform(put("/api/question-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQuestionFeedback)))
            .andExpect(status().isOk());

        // Validate the QuestionFeedback in the database
        List<QuestionFeedback> questionFeedbackList = questionFeedbackRepository.findAll();
        assertThat(questionFeedbackList).hasSize(databaseSizeBeforeUpdate);
        QuestionFeedback testQuestionFeedback = questionFeedbackList.get(questionFeedbackList.size() - 1);
        assertThat(testQuestionFeedback.getAnswer_quality()).isEqualTo(UPDATED_ANSWER_QUALITY);
        assertThat(testQuestionFeedback.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testQuestionFeedback.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testQuestionFeedback.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testQuestionFeedback.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingQuestionFeedback() throws Exception {
        int databaseSizeBeforeUpdate = questionFeedbackRepository.findAll().size();

        // Create the QuestionFeedback

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuestionFeedbackMockMvc.perform(put("/api/question-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the QuestionFeedback in the database
        List<QuestionFeedback> questionFeedbackList = questionFeedbackRepository.findAll();
        assertThat(questionFeedbackList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuestionFeedback() throws Exception {
        // Initialize the database
        questionFeedbackRepository.saveAndFlush(questionFeedback);

        int databaseSizeBeforeDelete = questionFeedbackRepository.findAll().size();

        // Delete the questionFeedback
        restQuestionFeedbackMockMvc.perform(delete("/api/question-feedbacks/{id}", questionFeedback.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<QuestionFeedback> questionFeedbackList = questionFeedbackRepository.findAll();
        assertThat(questionFeedbackList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuestionFeedback.class);
        QuestionFeedback questionFeedback1 = new QuestionFeedback();
        questionFeedback1.setId(1L);
        QuestionFeedback questionFeedback2 = new QuestionFeedback();
        questionFeedback2.setId(questionFeedback1.getId());
        assertThat(questionFeedback1).isEqualTo(questionFeedback2);
        questionFeedback2.setId(2L);
        assertThat(questionFeedback1).isNotEqualTo(questionFeedback2);
        questionFeedback1.setId(null);
        assertThat(questionFeedback1).isNotEqualTo(questionFeedback2);
    }
}
