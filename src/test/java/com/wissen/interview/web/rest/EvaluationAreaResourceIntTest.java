package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;

import com.wissen.interview.domain.EvaluationArea;
import com.wissen.interview.repository.EvaluationAreaRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EvaluationAreaResource REST controller.
 *
 * @see EvaluationAreaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterviewApp.class)
public class EvaluationAreaResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @Autowired
    private EvaluationAreaRepository evaluationAreaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEvaluationAreaMockMvc;

    private EvaluationArea evaluationArea;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EvaluationAreaResource evaluationAreaResource = new EvaluationAreaResource(evaluationAreaRepository);
log.debug("fjinfim");
        this.restEvaluationAreaMockMvc = MockMvcBuilders.standaloneSetup(evaluationAreaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EvaluationArea createEntity(EntityManager em) {
        EvaluationArea evaluationArea = new EvaluationArea()
            .name(DEFAULT_NAME);
        return evaluationArea;
    }

    @Before
    public void initTest() {
        evaluationArea = createEntity(em);
    }

    @Test
    @Transactional
    public void createEvaluationArea() throws Exception {
        int databaseSizeBeforeCreate = evaluationAreaRepository.findAll().size();

        // Create the EvaluationArea
        restEvaluationAreaMockMvc.perform(post("/api/evaluation-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evaluationArea)))
            .andExpect(status().isCreated());

        // Validate the EvaluationArea in the database
        List<EvaluationArea> evaluationAreaList = evaluationAreaRepository.findAll();
        assertThat(evaluationAreaList).hasSize(databaseSizeBeforeCreate + 1);
        EvaluationArea testEvaluationArea = evaluationAreaList.get(evaluationAreaList.size() - 1);
        assertThat(testEvaluationArea.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createEvaluationAreaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = evaluationAreaRepository.findAll().size();

        // Create the EvaluationArea with an existing ID
        evaluationArea.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEvaluationAreaMockMvc.perform(post("/api/evaluation-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evaluationArea)))
            .andExpect(status().isBadRequest());

        // Validate the EvaluationArea in the database
        List<EvaluationArea> evaluationAreaList = evaluationAreaRepository.findAll();
        assertThat(evaluationAreaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEvaluationAreas() throws Exception {
        // Initialize the database
        evaluationAreaRepository.saveAndFlush(evaluationArea);

        // Get all the evaluationAreaList
        restEvaluationAreaMockMvc.perform(get("/api/evaluation-areas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evaluationArea.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getEvaluationArea() throws Exception {
        // Initialize the database
        evaluationAreaRepository.saveAndFlush(evaluationArea);

        // Get the evaluationArea
        restEvaluationAreaMockMvc.perform(get("/api/evaluation-areas/{id}", evaluationArea.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(evaluationArea.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEvaluationArea() throws Exception {
        // Get the evaluationArea
        restEvaluationAreaMockMvc.perform(get("/api/evaluation-areas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEvaluationArea() throws Exception {
        // Initialize the database
        evaluationAreaRepository.saveAndFlush(evaluationArea);

        int databaseSizeBeforeUpdate = evaluationAreaRepository.findAll().size();

        // Update the evaluationArea
        EvaluationArea updatedEvaluationArea = evaluationAreaRepository.findById(evaluationArea.getId()).get();
        // Disconnect from session so that the updates on updatedEvaluationArea are not directly saved in db
        em.detach(updatedEvaluationArea);
        updatedEvaluationArea
            .name(UPDATED_NAME);

        restEvaluationAreaMockMvc.perform(put("/api/evaluation-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEvaluationArea)))
            .andExpect(status().isOk());

        // Validate the EvaluationArea in the database
        List<EvaluationArea> evaluationAreaList = evaluationAreaRepository.findAll();
        assertThat(evaluationAreaList).hasSize(databaseSizeBeforeUpdate);
        EvaluationArea testEvaluationArea = evaluationAreaList.get(evaluationAreaList.size() - 1);
        assertThat(testEvaluationArea.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingEvaluationArea() throws Exception {
        int databaseSizeBeforeUpdate = evaluationAreaRepository.findAll().size();

        // Create the EvaluationArea

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEvaluationAreaMockMvc.perform(put("/api/evaluation-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evaluationArea)))
            .andExpect(status().isBadRequest());

        // Validate the EvaluationArea in the database
        List<EvaluationArea> evaluationAreaList = evaluationAreaRepository.findAll();
        assertThat(evaluationAreaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEvaluationArea() throws Exception {
        // Initialize the database
        evaluationAreaRepository.saveAndFlush(evaluationArea);

        int databaseSizeBeforeDelete = evaluationAreaRepository.findAll().size();

        // Delete the evaluationArea
        restEvaluationAreaMockMvc.perform(delete("/api/evaluation-areas/{id}", evaluationArea.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EvaluationArea> evaluationAreaList = evaluationAreaRepository.findAll();
        assertThat(evaluationAreaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EvaluationArea.class);
        EvaluationArea evaluationArea1 = new EvaluationArea();
        evaluationArea1.setId(1L);
        EvaluationArea evaluationArea2 = new EvaluationArea();
        evaluationArea2.setId(evaluationArea1.getId());
        assertThat(evaluationArea1).isEqualTo(evaluationArea2);
        evaluationArea2.setId(2L);
        assertThat(evaluationArea1).isNotEqualTo(evaluationArea2);
        evaluationArea1.setId(null);
        assertThat(evaluationArea1).isNotEqualTo(evaluationArea2);
    }
}
