package com.wissen.interview.web.rest;

import com.wissen.interview.InterviewApp;
import com.wissen.interview.domain.AdminUser;
import com.wissen.interview.repository.AdminUserRepository;
import com.wissen.interview.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.wissen.interview.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AdminUserResource} REST controller.
 */
@SpringBootTest(classes = InterviewApp.class)
public class AdminUserResourceIT {

    private static final String DEFAULT_EMAIL_ID = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ID = "BBBBBBBBBB";

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdminUserMockMvc;

    private AdminUser adminUser;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdminUserResource adminUserResource = new AdminUserResource(adminUserRepository);
        this.restAdminUserMockMvc = MockMvcBuilders.standaloneSetup(adminUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdminUser createEntity(EntityManager em) {
        AdminUser adminUser = new AdminUser()
            .emailId(DEFAULT_EMAIL_ID);
        return adminUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdminUser createUpdatedEntity(EntityManager em) {
        AdminUser adminUser = new AdminUser()
            .emailId(UPDATED_EMAIL_ID);
        return adminUser;
    }

    @BeforeEach
    public void initTest() {
        adminUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdminUser() throws Exception {
        int databaseSizeBeforeCreate = adminUserRepository.findAll().size();

        // Create the AdminUser
        restAdminUserMockMvc.perform(post("/api/admin-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminUser)))
            .andExpect(status().isCreated());

        // Validate the AdminUser in the database
        List<AdminUser> adminUserList = adminUserRepository.findAll();
        assertThat(adminUserList).hasSize(databaseSizeBeforeCreate + 1);
        AdminUser testAdminUser = adminUserList.get(adminUserList.size() - 1);
        assertThat(testAdminUser.getEmailId()).isEqualTo(DEFAULT_EMAIL_ID);
    }

    @Test
    @Transactional
    public void createAdminUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = adminUserRepository.findAll().size();

        // Create the AdminUser with an existing ID
        adminUser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdminUserMockMvc.perform(post("/api/admin-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminUser)))
            .andExpect(status().isBadRequest());

        // Validate the AdminUser in the database
        List<AdminUser> adminUserList = adminUserRepository.findAll();
        assertThat(adminUserList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAdminUsers() throws Exception {
        // Initialize the database
        adminUserRepository.saveAndFlush(adminUser);

        // Get all the adminUserList
        restAdminUserMockMvc.perform(get("/api/admin-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adminUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].emailId").value(hasItem(DEFAULT_EMAIL_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getAdminUser() throws Exception {
        // Initialize the database
        adminUserRepository.saveAndFlush(adminUser);

        // Get the adminUser
        restAdminUserMockMvc.perform(get("/api/admin-users/{id}", adminUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(adminUser.getId().intValue()))
            .andExpect(jsonPath("$.emailId").value(DEFAULT_EMAIL_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdminUser() throws Exception {
        // Get the adminUser
        restAdminUserMockMvc.perform(get("/api/admin-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdminUser() throws Exception {
        // Initialize the database
        adminUserRepository.saveAndFlush(adminUser);

        int databaseSizeBeforeUpdate = adminUserRepository.findAll().size();

        // Update the adminUser
        AdminUser updatedAdminUser = adminUserRepository.findById(adminUser.getId()).get();
        // Disconnect from session so that the updates on updatedAdminUser are not directly saved in db
        em.detach(updatedAdminUser);
        updatedAdminUser
            .emailId(UPDATED_EMAIL_ID);

        restAdminUserMockMvc.perform(put("/api/admin-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAdminUser)))
            .andExpect(status().isOk());

        // Validate the AdminUser in the database
        List<AdminUser> adminUserList = adminUserRepository.findAll();
        assertThat(adminUserList).hasSize(databaseSizeBeforeUpdate);
        AdminUser testAdminUser = adminUserList.get(adminUserList.size() - 1);
        assertThat(testAdminUser.getEmailId()).isEqualTo(UPDATED_EMAIL_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingAdminUser() throws Exception {
        int databaseSizeBeforeUpdate = adminUserRepository.findAll().size();

        // Create the AdminUser

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdminUserMockMvc.perform(put("/api/admin-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminUser)))
            .andExpect(status().isBadRequest());

        // Validate the AdminUser in the database
        List<AdminUser> adminUserList = adminUserRepository.findAll();
        assertThat(adminUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdminUser() throws Exception {
        // Initialize the database
        adminUserRepository.saveAndFlush(adminUser);

        int databaseSizeBeforeDelete = adminUserRepository.findAll().size();

        // Delete the adminUser
        restAdminUserMockMvc.perform(delete("/api/admin-users/{id}", adminUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AdminUser> adminUserList = adminUserRepository.findAll();
        assertThat(adminUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdminUser.class);
        AdminUser adminUser1 = new AdminUser();
        adminUser1.setId(1L);
        AdminUser adminUser2 = new AdminUser();
        adminUser2.setId(adminUser1.getId());
        assertThat(adminUser1).isEqualTo(adminUser2);
        adminUser2.setId(2L);
        assertThat(adminUser1).isNotEqualTo(adminUser2);
        adminUser1.setId(null);
        assertThat(adminUser1).isNotEqualTo(adminUser2);
    }
}
