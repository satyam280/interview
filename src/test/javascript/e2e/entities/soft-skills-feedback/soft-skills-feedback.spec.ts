/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
    SoftSkillsFeedbackComponentsPage,
    SoftSkillsFeedbackDeleteDialog,
    SoftSkillsFeedbackUpdatePage
} from './soft-skills-feedback.page-object';

const expect = chai.expect;

describe('SoftSkillsFeedback e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let softSkillsFeedbackUpdatePage: SoftSkillsFeedbackUpdatePage;
    let softSkillsFeedbackComponentsPage: SoftSkillsFeedbackComponentsPage;
    let softSkillsFeedbackDeleteDialog: SoftSkillsFeedbackDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load SoftSkillsFeedbacks', async () => {
        await navBarPage.goToEntity('soft-skills-feedback');
        softSkillsFeedbackComponentsPage = new SoftSkillsFeedbackComponentsPage();
        await browser.wait(ec.visibilityOf(softSkillsFeedbackComponentsPage.title), 5000);
        expect(await softSkillsFeedbackComponentsPage.getTitle()).to.eq('interviewApp.softSkillsFeedback.home.title');
    });

    it('should load create SoftSkillsFeedback page', async () => {
        await softSkillsFeedbackComponentsPage.clickOnCreateButton();
        softSkillsFeedbackUpdatePage = new SoftSkillsFeedbackUpdatePage();
        expect(await softSkillsFeedbackUpdatePage.getPageTitle()).to.eq('interviewApp.softSkillsFeedback.home.createOrEditLabel');
        await softSkillsFeedbackUpdatePage.cancel();
    });

    it('should create and save SoftSkillsFeedbacks', async () => {
        const nbButtonsBeforeCreate = await softSkillsFeedbackComponentsPage.countDeleteButtons();

        await softSkillsFeedbackComponentsPage.clickOnCreateButton();
        await promise.all([
            softSkillsFeedbackUpdatePage.communicationSelectLastOption(),
            softSkillsFeedbackUpdatePage.motivationLevelSelectLastOption(),
            softSkillsFeedbackUpdatePage.interviewSelectLastOption()
        ]);
        await softSkillsFeedbackUpdatePage.save();
        expect(await softSkillsFeedbackUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await softSkillsFeedbackComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last SoftSkillsFeedback', async () => {
        const nbButtonsBeforeDelete = await softSkillsFeedbackComponentsPage.countDeleteButtons();
        await softSkillsFeedbackComponentsPage.clickOnLastDeleteButton();

        softSkillsFeedbackDeleteDialog = new SoftSkillsFeedbackDeleteDialog();
        expect(await softSkillsFeedbackDeleteDialog.getDialogTitle()).to.eq('interviewApp.softSkillsFeedback.delete.question');
        await softSkillsFeedbackDeleteDialog.clickOnConfirmButton();

        expect(await softSkillsFeedbackComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
