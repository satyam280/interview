import { element, by, ElementFinder } from 'protractor';

export class SoftSkillsFeedbackComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-soft-skills-feedback div table .btn-danger'));
    title = element.all(by.css('jhi-soft-skills-feedback div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SoftSkillsFeedbackUpdatePage {
    pageTitle = element(by.id('jhi-soft-skills-feedback-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    communicationSelect = element(by.id('field_communication'));
    motivationLevelSelect = element(by.id('field_motivationLevel'));
    interviewSelect = element(by.id('field_interview'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setCommunicationSelect(communication) {
        await this.communicationSelect.sendKeys(communication);
    }

    async getCommunicationSelect() {
        return this.communicationSelect.element(by.css('option:checked')).getText();
    }

    async communicationSelectLastOption() {
        await this.communicationSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setMotivationLevelSelect(motivationLevel) {
        await this.motivationLevelSelect.sendKeys(motivationLevel);
    }

    async getMotivationLevelSelect() {
        return this.motivationLevelSelect.element(by.css('option:checked')).getText();
    }

    async motivationLevelSelectLastOption() {
        await this.motivationLevelSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async interviewSelectLastOption() {
        await this.interviewSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async interviewSelectOption(option) {
        await this.interviewSelect.sendKeys(option);
    }

    getInterviewSelect(): ElementFinder {
        return this.interviewSelect;
    }

    async getInterviewSelectedOption() {
        return this.interviewSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class SoftSkillsFeedbackDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-softSkillsFeedback-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-softSkillsFeedback'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
