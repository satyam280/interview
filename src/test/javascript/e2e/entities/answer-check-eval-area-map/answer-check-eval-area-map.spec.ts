/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
    AnswerCheckEvalAreaMapComponentsPage,
    AnswerCheckEvalAreaMapDeleteDialog,
    AnswerCheckEvalAreaMapUpdatePage
} from './answer-check-eval-area-map.page-object';

const expect = chai.expect;

describe('AnswerCheckEvalAreaMap e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let answerCheckEvalAreaMapUpdatePage: AnswerCheckEvalAreaMapUpdatePage;
    let answerCheckEvalAreaMapComponentsPage: AnswerCheckEvalAreaMapComponentsPage;
    let answerCheckEvalAreaMapDeleteDialog: AnswerCheckEvalAreaMapDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load AnswerCheckEvalAreaMaps', async () => {
        await navBarPage.goToEntity('answer-check-eval-area-map');
        answerCheckEvalAreaMapComponentsPage = new AnswerCheckEvalAreaMapComponentsPage();
        await browser.wait(ec.visibilityOf(answerCheckEvalAreaMapComponentsPage.title), 5000);
        expect(await answerCheckEvalAreaMapComponentsPage.getTitle()).to.eq('interviewApp.answerCheckEvalAreaMap.home.title');
    });

    it('should load create AnswerCheckEvalAreaMap page', async () => {
        await answerCheckEvalAreaMapComponentsPage.clickOnCreateButton();
        answerCheckEvalAreaMapUpdatePage = new AnswerCheckEvalAreaMapUpdatePage();
        expect(await answerCheckEvalAreaMapUpdatePage.getPageTitle()).to.eq('interviewApp.answerCheckEvalAreaMap.home.createOrEditLabel');
        await answerCheckEvalAreaMapUpdatePage.cancel();
    });

    it('should create and save AnswerCheckEvalAreaMaps', async () => {
        const nbButtonsBeforeCreate = await answerCheckEvalAreaMapComponentsPage.countDeleteButtons();

        await answerCheckEvalAreaMapComponentsPage.clickOnCreateButton();
        await promise.all([
            answerCheckEvalAreaMapUpdatePage.answerCheckSelectLastOption(),
            answerCheckEvalAreaMapUpdatePage.evaluationAreaSelectLastOption()
        ]);
        await answerCheckEvalAreaMapUpdatePage.save();
        expect(await answerCheckEvalAreaMapUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await answerCheckEvalAreaMapComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last AnswerCheckEvalAreaMap', async () => {
        const nbButtonsBeforeDelete = await answerCheckEvalAreaMapComponentsPage.countDeleteButtons();
        await answerCheckEvalAreaMapComponentsPage.clickOnLastDeleteButton();

        answerCheckEvalAreaMapDeleteDialog = new AnswerCheckEvalAreaMapDeleteDialog();
        expect(await answerCheckEvalAreaMapDeleteDialog.getDialogTitle()).to.eq('interviewApp.answerCheckEvalAreaMap.delete.question');
        await answerCheckEvalAreaMapDeleteDialog.clickOnConfirmButton();

        expect(await answerCheckEvalAreaMapComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
