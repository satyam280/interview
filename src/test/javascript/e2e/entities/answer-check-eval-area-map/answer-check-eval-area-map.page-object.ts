import { element, by, ElementFinder } from 'protractor';

export class AnswerCheckEvalAreaMapComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-answer-check-eval-area-map div table .btn-danger'));
    title = element.all(by.css('jhi-answer-check-eval-area-map div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AnswerCheckEvalAreaMapUpdatePage {
    pageTitle = element(by.id('jhi-answer-check-eval-area-map-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    answerCheckSelect = element(by.id('field_answerCheck'));
    evaluationAreaSelect = element(by.id('field_evaluationArea'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async answerCheckSelectLastOption() {
        await this.answerCheckSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async answerCheckSelectOption(option) {
        await this.answerCheckSelect.sendKeys(option);
    }

    getAnswerCheckSelect(): ElementFinder {
        return this.answerCheckSelect;
    }

    async getAnswerCheckSelectedOption() {
        return this.answerCheckSelect.element(by.css('option:checked')).getText();
    }

    async evaluationAreaSelectLastOption() {
        await this.evaluationAreaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async evaluationAreaSelectOption(option) {
        await this.evaluationAreaSelect.sendKeys(option);
    }

    getEvaluationAreaSelect(): ElementFinder {
        return this.evaluationAreaSelect;
    }

    async getEvaluationAreaSelectedOption() {
        return this.evaluationAreaSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AnswerCheckEvalAreaMapDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-answerCheckEvalAreaMap-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-answerCheckEvalAreaMap'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
