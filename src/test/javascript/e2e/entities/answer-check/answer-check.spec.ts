/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AnswerCheckComponentsPage, AnswerCheckDeleteDialog, AnswerCheckUpdatePage } from './answer-check.page-object';

const expect = chai.expect;

describe('AnswerCheck e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let answerCheckUpdatePage: AnswerCheckUpdatePage;
    let answerCheckComponentsPage: AnswerCheckComponentsPage;
    let answerCheckDeleteDialog: AnswerCheckDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load AnswerChecks', async () => {
        await navBarPage.goToEntity('answer-check');
        answerCheckComponentsPage = new AnswerCheckComponentsPage();
        await browser.wait(ec.visibilityOf(answerCheckComponentsPage.title), 5000);
        expect(await answerCheckComponentsPage.getTitle()).to.eq('interviewApp.answerCheck.home.title');
    });

    it('should load create AnswerCheck page', async () => {
        await answerCheckComponentsPage.clickOnCreateButton();
        answerCheckUpdatePage = new AnswerCheckUpdatePage();
        expect(await answerCheckUpdatePage.getPageTitle()).to.eq('interviewApp.answerCheck.home.createOrEditLabel');
        await answerCheckUpdatePage.cancel();
    });

    it('should create and save AnswerChecks', async () => {
        const nbButtonsBeforeCreate = await answerCheckComponentsPage.countDeleteButtons();

        await answerCheckComponentsPage.clickOnCreateButton();
        await promise.all([answerCheckUpdatePage.setNameInput('name'), answerCheckUpdatePage.questionSelectLastOption()]);
        expect(await answerCheckUpdatePage.getNameInput()).to.eq('name');
        await answerCheckUpdatePage.save();
        expect(await answerCheckUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await answerCheckComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last AnswerCheck', async () => {
        const nbButtonsBeforeDelete = await answerCheckComponentsPage.countDeleteButtons();
        await answerCheckComponentsPage.clickOnLastDeleteButton();

        answerCheckDeleteDialog = new AnswerCheckDeleteDialog();
        expect(await answerCheckDeleteDialog.getDialogTitle()).to.eq('interviewApp.answerCheck.delete.question');
        await answerCheckDeleteDialog.clickOnConfirmButton();

        expect(await answerCheckComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
