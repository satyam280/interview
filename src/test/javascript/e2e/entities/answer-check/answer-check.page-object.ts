import { element, by, ElementFinder } from 'protractor';

export class AnswerCheckComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-answer-check div table .btn-danger'));
    title = element.all(by.css('jhi-answer-check div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AnswerCheckUpdatePage {
    pageTitle = element(by.id('jhi-answer-check-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nameInput = element(by.id('field_name'));
    questionSelect = element(by.id('field_question'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNameInput(name) {
        await this.nameInput.sendKeys(name);
    }

    async getNameInput() {
        return this.nameInput.getAttribute('value');
    }

    async questionSelectLastOption() {
        await this.questionSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async questionSelectOption(option) {
        await this.questionSelect.sendKeys(option);
    }

    getQuestionSelect(): ElementFinder {
        return this.questionSelect;
    }

    async getQuestionSelectedOption() {
        return this.questionSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AnswerCheckDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-answerCheck-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-answerCheck'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
