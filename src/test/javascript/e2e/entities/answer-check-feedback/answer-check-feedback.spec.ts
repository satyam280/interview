/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
    AnswerCheckFeedbackComponentsPage,
    AnswerCheckFeedbackDeleteDialog,
    AnswerCheckFeedbackUpdatePage
} from './answer-check-feedback.page-object';

const expect = chai.expect;

describe('AnswerCheckFeedback e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let answerCheckFeedbackUpdatePage: AnswerCheckFeedbackUpdatePage;
    let answerCheckFeedbackComponentsPage: AnswerCheckFeedbackComponentsPage;
    let answerCheckFeedbackDeleteDialog: AnswerCheckFeedbackDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load AnswerCheckFeedbacks', async () => {
        await navBarPage.goToEntity('answer-check-feedback');
        answerCheckFeedbackComponentsPage = new AnswerCheckFeedbackComponentsPage();
        await browser.wait(ec.visibilityOf(answerCheckFeedbackComponentsPage.title), 5000);
        expect(await answerCheckFeedbackComponentsPage.getTitle()).to.eq('interviewApp.answerCheckFeedback.home.title');
    });

    it('should load create AnswerCheckFeedback page', async () => {
        await answerCheckFeedbackComponentsPage.clickOnCreateButton();
        answerCheckFeedbackUpdatePage = new AnswerCheckFeedbackUpdatePage();
        expect(await answerCheckFeedbackUpdatePage.getPageTitle()).to.eq('interviewApp.answerCheckFeedback.home.createOrEditLabel');
        await answerCheckFeedbackUpdatePage.cancel();
    });

    it('should create and save AnswerCheckFeedbacks', async () => {
        const nbButtonsBeforeCreate = await answerCheckFeedbackComponentsPage.countDeleteButtons();

        await answerCheckFeedbackComponentsPage.clickOnCreateButton();
        await promise.all([
            answerCheckFeedbackUpdatePage.answer_qualitySelectLastOption(),
            answerCheckFeedbackUpdatePage.interviewSelectLastOption(),
            answerCheckFeedbackUpdatePage.answerCheckSelectLastOption()
        ]);
        await answerCheckFeedbackUpdatePage.save();
        expect(await answerCheckFeedbackUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await answerCheckFeedbackComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last AnswerCheckFeedback', async () => {
        const nbButtonsBeforeDelete = await answerCheckFeedbackComponentsPage.countDeleteButtons();
        await answerCheckFeedbackComponentsPage.clickOnLastDeleteButton();

        answerCheckFeedbackDeleteDialog = new AnswerCheckFeedbackDeleteDialog();
        expect(await answerCheckFeedbackDeleteDialog.getDialogTitle()).to.eq('interviewApp.answerCheckFeedback.delete.question');
        await answerCheckFeedbackDeleteDialog.clickOnConfirmButton();

        expect(await answerCheckFeedbackComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
