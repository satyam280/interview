import { element, by, ElementFinder } from 'protractor';

export class AnswerCheckFeedbackComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-answer-check-feedback div table .btn-danger'));
    title = element.all(by.css('jhi-answer-check-feedback div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AnswerCheckFeedbackUpdatePage {
    pageTitle = element(by.id('jhi-answer-check-feedback-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    answer_qualitySelect = element(by.id('field_answer_quality'));
    interviewSelect = element(by.id('field_interview'));
    answerCheckSelect = element(by.id('field_answerCheck'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setAnswer_qualitySelect(answer_quality) {
        await this.answer_qualitySelect.sendKeys(answer_quality);
    }

    async getAnswer_qualitySelect() {
        return this.answer_qualitySelect.element(by.css('option:checked')).getText();
    }

    async answer_qualitySelectLastOption() {
        await this.answer_qualitySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async interviewSelectLastOption() {
        await this.interviewSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async interviewSelectOption(option) {
        await this.interviewSelect.sendKeys(option);
    }

    getInterviewSelect(): ElementFinder {
        return this.interviewSelect;
    }

    async getInterviewSelectedOption() {
        return this.interviewSelect.element(by.css('option:checked')).getText();
    }

    async answerCheckSelectLastOption() {
        await this.answerCheckSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async answerCheckSelectOption(option) {
        await this.answerCheckSelect.sendKeys(option);
    }

    getAnswerCheckSelect(): ElementFinder {
        return this.answerCheckSelect;
    }

    async getAnswerCheckSelectedOption() {
        return this.answerCheckSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AnswerCheckFeedbackDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-answerCheckFeedback-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-answerCheckFeedback'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
