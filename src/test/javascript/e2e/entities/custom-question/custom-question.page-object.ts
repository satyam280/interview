import { element, by, ElementFinder } from 'protractor';

export class CustomQuestionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-custom-question div table .btn-danger'));
    title = element.all(by.css('jhi-custom-question div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class CustomQuestionUpdatePage {
    pageTitle = element(by.id('jhi-custom-question-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    question_textInput = element(by.id('field_question_text'));
    answerQualitySelect = element(by.id('field_answerQuality'));
    answerTextInput = element(by.id('field_answerText'));
    startedAtInput = element(by.id('field_startedAt'));
    finishedAtInput = element(by.id('field_finishedAt'));
    interviewSelect = element(by.id('field_interview'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setQuestion_textInput(question_text) {
        await this.question_textInput.sendKeys(question_text);
    }

    async getQuestion_textInput() {
        return this.question_textInput.getAttribute('value');
    }

    async setAnswerQualitySelect(answerQuality) {
        await this.answerQualitySelect.sendKeys(answerQuality);
    }

    async getAnswerQualitySelect() {
        return this.answerQualitySelect.element(by.css('option:checked')).getText();
    }

    async answerQualitySelectLastOption() {
        await this.answerQualitySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setAnswerTextInput(answerText) {
        await this.answerTextInput.sendKeys(answerText);
    }

    async getAnswerTextInput() {
        return this.answerTextInput.getAttribute('value');
    }

    async setStartedAtInput(startedAt) {
        await this.startedAtInput.sendKeys(startedAt);
    }

    async getStartedAtInput() {
        return this.startedAtInput.getAttribute('value');
    }

    async setFinishedAtInput(finishedAt) {
        await this.finishedAtInput.sendKeys(finishedAt);
    }

    async getFinishedAtInput() {
        return this.finishedAtInput.getAttribute('value');
    }

    async interviewSelectLastOption() {
        await this.interviewSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async interviewSelectOption(option) {
        await this.interviewSelect.sendKeys(option);
    }

    getInterviewSelect(): ElementFinder {
        return this.interviewSelect;
    }

    async getInterviewSelectedOption() {
        return this.interviewSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class CustomQuestionDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-customQuestion-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-customQuestion'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
