/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CustomQuestionComponentsPage, CustomQuestionDeleteDialog, CustomQuestionUpdatePage } from './custom-question.page-object';

const expect = chai.expect;

describe('CustomQuestion e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let customQuestionUpdatePage: CustomQuestionUpdatePage;
    let customQuestionComponentsPage: CustomQuestionComponentsPage;
    let customQuestionDeleteDialog: CustomQuestionDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load CustomQuestions', async () => {
        await navBarPage.goToEntity('custom-question');
        customQuestionComponentsPage = new CustomQuestionComponentsPage();
        await browser.wait(ec.visibilityOf(customQuestionComponentsPage.title), 5000);
        expect(await customQuestionComponentsPage.getTitle()).to.eq('interviewApp.customQuestion.home.title');
    });

    it('should load create CustomQuestion page', async () => {
        await customQuestionComponentsPage.clickOnCreateButton();
        customQuestionUpdatePage = new CustomQuestionUpdatePage();
        expect(await customQuestionUpdatePage.getPageTitle()).to.eq('interviewApp.customQuestion.home.createOrEditLabel');
        await customQuestionUpdatePage.cancel();
    });

    it('should create and save CustomQuestions', async () => {
        const nbButtonsBeforeCreate = await customQuestionComponentsPage.countDeleteButtons();

        await customQuestionComponentsPage.clickOnCreateButton();
        await promise.all([
            customQuestionUpdatePage.setQuestion_textInput('question_text'),
            customQuestionUpdatePage.answerQualitySelectLastOption(),
            customQuestionUpdatePage.setAnswerTextInput('answerText'),
            customQuestionUpdatePage.setStartedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            customQuestionUpdatePage.setFinishedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            customQuestionUpdatePage.interviewSelectLastOption()
        ]);
        expect(await customQuestionUpdatePage.getQuestion_textInput()).to.eq('question_text');
        expect(await customQuestionUpdatePage.getAnswerTextInput()).to.eq('answerText');
        expect(await customQuestionUpdatePage.getStartedAtInput()).to.contain('2001-01-01T02:30');
        expect(await customQuestionUpdatePage.getFinishedAtInput()).to.contain('2001-01-01T02:30');
        await customQuestionUpdatePage.save();
        expect(await customQuestionUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await customQuestionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last CustomQuestion', async () => {
        const nbButtonsBeforeDelete = await customQuestionComponentsPage.countDeleteButtons();
        await customQuestionComponentsPage.clickOnLastDeleteButton();

        customQuestionDeleteDialog = new CustomQuestionDeleteDialog();
        expect(await customQuestionDeleteDialog.getDialogTitle()).to.eq('interviewApp.customQuestion.delete.question');
        await customQuestionDeleteDialog.clickOnConfirmButton();

        expect(await customQuestionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
