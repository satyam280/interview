/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EvaluationAreaComponentsPage, EvaluationAreaDeleteDialog, EvaluationAreaUpdatePage } from './evaluation-area.page-object';

const expect = chai.expect;

describe('EvaluationArea e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let evaluationAreaUpdatePage: EvaluationAreaUpdatePage;
    let evaluationAreaComponentsPage: EvaluationAreaComponentsPage;
    let evaluationAreaDeleteDialog: EvaluationAreaDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load EvaluationAreas', async () => {
        await navBarPage.goToEntity('evaluation-area');
        evaluationAreaComponentsPage = new EvaluationAreaComponentsPage();
        await browser.wait(ec.visibilityOf(evaluationAreaComponentsPage.title), 5000);
        expect(await evaluationAreaComponentsPage.getTitle()).to.eq('interviewApp.evaluationArea.home.title');
    });

    it('should load create EvaluationArea page', async () => {
        await evaluationAreaComponentsPage.clickOnCreateButton();
        evaluationAreaUpdatePage = new EvaluationAreaUpdatePage();
        expect(await evaluationAreaUpdatePage.getPageTitle()).to.eq('interviewApp.evaluationArea.home.createOrEditLabel');
        await evaluationAreaUpdatePage.cancel();
    });

    it('should create and save EvaluationAreas', async () => {
        const nbButtonsBeforeCreate = await evaluationAreaComponentsPage.countDeleteButtons();

        await evaluationAreaComponentsPage.clickOnCreateButton();
        await promise.all([evaluationAreaUpdatePage.setNameInput('name')]);
        expect(await evaluationAreaUpdatePage.getNameInput()).to.eq('name');
        await evaluationAreaUpdatePage.save();
        expect(await evaluationAreaUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await evaluationAreaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last EvaluationArea', async () => {
        const nbButtonsBeforeDelete = await evaluationAreaComponentsPage.countDeleteButtons();
        await evaluationAreaComponentsPage.clickOnLastDeleteButton();

        evaluationAreaDeleteDialog = new EvaluationAreaDeleteDialog();
        expect(await evaluationAreaDeleteDialog.getDialogTitle()).to.eq('interviewApp.evaluationArea.delete.question');
        await evaluationAreaDeleteDialog.clickOnConfirmButton();

        expect(await evaluationAreaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
