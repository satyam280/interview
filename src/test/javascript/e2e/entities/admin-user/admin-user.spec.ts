/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AdminUserComponentsPage, AdminUserDeleteDialog, AdminUserUpdatePage } from './admin-user.page-object';

const expect = chai.expect;

describe('AdminUser e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let adminUserUpdatePage: AdminUserUpdatePage;
    let adminUserComponentsPage: AdminUserComponentsPage;
    let adminUserDeleteDialog: AdminUserDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load AdminUsers', async () => {
        await navBarPage.goToEntity('admin-user');
        adminUserComponentsPage = new AdminUserComponentsPage();
        await browser.wait(ec.visibilityOf(adminUserComponentsPage.title), 5000);
        expect(await adminUserComponentsPage.getTitle()).to.eq('interviewApp.adminUser.home.title');
    });

    it('should load create AdminUser page', async () => {
        await adminUserComponentsPage.clickOnCreateButton();
        adminUserUpdatePage = new AdminUserUpdatePage();
        expect(await adminUserUpdatePage.getPageTitle()).to.eq('interviewApp.adminUser.home.createOrEditLabel');
        await adminUserUpdatePage.cancel();
    });

    it('should create and save AdminUsers', async () => {
        const nbButtonsBeforeCreate = await adminUserComponentsPage.countDeleteButtons();

        await adminUserComponentsPage.clickOnCreateButton();
        await promise.all([adminUserUpdatePage.setEmailIdInput('emailId')]);
        expect(await adminUserUpdatePage.getEmailIdInput()).to.eq('emailId', 'Expected EmailId value to be equals to emailId');
        await adminUserUpdatePage.save();
        expect(await adminUserUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await adminUserComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });

    it('should delete last AdminUser', async () => {
        const nbButtonsBeforeDelete = await adminUserComponentsPage.countDeleteButtons();
        await adminUserComponentsPage.clickOnLastDeleteButton();

        adminUserDeleteDialog = new AdminUserDeleteDialog();
        expect(await adminUserDeleteDialog.getDialogTitle()).to.eq('interviewApp.adminUser.delete.question');
        await adminUserDeleteDialog.clickOnConfirmButton();

        expect(await adminUserComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
