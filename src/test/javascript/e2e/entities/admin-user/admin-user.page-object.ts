import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class AdminUserComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-admin-user div table .btn-danger'));
    title = element.all(by.css('jhi-admin-user div h2#page-heading span')).first();

    async clickOnCreateButton(timeout?: number) {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton(timeout?: number) {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdminUserUpdatePage {
    pageTitle = element(by.id('jhi-admin-user-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    emailIdInput = element(by.id('field_emailId'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setEmailIdInput(emailId) {
        await this.emailIdInput.sendKeys(emailId);
    }

    async getEmailIdInput() {
        return await this.emailIdInput.getAttribute('value');
    }

    async save(timeout?: number) {
        await this.saveButton.click();
    }

    async cancel(timeout?: number) {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AdminUserDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-adminUser-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-adminUser'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton(timeout?: number) {
        await this.confirmButton.click();
    }
}
