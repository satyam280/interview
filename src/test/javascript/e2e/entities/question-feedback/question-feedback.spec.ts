/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { QuestionFeedbackComponentsPage, QuestionFeedbackDeleteDialog, QuestionFeedbackUpdatePage } from './question-feedback.page-object';

const expect = chai.expect;

describe('QuestionFeedback e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let questionFeedbackUpdatePage: QuestionFeedbackUpdatePage;
    let questionFeedbackComponentsPage: QuestionFeedbackComponentsPage;
    let questionFeedbackDeleteDialog: QuestionFeedbackDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load QuestionFeedbacks', async () => {
        await navBarPage.goToEntity('question-feedback');
        questionFeedbackComponentsPage = new QuestionFeedbackComponentsPage();
        await browser.wait(ec.visibilityOf(questionFeedbackComponentsPage.title), 5000);
        expect(await questionFeedbackComponentsPage.getTitle()).to.eq('interviewApp.questionFeedback.home.title');
    });

    it('should load create QuestionFeedback page', async () => {
        await questionFeedbackComponentsPage.clickOnCreateButton();
        questionFeedbackUpdatePage = new QuestionFeedbackUpdatePage();
        expect(await questionFeedbackUpdatePage.getPageTitle()).to.eq('interviewApp.questionFeedback.home.createOrEditLabel');
        await questionFeedbackUpdatePage.cancel();
    });

    it('should create and save QuestionFeedbacks', async () => {
        const nbButtonsBeforeCreate = await questionFeedbackComponentsPage.countDeleteButtons();

        await questionFeedbackComponentsPage.clickOnCreateButton();
        await promise.all([
            questionFeedbackUpdatePage.answer_qualitySelectLastOption(),
            questionFeedbackUpdatePage.setStartedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            questionFeedbackUpdatePage.setFinishedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            questionFeedbackUpdatePage.setDurationInput('5'),
            questionFeedbackUpdatePage.setDescriptionInput('description'),
            questionFeedbackUpdatePage.interviewSelectLastOption(),
            questionFeedbackUpdatePage.questionSelectLastOption()
        ]);
        expect(await questionFeedbackUpdatePage.getStartedAtInput()).to.contain('2001-01-01T02:30');
        expect(await questionFeedbackUpdatePage.getFinishedAtInput()).to.contain('2001-01-01T02:30');
        expect(await questionFeedbackUpdatePage.getDurationInput()).to.eq('5');
        expect(await questionFeedbackUpdatePage.getDescriptionInput()).to.eq('description');
        await questionFeedbackUpdatePage.save();
        expect(await questionFeedbackUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await questionFeedbackComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last QuestionFeedback', async () => {
        const nbButtonsBeforeDelete = await questionFeedbackComponentsPage.countDeleteButtons();
        await questionFeedbackComponentsPage.clickOnLastDeleteButton();

        questionFeedbackDeleteDialog = new QuestionFeedbackDeleteDialog();
        expect(await questionFeedbackDeleteDialog.getDialogTitle()).to.eq('interviewApp.questionFeedback.delete.question');
        await questionFeedbackDeleteDialog.clickOnConfirmButton();

        expect(await questionFeedbackComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
