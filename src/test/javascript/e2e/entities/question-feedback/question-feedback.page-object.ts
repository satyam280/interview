import { element, by, ElementFinder } from 'protractor';

export class QuestionFeedbackComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-question-feedback div table .btn-danger'));
    title = element.all(by.css('jhi-question-feedback div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class QuestionFeedbackUpdatePage {
    pageTitle = element(by.id('jhi-question-feedback-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    answer_qualitySelect = element(by.id('field_answer_quality'));
    startedAtInput = element(by.id('field_startedAt'));
    finishedAtInput = element(by.id('field_finishedAt'));
    durationInput = element(by.id('field_duration'));
    descriptionInput = element(by.id('field_description'));
    interviewSelect = element(by.id('field_interview'));
    questionSelect = element(by.id('field_question'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setAnswer_qualitySelect(answer_quality) {
        await this.answer_qualitySelect.sendKeys(answer_quality);
    }

    async getAnswer_qualitySelect() {
        return this.answer_qualitySelect.element(by.css('option:checked')).getText();
    }

    async answer_qualitySelectLastOption() {
        await this.answer_qualitySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setStartedAtInput(startedAt) {
        await this.startedAtInput.sendKeys(startedAt);
    }

    async getStartedAtInput() {
        return this.startedAtInput.getAttribute('value');
    }

    async setFinishedAtInput(finishedAt) {
        await this.finishedAtInput.sendKeys(finishedAt);
    }

    async getFinishedAtInput() {
        return this.finishedAtInput.getAttribute('value');
    }

    async setDurationInput(duration) {
        await this.durationInput.sendKeys(duration);
    }

    async getDurationInput() {
        return this.durationInput.getAttribute('value');
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return this.descriptionInput.getAttribute('value');
    }

    async interviewSelectLastOption() {
        await this.interviewSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async interviewSelectOption(option) {
        await this.interviewSelect.sendKeys(option);
    }

    getInterviewSelect(): ElementFinder {
        return this.interviewSelect;
    }

    async getInterviewSelectedOption() {
        return this.interviewSelect.element(by.css('option:checked')).getText();
    }

    async questionSelectLastOption() {
        await this.questionSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async questionSelectOption(option) {
        await this.questionSelect.sendKeys(option);
    }

    getQuestionSelect(): ElementFinder {
        return this.questionSelect;
    }

    async getQuestionSelectedOption() {
        return this.questionSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class QuestionFeedbackDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-questionFeedback-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-questionFeedback'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
