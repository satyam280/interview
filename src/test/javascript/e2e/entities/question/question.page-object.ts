import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class QuestionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-question div table .btn-danger'));
    title = element.all(by.css('jhi-question div h2#page-heading span')).first();

    async clickOnCreateButton(timeout?: number) {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton(timeout?: number) {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class QuestionUpdatePage {
    pageTitle = element(by.id('jhi-question-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    min_experienceInput = element(by.id('field_min_experience'));
    descriptionInput = element(by.id('field_description'));
    expected_answersInput = element(by.id('field_expected_answers'));
    sequenceInput = element(by.id('field_sequence'));
    isActiveInput = element(by.id('field_isActive'));
    skillSelect = element(by.id('field_skill'));
    topicSelect = element(by.id('field_topic'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setMin_experienceInput(min_experience) {
        await this.min_experienceInput.sendKeys(min_experience);
    }

    async getMin_experienceInput() {
        return await this.min_experienceInput.getAttribute('value');
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return await this.descriptionInput.getAttribute('value');
    }

    async setExpected_answersInput(expected_answers) {
        await this.expected_answersInput.sendKeys(expected_answers);
    }

    async getExpected_answersInput() {
        return await this.expected_answersInput.getAttribute('value');
    }

    async setSequenceInput(sequence) {
        await this.sequenceInput.sendKeys(sequence);
    }

    async getSequenceInput() {
        return await this.sequenceInput.getAttribute('value');
    }

    getIsActiveInput(timeout?: number) {
        return this.isActiveInput;
    }

    async skillSelectLastOption(timeout?: number) {
        await this.skillSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async skillSelectOption(option) {
        await this.skillSelect.sendKeys(option);
    }

    getSkillSelect(): ElementFinder {
        return this.skillSelect;
    }

    async getSkillSelectedOption() {
        return await this.skillSelect.element(by.css('option:checked')).getText();
    }

    async topicSelectLastOption(timeout?: number) {
        await this.topicSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async topicSelectOption(option) {
        await this.topicSelect.sendKeys(option);
    }

    getTopicSelect(): ElementFinder {
        return this.topicSelect;
    }

    async getTopicSelectedOption() {
        return await this.topicSelect.element(by.css('option:checked')).getText();
    }

    async save(timeout?: number) {
        await this.saveButton.click();
    }

    async cancel(timeout?: number) {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class QuestionDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-question-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-question'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton(timeout?: number) {
        await this.confirmButton.click();
    }
}
