/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { QuestionComponentsPage, QuestionDeleteDialog, QuestionUpdatePage } from './question.page-object';

const expect = chai.expect;

describe('Question e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let questionUpdatePage: QuestionUpdatePage;
    let questionComponentsPage: QuestionComponentsPage;
    let questionDeleteDialog: QuestionDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Questions', async () => {
        await navBarPage.goToEntity('question');
        questionComponentsPage = new QuestionComponentsPage();
        await browser.wait(ec.visibilityOf(questionComponentsPage.title), 5000);
        expect(await questionComponentsPage.getTitle()).to.eq('interviewApp.question.home.title');
    });

    it('should load create Question page', async () => {
        await questionComponentsPage.clickOnCreateButton();
        questionUpdatePage = new QuestionUpdatePage();
        expect(await questionUpdatePage.getPageTitle()).to.eq('interviewApp.question.home.createOrEditLabel');
        await questionUpdatePage.cancel();
    });

    it('should create and save Questions', async () => {
        const nbButtonsBeforeCreate = await questionComponentsPage.countDeleteButtons();

        await questionComponentsPage.clickOnCreateButton();
        await promise.all([
            questionUpdatePage.setMin_experienceInput('5'),
            questionUpdatePage.setDescriptionInput('description'),
            questionUpdatePage.setExpected_answersInput('expected_answers'),
            questionUpdatePage.setSequenceInput('5'),
            questionUpdatePage.skillSelectLastOption(),
            questionUpdatePage.topicSelectLastOption()
        ]);
        expect(await questionUpdatePage.getMin_experienceInput()).to.eq('5', 'Expected min_experience value to be equals to 5');
        expect(await questionUpdatePage.getDescriptionInput()).to.eq(
            'description',
            'Expected Description value to be equals to description'
        );
        expect(await questionUpdatePage.getExpected_answersInput()).to.eq(
            'expected_answers',
            'Expected Expected_answers value to be equals to expected_answers'
        );
        expect(await questionUpdatePage.getSequenceInput()).to.eq('5', 'Expected sequence value to be equals to 5');
        const selectedIsActive = questionUpdatePage.getIsActiveInput();
        if (await selectedIsActive.isSelected()) {
            await questionUpdatePage.getIsActiveInput().click();
            expect(await questionUpdatePage.getIsActiveInput().isSelected(), 'Expected isActive not to be selected').to.be.false;
        } else {
            await questionUpdatePage.getIsActiveInput().click();
            expect(await questionUpdatePage.getIsActiveInput().isSelected(), 'Expected isActive to be selected').to.be.true;
        }
        await questionUpdatePage.save();
        expect(await questionUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await questionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });

    it('should delete last Question', async () => {
        const nbButtonsBeforeDelete = await questionComponentsPage.countDeleteButtons();
        await questionComponentsPage.clickOnLastDeleteButton();

        questionDeleteDialog = new QuestionDeleteDialog();
        expect(await questionDeleteDialog.getDialogTitle()).to.eq('interviewApp.question.delete.question');
        await questionDeleteDialog.clickOnConfirmButton();

        expect(await questionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
