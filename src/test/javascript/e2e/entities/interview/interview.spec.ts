/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { InterviewComponentsPage, InterviewDeleteDialog, InterviewUpdatePage } from './interview.page-object';

const expect = chai.expect;

describe('Interview e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let interviewUpdatePage: InterviewUpdatePage;
    let interviewComponentsPage: InterviewComponentsPage;
    let interviewDeleteDialog: InterviewDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Interviews', async () => {
        await navBarPage.goToEntity('interview');
        interviewComponentsPage = new InterviewComponentsPage();
        await browser.wait(ec.visibilityOf(interviewComponentsPage.title), 5000);
        expect(await interviewComponentsPage.getTitle()).to.eq('interviewApp.interview.home.title');
    });

    it('should load create Interview page', async () => {
        await interviewComponentsPage.clickOnCreateButton();
        interviewUpdatePage = new InterviewUpdatePage();
        expect(await interviewUpdatePage.getPageTitle()).to.eq('interviewApp.interview.home.createOrEditLabel');
        await interviewUpdatePage.cancel();
    });

    it('should create and save Interviews', async () => {
        const nbButtonsBeforeCreate = await interviewComponentsPage.countDeleteButtons();

        await interviewComponentsPage.clickOnCreateButton();
        await promise.all([
            interviewUpdatePage.setInterviewerInput('interviewer'),
            interviewUpdatePage.setCandidateInput('candidate'),
            interviewUpdatePage.setCreatedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            interviewUpdatePage.setStartedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            interviewUpdatePage.setFinishedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            interviewUpdatePage.statusSelectLastOption(),
            interviewUpdatePage.resultSelectLastOption(),
            interviewUpdatePage.setFeedbackInput('feedback'),
            interviewUpdatePage.setExperienceInput('5'),
            interviewUpdatePage.skillSelectLastOption()
        ]);
        expect(await interviewUpdatePage.getInterviewerInput()).to.eq('interviewer');
        expect(await interviewUpdatePage.getCandidateInput()).to.eq('candidate');
        expect(await interviewUpdatePage.getCreatedAtInput()).to.contain('2001-01-01T02:30');
        expect(await interviewUpdatePage.getStartedAtInput()).to.contain('2001-01-01T02:30');
        expect(await interviewUpdatePage.getFinishedAtInput()).to.contain('2001-01-01T02:30');
        expect(await interviewUpdatePage.getFeedbackInput()).to.eq('feedback');
        expect(await interviewUpdatePage.getExperienceInput()).to.eq('5');
        await interviewUpdatePage.save();
        expect(await interviewUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await interviewComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Interview', async () => {
        const nbButtonsBeforeDelete = await interviewComponentsPage.countDeleteButtons();
        await interviewComponentsPage.clickOnLastDeleteButton();

        interviewDeleteDialog = new InterviewDeleteDialog();
        expect(await interviewDeleteDialog.getDialogTitle()).to.eq('interviewApp.interview.delete.question');
        await interviewDeleteDialog.clickOnConfirmButton();

        expect(await interviewComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
