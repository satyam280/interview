import { element, by, ElementFinder } from 'protractor';

export class InterviewComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-interview div table .btn-danger'));
    title = element.all(by.css('jhi-interview div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class InterviewUpdatePage {
    pageTitle = element(by.id('jhi-interview-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    interviewerInput = element(by.id('field_interviewer'));
    candidateInput = element(by.id('field_candidate'));
    createdAtInput = element(by.id('field_createdAt'));
    startedAtInput = element(by.id('field_startedAt'));
    finishedAtInput = element(by.id('field_finishedAt'));
    statusSelect = element(by.id('field_status'));
    resultSelect = element(by.id('field_result'));
    feedbackInput = element(by.id('field_feedback'));
    experienceInput = element(by.id('field_experience'));
    skillSelect = element(by.id('field_skill'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setInterviewerInput(interviewer) {
        await this.interviewerInput.sendKeys(interviewer);
    }

    async getInterviewerInput() {
        return this.interviewerInput.getAttribute('value');
    }

    async setCandidateInput(candidate) {
        await this.candidateInput.sendKeys(candidate);
    }

    async getCandidateInput() {
        return this.candidateInput.getAttribute('value');
    }

    async setCreatedAtInput(createdAt) {
        await this.createdAtInput.sendKeys(createdAt);
    }

    async getCreatedAtInput() {
        return this.createdAtInput.getAttribute('value');
    }

    async setStartedAtInput(startedAt) {
        await this.startedAtInput.sendKeys(startedAt);
    }

    async getStartedAtInput() {
        return this.startedAtInput.getAttribute('value');
    }

    async setFinishedAtInput(finishedAt) {
        await this.finishedAtInput.sendKeys(finishedAt);
    }

    async getFinishedAtInput() {
        return this.finishedAtInput.getAttribute('value');
    }

    async setStatusSelect(status) {
        await this.statusSelect.sendKeys(status);
    }

    async getStatusSelect() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    async statusSelectLastOption() {
        await this.statusSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setResultSelect(result) {
        await this.resultSelect.sendKeys(result);
    }

    async getResultSelect() {
        return this.resultSelect.element(by.css('option:checked')).getText();
    }

    async resultSelectLastOption() {
        await this.resultSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setFeedbackInput(feedback) {
        await this.feedbackInput.sendKeys(feedback);
    }

    async getFeedbackInput() {
        return this.feedbackInput.getAttribute('value');
    }

    async setExperienceInput(experience) {
        await this.experienceInput.sendKeys(experience);
    }

    async getExperienceInput() {
        return this.experienceInput.getAttribute('value');
    }

    async skillSelectLastOption() {
        await this.skillSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async skillSelectOption(option) {
        await this.skillSelect.sendKeys(option);
    }

    getSkillSelect(): ElementFinder {
        return this.skillSelect;
    }

    async getSkillSelectedOption() {
        return this.skillSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class InterviewDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-interview-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-interview'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
