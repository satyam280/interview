/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { CustomQuestionService } from 'app/entities/custom-question/custom-question.service';
import { ICustomQuestion, CustomQuestion, AnswerQuality } from 'app/shared/model/custom-question.model';

describe('Service Tests', () => {
    describe('CustomQuestion Service', () => {
        let injector: TestBed;
        let service: CustomQuestionService;
        let httpMock: HttpTestingController;
        let elemDefault: ICustomQuestion;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(CustomQuestionService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new CustomQuestion(0, 'AAAAAAA', AnswerQuality.EXCELLENT, 'AAAAAAA', currentDate, currentDate);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a CustomQuestion', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        startedAt: currentDate,
                        finishedAt: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new CustomQuestion(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a CustomQuestion', async () => {
                const returnedFromService = Object.assign(
                    {
                        question_text: 'BBBBBB',
                        answerQuality: 'BBBBBB',
                        answerText: 'BBBBBB',
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        startedAt: currentDate,
                        finishedAt: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of CustomQuestion', async () => {
                const returnedFromService = Object.assign(
                    {
                        question_text: 'BBBBBB',
                        answerQuality: 'BBBBBB',
                        answerText: 'BBBBBB',
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        startedAt: currentDate,
                        finishedAt: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a CustomQuestion', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
