/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { CustomQuestionDeleteDialogComponent } from 'app/entities/custom-question/custom-question-delete-dialog.component';
import { CustomQuestionService } from 'app/entities/custom-question/custom-question.service';

describe('Component Tests', () => {
    describe('CustomQuestion Management Delete Component', () => {
        let comp: CustomQuestionDeleteDialogComponent;
        let fixture: ComponentFixture<CustomQuestionDeleteDialogComponent>;
        let service: CustomQuestionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [CustomQuestionDeleteDialogComponent]
            })
                .overrideTemplate(CustomQuestionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CustomQuestionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomQuestionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
