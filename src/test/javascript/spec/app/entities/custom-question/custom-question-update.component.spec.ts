/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { CustomQuestionUpdateComponent } from 'app/entities/custom-question/custom-question-update.component';
import { CustomQuestionService } from 'app/entities/custom-question/custom-question.service';
import { CustomQuestion } from 'app/shared/model/custom-question.model';

describe('Component Tests', () => {
    describe('CustomQuestion Management Update Component', () => {
        let comp: CustomQuestionUpdateComponent;
        let fixture: ComponentFixture<CustomQuestionUpdateComponent>;
        let service: CustomQuestionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [CustomQuestionUpdateComponent]
            })
                .overrideTemplate(CustomQuestionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CustomQuestionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomQuestionService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new CustomQuestion(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.customQuestion = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new CustomQuestion();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.customQuestion = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
