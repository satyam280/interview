/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { CustomQuestionDetailComponent } from 'app/entities/custom-question/custom-question-detail.component';
import { CustomQuestion } from 'app/shared/model/custom-question.model';

describe('Component Tests', () => {
    describe('CustomQuestion Management Detail Component', () => {
        let comp: CustomQuestionDetailComponent;
        let fixture: ComponentFixture<CustomQuestionDetailComponent>;
        const route = ({ data: of({ customQuestion: new CustomQuestion(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [CustomQuestionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CustomQuestionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CustomQuestionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.customQuestion).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
