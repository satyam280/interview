/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckFeedbackDeleteDialogComponent } from 'app/entities/answer-check-feedback/answer-check-feedback-delete-dialog.component';
import { AnswerCheckFeedbackService } from 'app/entities/answer-check-feedback/answer-check-feedback.service';

describe('Component Tests', () => {
    describe('AnswerCheckFeedback Management Delete Component', () => {
        let comp: AnswerCheckFeedbackDeleteDialogComponent;
        let fixture: ComponentFixture<AnswerCheckFeedbackDeleteDialogComponent>;
        let service: AnswerCheckFeedbackService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckFeedbackDeleteDialogComponent]
            })
                .overrideTemplate(AnswerCheckFeedbackDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AnswerCheckFeedbackDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckFeedbackService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
