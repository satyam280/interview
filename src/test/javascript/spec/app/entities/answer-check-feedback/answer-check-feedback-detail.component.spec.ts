/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckFeedbackDetailComponent } from 'app/entities/answer-check-feedback/answer-check-feedback-detail.component';
import { AnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';

describe('Component Tests', () => {
    describe('AnswerCheckFeedback Management Detail Component', () => {
        let comp: AnswerCheckFeedbackDetailComponent;
        let fixture: ComponentFixture<AnswerCheckFeedbackDetailComponent>;
        const route = ({ data: of({ answerCheckFeedback: new AnswerCheckFeedback(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckFeedbackDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AnswerCheckFeedbackDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AnswerCheckFeedbackDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.answerCheckFeedback).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
