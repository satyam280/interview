/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckFeedbackUpdateComponent } from 'app/entities/answer-check-feedback/answer-check-feedback-update.component';
import { AnswerCheckFeedbackService } from 'app/entities/answer-check-feedback/answer-check-feedback.service';
import { AnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';

describe('Component Tests', () => {
    describe('AnswerCheckFeedback Management Update Component', () => {
        let comp: AnswerCheckFeedbackUpdateComponent;
        let fixture: ComponentFixture<AnswerCheckFeedbackUpdateComponent>;
        let service: AnswerCheckFeedbackService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckFeedbackUpdateComponent]
            })
                .overrideTemplate(AnswerCheckFeedbackUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AnswerCheckFeedbackUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckFeedbackService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new AnswerCheckFeedback(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.answerCheckFeedback = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new AnswerCheckFeedback();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.answerCheckFeedback = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
