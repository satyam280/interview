/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckDeleteDialogComponent } from 'app/entities/answer-check/answer-check-delete-dialog.component';
import { AnswerCheckService } from 'app/entities/answer-check/answer-check.service';

describe('Component Tests', () => {
    describe('AnswerCheck Management Delete Component', () => {
        let comp: AnswerCheckDeleteDialogComponent;
        let fixture: ComponentFixture<AnswerCheckDeleteDialogComponent>;
        let service: AnswerCheckService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckDeleteDialogComponent]
            })
                .overrideTemplate(AnswerCheckDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AnswerCheckDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
