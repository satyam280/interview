/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckDetailComponent } from 'app/entities/answer-check/answer-check-detail.component';
import { AnswerCheck } from 'app/shared/model/answer-check.model';

describe('Component Tests', () => {
    describe('AnswerCheck Management Detail Component', () => {
        let comp: AnswerCheckDetailComponent;
        let fixture: ComponentFixture<AnswerCheckDetailComponent>;
        const route = ({ data: of({ answerCheck: new AnswerCheck(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AnswerCheckDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AnswerCheckDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.answerCheck).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
