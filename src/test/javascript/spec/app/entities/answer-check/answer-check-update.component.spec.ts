/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckUpdateComponent } from 'app/entities/answer-check/answer-check-update.component';
import { AnswerCheckService } from 'app/entities/answer-check/answer-check.service';
import { AnswerCheck } from 'app/shared/model/answer-check.model';

describe('Component Tests', () => {
    describe('AnswerCheck Management Update Component', () => {
        let comp: AnswerCheckUpdateComponent;
        let fixture: ComponentFixture<AnswerCheckUpdateComponent>;
        let service: AnswerCheckService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckUpdateComponent]
            })
                .overrideTemplate(AnswerCheckUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AnswerCheckUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new AnswerCheck(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.answerCheck = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new AnswerCheck();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.answerCheck = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
