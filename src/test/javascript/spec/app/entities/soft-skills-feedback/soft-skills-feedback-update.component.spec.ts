/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { SoftSkillsFeedbackUpdateComponent } from 'app/entities/soft-skills-feedback/soft-skills-feedback-update.component';
import { SoftSkillsFeedbackService } from 'app/entities/soft-skills-feedback/soft-skills-feedback.service';
import { SoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';

describe('Component Tests', () => {
    describe('SoftSkillsFeedback Management Update Component', () => {
        let comp: SoftSkillsFeedbackUpdateComponent;
        let fixture: ComponentFixture<SoftSkillsFeedbackUpdateComponent>;
        let service: SoftSkillsFeedbackService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [SoftSkillsFeedbackUpdateComponent]
            })
                .overrideTemplate(SoftSkillsFeedbackUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SoftSkillsFeedbackUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SoftSkillsFeedbackService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new SoftSkillsFeedback(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.softSkillsFeedback = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new SoftSkillsFeedback();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.softSkillsFeedback = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
