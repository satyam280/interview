/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { SoftSkillsFeedbackDetailComponent } from 'app/entities/soft-skills-feedback/soft-skills-feedback-detail.component';
import { SoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';

describe('Component Tests', () => {
    describe('SoftSkillsFeedback Management Detail Component', () => {
        let comp: SoftSkillsFeedbackDetailComponent;
        let fixture: ComponentFixture<SoftSkillsFeedbackDetailComponent>;
        const route = ({ data: of({ softSkillsFeedback: new SoftSkillsFeedback(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [SoftSkillsFeedbackDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SoftSkillsFeedbackDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SoftSkillsFeedbackDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.softSkillsFeedback).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
