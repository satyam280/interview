/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { InterviewTestModule } from '../../../test.module';
import { SoftSkillsFeedbackComponent } from 'app/entities/soft-skills-feedback/soft-skills-feedback.component';
import { SoftSkillsFeedbackService } from 'app/entities/soft-skills-feedback/soft-skills-feedback.service';
import { SoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';

describe('Component Tests', () => {
    describe('SoftSkillsFeedback Management Component', () => {
        let comp: SoftSkillsFeedbackComponent;
        let fixture: ComponentFixture<SoftSkillsFeedbackComponent>;
        let service: SoftSkillsFeedbackService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [SoftSkillsFeedbackComponent],
                providers: []
            })
                .overrideTemplate(SoftSkillsFeedbackComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SoftSkillsFeedbackComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SoftSkillsFeedbackService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SoftSkillsFeedback(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.softSkillsFeedbacks[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
