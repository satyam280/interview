/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { SoftSkillsFeedbackDeleteDialogComponent } from 'app/entities/soft-skills-feedback/soft-skills-feedback-delete-dialog.component';
import { SoftSkillsFeedbackService } from 'app/entities/soft-skills-feedback/soft-skills-feedback.service';

describe('Component Tests', () => {
    describe('SoftSkillsFeedback Management Delete Component', () => {
        let comp: SoftSkillsFeedbackDeleteDialogComponent;
        let fixture: ComponentFixture<SoftSkillsFeedbackDeleteDialogComponent>;
        let service: SoftSkillsFeedbackService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [SoftSkillsFeedbackDeleteDialogComponent]
            })
                .overrideTemplate(SoftSkillsFeedbackDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SoftSkillsFeedbackDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SoftSkillsFeedbackService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
