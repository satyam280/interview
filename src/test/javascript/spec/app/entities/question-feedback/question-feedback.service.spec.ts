/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { QuestionFeedbackService } from 'app/entities/question-feedback/question-feedback.service';
import { IQuestionFeedback, QuestionFeedback, AnswerQuality } from 'app/shared/model/question-feedback.model';

describe('Service Tests', () => {
    describe('QuestionFeedback Service', () => {
        let injector: TestBed;
        let service: QuestionFeedbackService;
        let httpMock: HttpTestingController;
        let elemDefault: IQuestionFeedback;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(QuestionFeedbackService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new QuestionFeedback(0, AnswerQuality.EXCELLENT, currentDate, currentDate, 0, 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a QuestionFeedback', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        startedAt: currentDate,
                        finishedAt: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new QuestionFeedback(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a QuestionFeedback', async () => {
                const returnedFromService = Object.assign(
                    {
                        answer_quality: 'BBBBBB',
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT),
                        duration: 1,
                        description: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        startedAt: currentDate,
                        finishedAt: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of QuestionFeedback', async () => {
                const returnedFromService = Object.assign(
                    {
                        answer_quality: 'BBBBBB',
                        startedAt: currentDate.format(DATE_TIME_FORMAT),
                        finishedAt: currentDate.format(DATE_TIME_FORMAT),
                        duration: 1,
                        description: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        startedAt: currentDate,
                        finishedAt: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a QuestionFeedback', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
