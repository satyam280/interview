/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { QuestionFeedbackDeleteDialogComponent } from 'app/entities/question-feedback/question-feedback-delete-dialog.component';
import { QuestionFeedbackService } from 'app/entities/question-feedback/question-feedback.service';

describe('Component Tests', () => {
    describe('QuestionFeedback Management Delete Component', () => {
        let comp: QuestionFeedbackDeleteDialogComponent;
        let fixture: ComponentFixture<QuestionFeedbackDeleteDialogComponent>;
        let service: QuestionFeedbackService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [QuestionFeedbackDeleteDialogComponent]
            })
                .overrideTemplate(QuestionFeedbackDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(QuestionFeedbackDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuestionFeedbackService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
