/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { QuestionFeedbackDetailComponent } from 'app/entities/question-feedback/question-feedback-detail.component';
import { QuestionFeedback } from 'app/shared/model/question-feedback.model';

describe('Component Tests', () => {
    describe('QuestionFeedback Management Detail Component', () => {
        let comp: QuestionFeedbackDetailComponent;
        let fixture: ComponentFixture<QuestionFeedbackDetailComponent>;
        const route = ({ data: of({ questionFeedback: new QuestionFeedback(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [QuestionFeedbackDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(QuestionFeedbackDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(QuestionFeedbackDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.questionFeedback).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
