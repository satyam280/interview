/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { QuestionFeedbackUpdateComponent } from 'app/entities/question-feedback/question-feedback-update.component';
import { QuestionFeedbackService } from 'app/entities/question-feedback/question-feedback.service';
import { QuestionFeedback } from 'app/shared/model/question-feedback.model';

describe('Component Tests', () => {
    describe('QuestionFeedback Management Update Component', () => {
        let comp: QuestionFeedbackUpdateComponent;
        let fixture: ComponentFixture<QuestionFeedbackUpdateComponent>;
        let service: QuestionFeedbackService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [QuestionFeedbackUpdateComponent]
            })
                .overrideTemplate(QuestionFeedbackUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(QuestionFeedbackUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuestionFeedbackService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new QuestionFeedback(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.questionFeedback = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new QuestionFeedback();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.questionFeedback = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
