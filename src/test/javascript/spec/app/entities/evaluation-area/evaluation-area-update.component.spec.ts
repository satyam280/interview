/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { EvaluationAreaUpdateComponent } from 'app/entities/evaluation-area/evaluation-area-update.component';
import { EvaluationAreaService } from 'app/entities/evaluation-area/evaluation-area.service';
import { EvaluationArea } from 'app/shared/model/evaluation-area.model';

describe('Component Tests', () => {
    describe('EvaluationArea Management Update Component', () => {
        let comp: EvaluationAreaUpdateComponent;
        let fixture: ComponentFixture<EvaluationAreaUpdateComponent>;
        let service: EvaluationAreaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [EvaluationAreaUpdateComponent]
            })
                .overrideTemplate(EvaluationAreaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EvaluationAreaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EvaluationAreaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new EvaluationArea(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.evaluationArea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new EvaluationArea();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.evaluationArea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
