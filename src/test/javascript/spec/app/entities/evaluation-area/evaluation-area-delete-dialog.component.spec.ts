/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { EvaluationAreaDeleteDialogComponent } from 'app/entities/evaluation-area/evaluation-area-delete-dialog.component';
import { EvaluationAreaService } from 'app/entities/evaluation-area/evaluation-area.service';

describe('Component Tests', () => {
    describe('EvaluationArea Management Delete Component', () => {
        let comp: EvaluationAreaDeleteDialogComponent;
        let fixture: ComponentFixture<EvaluationAreaDeleteDialogComponent>;
        let service: EvaluationAreaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [EvaluationAreaDeleteDialogComponent]
            })
                .overrideTemplate(EvaluationAreaDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EvaluationAreaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EvaluationAreaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
