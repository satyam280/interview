/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { InterviewTestModule } from '../../../test.module';
import { EvaluationAreaComponent } from 'app/entities/evaluation-area/evaluation-area.component';
import { EvaluationAreaService } from 'app/entities/evaluation-area/evaluation-area.service';
import { EvaluationArea } from 'app/shared/model/evaluation-area.model';

describe('Component Tests', () => {
    describe('EvaluationArea Management Component', () => {
        let comp: EvaluationAreaComponent;
        let fixture: ComponentFixture<EvaluationAreaComponent>;
        let service: EvaluationAreaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [EvaluationAreaComponent],
                providers: []
            })
                .overrideTemplate(EvaluationAreaComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EvaluationAreaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EvaluationAreaService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new EvaluationArea(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.evaluationAreas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
