/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { EvaluationAreaDetailComponent } from 'app/entities/evaluation-area/evaluation-area-detail.component';
import { EvaluationArea } from 'app/shared/model/evaluation-area.model';

describe('Component Tests', () => {
    describe('EvaluationArea Management Detail Component', () => {
        let comp: EvaluationAreaDetailComponent;
        let fixture: ComponentFixture<EvaluationAreaDetailComponent>;
        const route = ({ data: of({ evaluationArea: new EvaluationArea(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [EvaluationAreaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(EvaluationAreaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EvaluationAreaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.evaluationArea).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
