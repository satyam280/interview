/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckEvalAreaMapDetailComponent } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map-detail.component';
import { AnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';

describe('Component Tests', () => {
    describe('AnswerCheckEvalAreaMap Management Detail Component', () => {
        let comp: AnswerCheckEvalAreaMapDetailComponent;
        let fixture: ComponentFixture<AnswerCheckEvalAreaMapDetailComponent>;
        const route = ({ data: of({ answerCheckEvalAreaMap: new AnswerCheckEvalAreaMap(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckEvalAreaMapDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AnswerCheckEvalAreaMapDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AnswerCheckEvalAreaMapDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.answerCheckEvalAreaMap).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
