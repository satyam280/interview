/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckEvalAreaMapDeleteDialogComponent } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map-delete-dialog.component';
import { AnswerCheckEvalAreaMapService } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map.service';

describe('Component Tests', () => {
    describe('AnswerCheckEvalAreaMap Management Delete Component', () => {
        let comp: AnswerCheckEvalAreaMapDeleteDialogComponent;
        let fixture: ComponentFixture<AnswerCheckEvalAreaMapDeleteDialogComponent>;
        let service: AnswerCheckEvalAreaMapService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckEvalAreaMapDeleteDialogComponent]
            })
                .overrideTemplate(AnswerCheckEvalAreaMapDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AnswerCheckEvalAreaMapDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckEvalAreaMapService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
