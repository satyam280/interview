/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckEvalAreaMapUpdateComponent } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map-update.component';
import { AnswerCheckEvalAreaMapService } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map.service';
import { AnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';

describe('Component Tests', () => {
    describe('AnswerCheckEvalAreaMap Management Update Component', () => {
        let comp: AnswerCheckEvalAreaMapUpdateComponent;
        let fixture: ComponentFixture<AnswerCheckEvalAreaMapUpdateComponent>;
        let service: AnswerCheckEvalAreaMapService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckEvalAreaMapUpdateComponent]
            })
                .overrideTemplate(AnswerCheckEvalAreaMapUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AnswerCheckEvalAreaMapUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckEvalAreaMapService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new AnswerCheckEvalAreaMap(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.answerCheckEvalAreaMap = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new AnswerCheckEvalAreaMap();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.answerCheckEvalAreaMap = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
