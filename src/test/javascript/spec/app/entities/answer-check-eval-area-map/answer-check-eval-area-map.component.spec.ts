/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { InterviewTestModule } from '../../../test.module';
import { AnswerCheckEvalAreaMapComponent } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map.component';
import { AnswerCheckEvalAreaMapService } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map.service';
import { AnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';

describe('Component Tests', () => {
    describe('AnswerCheckEvalAreaMap Management Component', () => {
        let comp: AnswerCheckEvalAreaMapComponent;
        let fixture: ComponentFixture<AnswerCheckEvalAreaMapComponent>;
        let service: AnswerCheckEvalAreaMapService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AnswerCheckEvalAreaMapComponent],
                providers: []
            })
                .overrideTemplate(AnswerCheckEvalAreaMapComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AnswerCheckEvalAreaMapComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnswerCheckEvalAreaMapService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new AnswerCheckEvalAreaMap(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.answerCheckEvalAreaMaps[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
