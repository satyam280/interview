/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InterviewTestModule } from '../../../test.module';
import { AdminUserDeleteDialogComponent } from 'app/entities/admin-user/admin-user-delete-dialog.component';
import { AdminUserService } from 'app/entities/admin-user/admin-user.service';

describe('Component Tests', () => {
    describe('AdminUser Management Delete Component', () => {
        let comp: AdminUserDeleteDialogComponent;
        let fixture: ComponentFixture<AdminUserDeleteDialogComponent>;
        let service: AdminUserService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [InterviewTestModule],
                declarations: [AdminUserDeleteDialogComponent]
            })
                .overrideTemplate(AdminUserDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AdminUserDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdminUserService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
