package com.wissen.interview.web.rest;
import com.wissen.interview.domain.AnswerCheckFeedback;
import com.wissen.interview.repository.AnswerCheckFeedbackRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import com.wissen.interview.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AnswerCheckFeedback.
 */
@RestController
@RequestMapping("/api")
public class AnswerCheckFeedbackResource {

    private final Logger log = LoggerFactory.getLogger(AnswerCheckFeedbackResource.class);

    private static final String ENTITY_NAME = "answerCheckFeedback";

    private final AnswerCheckFeedbackRepository answerCheckFeedbackRepository;

    public AnswerCheckFeedbackResource(AnswerCheckFeedbackRepository answerCheckFeedbackRepository) {
        this.answerCheckFeedbackRepository = answerCheckFeedbackRepository;
    }

    /**
     * POST  /answer-check-feedbacks : Create a new answerCheckFeedback.
     *
     * @param answerCheckFeedback the answerCheckFeedback to create
     * @return the ResponseEntity with status 201 (Created) and with body the new answerCheckFeedback, or with status 400 (Bad Request) if the answerCheckFeedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/answer-check-feedbacks")
    public ResponseEntity<AnswerCheckFeedback> createAnswerCheckFeedback(@RequestBody AnswerCheckFeedback answerCheckFeedback) throws URISyntaxException {
        log.debug("REST request to save AnswerCheckFeedback : {}", answerCheckFeedback);
        if (answerCheckFeedback.getId() != null) {
            throw new BadRequestAlertException("A new answerCheckFeedback cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(answerCheckFeedbackRepository.findByAnswerCheckAndInterview(answerCheckFeedback.getAnswerCheck().getId(),answerCheckFeedback.getInterview().getId())!=null) {
        	AnswerCheckFeedback answerCheckFeedback2=answerCheckFeedbackRepository.findByAnswerCheckAndInterview(answerCheckFeedback.getAnswerCheck().getId(),answerCheckFeedback.getInterview().getId());
        	   answerCheckFeedbackRepository.deleteById(answerCheckFeedback2.getId());
        	
        	
        }
        AnswerCheckFeedback result = answerCheckFeedbackRepository.save(answerCheckFeedback);
        return ResponseEntity.created(new URI("/api/answer-check-feedbacks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /answer-check-feedbacks : Updates an existing answerCheckFeedback.
     *
     * @param answerCheckFeedback the answerCheckFeedback to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated answerCheckFeedback,
     * or with status 400 (Bad Request) if the answerCheckFeedback is not valid,
     * or with status 500 (Internal Server Error) if the answerCheckFeedback couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/answer-check-feedbacks")
    public ResponseEntity<AnswerCheckFeedback> updateAnswerCheckFeedback(@RequestBody AnswerCheckFeedback answerCheckFeedback) throws URISyntaxException {
        log.debug("REST request to update AnswerCheckFeedback : {}", answerCheckFeedback);
        if (answerCheckFeedback.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AnswerCheckFeedback result = answerCheckFeedbackRepository.save(answerCheckFeedback);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, answerCheckFeedback.getId().toString()))
            .body(result);
    }

    /**
     * GET  /answer-check-feedbacks : get all the answerCheckFeedbacks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of answerCheckFeedbacks in body
     */
    @GetMapping("/answer-check-feedbacks")
    public ResponseEntity<List<AnswerCheckFeedback>> getAllAnswerCheckFeedbacks(Pageable pageable) {
        log.debug("REST request to get a page of AnswerCheckFeedbacks");
        Page<AnswerCheckFeedback> page = answerCheckFeedbackRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/answer-check-feedbacks");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /answer-check-feedbacks/:id : get the "id" answerCheckFeedback.
     *
     * @param id the id of the answerCheckFeedback to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the answerCheckFeedback, or with status 404 (Not Found)
     */
    @GetMapping("/answer-check-feedbacks/{id}")
    public ResponseEntity<AnswerCheckFeedback> getAnswerCheckFeedback(@PathVariable Long id) {
        log.debug("REST request to get AnswerCheckFeedback : {}", id);
        Optional<AnswerCheckFeedback> answerCheckFeedback = answerCheckFeedbackRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(answerCheckFeedback);
    }

    @GetMapping("/answer-check-feedbacks-id/{id}")
    public List<AnswerCheckFeedback> getAnswerCheckFeedbacks(@PathVariable Long id){
    	List<AnswerCheckFeedback> acF=new ArrayList<>();
    	acF=answerCheckFeedbackRepository.findByInterview(id);
    	return acF;
    }
    /**
     * DELETE  /answer-check-feedbacks/:id : delete the "id" answerCheckFeedback.
     *
     * @param id the id of the answerCheckFeedback to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/answer-check-feedbacks/{id}")
    public ResponseEntity<Void> deleteAnswerCheckFeedback(@PathVariable Long id) {
        log.debug("REST request to delete AnswerCheckFeedback : {}", id);
        answerCheckFeedbackRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
