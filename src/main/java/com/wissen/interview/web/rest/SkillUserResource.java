package com.wissen.interview.web.rest;

import com.wissen.interview.domain.SkillUser;
import com.wissen.interview.repository.SkillUserRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.wissen.interview.domain.SkillUser}.
 */
@RestController
@RequestMapping("/api")
public class SkillUserResource {

    private final Logger log = LoggerFactory.getLogger(SkillUserResource.class);

    private static final String ENTITY_NAME = "skillUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SkillUserRepository skillUserRepository;

    public SkillUserResource(SkillUserRepository skillUserRepository) {
        this.skillUserRepository = skillUserRepository;
    }

    /**
     * {@code POST  /skill-users} : Create a new skillUser.
     *
     * @param skillUser the skillUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new skillUser, or with status {@code 400 (Bad Request)} if the skillUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/skill-users")
    public ResponseEntity<SkillUser> createSkillUser(@Valid @RequestBody SkillUser skillUser) throws URISyntaxException {
        log.debug("REST request to save SkillUser : {}", skillUser);
        if (skillUser.getId() != null) {
            throw new BadRequestAlertException("A new skillUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SkillUser result = skillUserRepository.save(skillUser);
        return ResponseEntity.created(new URI("/api/skill-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /skill-users} : Updates an existing skillUser.
     *
     * @param skillUser the skillUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated skillUser,
     * or with status {@code 400 (Bad Request)} if the skillUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the skillUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/skill-users")
    public ResponseEntity<SkillUser> updateSkillUser(@Valid @RequestBody SkillUser skillUser) throws URISyntaxException {
        log.debug("REST request to update SkillUser : {}", skillUser);
        if (skillUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SkillUser result = skillUserRepository.save(skillUser);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, skillUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /skill-users} : get all the skillUsers.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of skillUsers in body.
     */
    @GetMapping("/skill-users")
    public List<SkillUser> getAllSkillUsers() {
        log.debug("REST request to get all SkillUsers");
        return skillUserRepository.findAll();
    }

    /**
     * {@code GET  /skill-users/:id} : get the "id" skillUser.
     *
     * @param id the id of the skillUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the skillUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/skill-users/{id}")
    public ResponseEntity<SkillUser> getSkillUser(@PathVariable Long id) {
        log.debug("REST request to get SkillUser : {}", id);
        Optional<SkillUser> skillUser = skillUserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(skillUser);
    }

    /**
     * {@code DELETE  /skill-users/:id} : delete the "id" skillUser.
     *
     * @param id the id of the skillUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/skill-users/{id}")
    public ResponseEntity<Void> deleteSkillUser(@PathVariable Long id) {
        log.debug("REST request to delete SkillUser : {}", id);
        skillUserRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
