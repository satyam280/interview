package com.wissen.interview.web.rest;
import com.wissen.interview.domain.SoftSkillsFeedback;
import com.wissen.interview.repository.SoftSkillsFeedbackRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SoftSkillsFeedback.
 */
@RestController
@RequestMapping("/api")
public class SoftSkillsFeedbackResource {

    private final Logger log = LoggerFactory.getLogger(SoftSkillsFeedbackResource.class);

    private static final String ENTITY_NAME = "softSkillsFeedback";

    private final SoftSkillsFeedbackRepository softSkillsFeedbackRepository;

    public SoftSkillsFeedbackResource(SoftSkillsFeedbackRepository softSkillsFeedbackRepository) {
        this.softSkillsFeedbackRepository = softSkillsFeedbackRepository;
    }

    /**
     * POST  /soft-skills-feedbacks : Create a new softSkillsFeedback.
     *
     * @param softSkillsFeedback the softSkillsFeedback to create
     * @return the ResponseEntity with status 201 (Created) and with body the new softSkillsFeedback, or with status 400 (Bad Request) if the softSkillsFeedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/soft-skills-feedbacks")
    public ResponseEntity<SoftSkillsFeedback> createSoftSkillsFeedback(@RequestBody SoftSkillsFeedback softSkillsFeedback) throws URISyntaxException {
        log.debug("REST request to save SoftSkillsFeedback : {}", softSkillsFeedback);
        if (softSkillsFeedback.getId() != null) {
            throw new BadRequestAlertException("A new softSkillsFeedback cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SoftSkillsFeedback temp=softSkillsFeedbackRepository.findByInterview(softSkillsFeedback.getInterview().getId());
        if(temp!=null) {
            softSkillsFeedbackRepository.deleteById(temp.getId());
        }
        SoftSkillsFeedback result = softSkillsFeedbackRepository.save(softSkillsFeedback);
        return ResponseEntity.created(new URI("/api/soft-skills-feedbacks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /soft-skills-feedbacks : Updates an existing softSkillsFeedback.
     *
     * @param softSkillsFeedback the softSkillsFeedback to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated softSkillsFeedback,
     * or with status 400 (Bad Request) if the softSkillsFeedback is not valid,
     * or with status 500 (Internal Server Error) if the softSkillsFeedback couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/soft-skills-feedbacks")
    public ResponseEntity<SoftSkillsFeedback> updateSoftSkillsFeedback(@RequestBody SoftSkillsFeedback softSkillsFeedback) throws URISyntaxException {
        log.debug("REST request to update SoftSkillsFeedback : {}", softSkillsFeedback);
        if (softSkillsFeedback.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SoftSkillsFeedback result = softSkillsFeedbackRepository.save(softSkillsFeedback);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, softSkillsFeedback.getId().toString()))
            .body(result);
    }

    /**
     * GET  /soft-skills-feedbacks : get all the softSkillsFeedbacks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of softSkillsFeedbacks in body
     */
    @GetMapping("/soft-skills-feedbacks")
    public List<SoftSkillsFeedback> getAllSoftSkillsFeedbacks() {
        log.debug("REST request to get all SoftSkillsFeedbacks");
        return softSkillsFeedbackRepository.findAll();
    }

    /**
     * GET  /soft-skills-feedbacks/:id : get the "id" softSkillsFeedback.
     *
     * @param id the id of the softSkillsFeedback to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the softSkillsFeedback, or with status 404 (Not Found)
     */
    @GetMapping("/soft-skills-feedbacks/{id}")
    public ResponseEntity<SoftSkillsFeedback> getSoftSkillsFeedback(@PathVariable Long id) {
        log.debug("REST request to get SoftSkillsFeedback : {}", id);
        Optional<SoftSkillsFeedback> softSkillsFeedback = softSkillsFeedbackRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(softSkillsFeedback);
    }
    
    
    @GetMapping("/soft-skills-feedbacks-interview-id/{id}")
    public SoftSkillsFeedback getSoftSkillsFeedbackByInterviewId(@PathVariable Long id) {
        log.debug("REST request to get SoftSkillsFeedback : {}", id);
        SoftSkillsFeedback softSkillsFeedback = softSkillsFeedbackRepository.findByInterview(id);
        return softSkillsFeedback;
    }

    /**
     * DELETE  /soft-skills-feedbacks/:id : delete the "id" softSkillsFeedback.
     *
     * @param id the id of the softSkillsFeedback to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/soft-skills-feedbacks/{id}")
    public ResponseEntity<Void> deleteSoftSkillsFeedback(@PathVariable Long id) {
        log.debug("REST request to delete SoftSkillsFeedback : {}", id);
        softSkillsFeedbackRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
