package com.wissen.interview.web.rest;
import com.wissen.interview.domain.EvaluationArea;
import com.wissen.interview.repository.EvaluationAreaRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EvaluationArea.
 */
@RestController
@RequestMapping("/api")
public class EvaluationAreaResource {

    private final Logger log = LoggerFactory.getLogger(EvaluationAreaResource.class);

    private static final String ENTITY_NAME = "evaluationArea";

    private final EvaluationAreaRepository evaluationAreaRepository;

    public EvaluationAreaResource(EvaluationAreaRepository evaluationAreaRepository) {
        this.evaluationAreaRepository = evaluationAreaRepository;
    }

    /**
     * POST  /evaluation-areas : Create a new evaluationArea.
     *
     * @param evaluationArea the evaluationArea to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evaluationArea, or with status 400 (Bad Request) if the evaluationArea has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evaluation-areas")
    public ResponseEntity<EvaluationArea> createEvaluationArea(@RequestBody EvaluationArea evaluationArea) throws URISyntaxException {
        log.debug("REST request to save EvaluationArea : {}", evaluationArea);
        if (evaluationArea.getId() != null) {
            throw new BadRequestAlertException("A new evaluationArea cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EvaluationArea result = evaluationAreaRepository.save(evaluationArea);
        return ResponseEntity.created(new URI("/api/evaluation-areas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /evaluation-areas : Updates an existing evaluationArea.
     *
     * @param evaluationArea the evaluationArea to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evaluationArea,
     * or with status 400 (Bad Request) if the evaluationArea is not valid,
     * or with status 500 (Internal Server Error) if the evaluationArea couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evaluation-areas")
    public ResponseEntity<EvaluationArea> updateEvaluationArea(@RequestBody EvaluationArea evaluationArea) throws URISyntaxException {
        log.debug("REST request to update EvaluationArea : {}", evaluationArea);
        if (evaluationArea.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EvaluationArea result = evaluationAreaRepository.save(evaluationArea);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evaluationArea.getId().toString()))
            .body(result);
    }

    /**
     * GET  /evaluation-areas : get all the evaluationAreas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of evaluationAreas in body
     */
    @GetMapping("/evaluation-areas")
    public List<EvaluationArea> getAllEvaluationAreas() {
        log.debug("REST request to get all EvaluationAreas");
        return evaluationAreaRepository.findAll();
    }

    /**
     * GET  /evaluation-areas/:id : get the "id" evaluationArea.
     *
     * @param id the id of the evaluationArea to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evaluationArea, or with status 404 (Not Found)
     */
    @GetMapping("/evaluation-areas/{id}")
    public ResponseEntity<EvaluationArea> getEvaluationArea(@PathVariable Long id) {
        log.debug("REST request to get EvaluationArea : {}", id);
        Optional<EvaluationArea> evaluationArea = evaluationAreaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(evaluationArea);
    }

    /**
     * DELETE  /evaluation-areas/:id : delete the "id" evaluationArea.
     *
     * @param id the id of the evaluationArea to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evaluation-areas/{id}")
    public ResponseEntity<Void> deleteEvaluationArea(@PathVariable Long id) {
        log.debug("REST request to delete EvaluationArea : {}", id);
        evaluationAreaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
