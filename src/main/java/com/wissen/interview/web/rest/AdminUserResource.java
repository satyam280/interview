package com.wissen.interview.web.rest;

import com.wissen.interview.domain.AdminUser;
import com.wissen.interview.repository.AdminUserRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.wissen.interview.domain.AdminUser}.
 */
@RestController
@RequestMapping("/api")
public class AdminUserResource {

    private final Logger log = LoggerFactory.getLogger(AdminUserResource.class);

    private static final String ENTITY_NAME = "adminUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdminUserRepository adminUserRepository;

    public AdminUserResource(AdminUserRepository adminUserRepository) {
        this.adminUserRepository = adminUserRepository;
    }

    /**
     * {@code POST  /admin-users} : Create a new adminUser.
     *
     * @param adminUser the adminUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adminUser, or with status {@code 400 (Bad Request)} if the adminUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/admin-users")
    public ResponseEntity<AdminUser> createAdminUser(@RequestBody AdminUser adminUser) throws URISyntaxException {
        log.debug("REST request to save AdminUser : {}", adminUser);
        if (adminUser.getId() != null) {
            throw new BadRequestAlertException("A new adminUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdminUser result = adminUserRepository.save(adminUser);
        return ResponseEntity.created(new URI("/api/admin-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /admin-users} : Updates an existing adminUser.
     *
     * @param adminUser the adminUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adminUser,
     * or with status {@code 400 (Bad Request)} if the adminUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the adminUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/admin-users")
    public ResponseEntity<AdminUser> updateAdminUser(@RequestBody AdminUser adminUser) throws URISyntaxException {
        log.debug("REST request to update AdminUser : {}", adminUser);
        if (adminUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdminUser result = adminUserRepository.save(adminUser);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adminUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /admin-users} : get all the adminUsers.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adminUsers in body.
     */
    @GetMapping("/admin-users")
    public List<AdminUser> getAllAdminUsers() {
        log.debug("REST request to get all AdminUsers");
        return adminUserRepository.findAll();
    }

    /**
     * {@code GET  /admin-users/:id} : get the "id" adminUser.
     *
     * @param id the id of the adminUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adminUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/admin-users/{id}")
    public ResponseEntity<AdminUser> getAdminUser(@PathVariable Long id) {
        log.debug("REST request to get AdminUser : {}", id);
        Optional<AdminUser> adminUser = adminUserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(adminUser);
    }

    /**
     * {@code DELETE  /admin-users/:id} : delete the "id" adminUser.
     *
     * @param id the id of the adminUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/admin-users/{id}")
    public ResponseEntity<Void> deleteAdminUser(@PathVariable Long id) {
        log.debug("REST request to delete AdminUser : {}", id);
        adminUserRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
