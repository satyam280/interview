package com.wissen.interview.web.rest;
import com.wissen.interview.domain.QuestionFeedback;
import com.wissen.interview.repository.QuestionFeedbackRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import com.wissen.interview.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing QuestionFeedback.
 */
@RestController
@RequestMapping("/api")
public class QuestionFeedbackResource {

    private final Logger log = LoggerFactory.getLogger(QuestionFeedbackResource.class);

    private static final String ENTITY_NAME = "questionFeedback";

    private final QuestionFeedbackRepository questionFeedbackRepository;

    public QuestionFeedbackResource(QuestionFeedbackRepository questionFeedbackRepository) {
        this.questionFeedbackRepository = questionFeedbackRepository;
    }

    /**
     * POST  /question-feedbacks : Create a new questionFeedback.
     *
     * @param questionFeedback the questionFeedback to create
     * @return the ResponseEntity with status 201 (Created) and with body the new questionFeedback, or with status 400 (Bad Request) if the questionFeedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/question-feedbacks")
    public ResponseEntity<QuestionFeedback> createQuestionFeedback(@Valid @RequestBody QuestionFeedback questionFeedback) throws URISyntaxException {
        log.debug("REST request to save QuestionFeedback : {}", questionFeedback);
        if (questionFeedback.getId() != null) {
            throw new BadRequestAlertException("A new questionFeedback cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuestionFeedback result = questionFeedbackRepository.save(questionFeedback);
        return ResponseEntity.created(new URI("/api/question-feedbacks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /question-feedbacks : Updates an existing questionFeedback.
     *
     * @param questionFeedback the questionFeedback to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated questionFeedback,
     * or with status 400 (Bad Request) if the questionFeedback is not valid,
     * or with status 500 (Internal Server Error) if the questionFeedback couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/question-feedbacks")
    public ResponseEntity<QuestionFeedback> updateQuestionFeedback(@Valid @RequestBody QuestionFeedback questionFeedback) throws URISyntaxException {
        log.debug("REST request to update QuestionFeedback : {}", questionFeedback);
        if (questionFeedback.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        QuestionFeedback result = questionFeedbackRepository.save(questionFeedback);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, questionFeedback.getId().toString()))
            .body(result);
    }

    /**
     * GET  /question-feedbacks : get all the questionFeedbacks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of questionFeedbacks in body
     */
    @GetMapping("/question-feedbacks")
    public ResponseEntity<List<QuestionFeedback>> getAllQuestionFeedbacks(Pageable pageable) {
        log.debug("REST request to get a page of QuestionFeedbacks");
        Page<QuestionFeedback> page = questionFeedbackRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/question-feedbacks");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /question-feedbacks/:id : get the "id" questionFeedback.
     *
     * @param id the id of the questionFeedback to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the questionFeedback, or with status 404 (Not Found)
     */
    @GetMapping("/question-feedbacks/{id}")
    public ResponseEntity<QuestionFeedback> getQuestionFeedback(@PathVariable Long id) {
        log.debug("REST request to get QuestionFeedback : {}", id);
        Optional<QuestionFeedback> questionFeedback = questionFeedbackRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(questionFeedback);
    }
    @GetMapping("/question-feedbacks-id/{id}")
    public List<QuestionFeedback> getQuestionFeedbacks(@PathVariable Long id) {
        log.debug("REST request to get QuestionFeedback : {}", id);
        List<QuestionFeedback> questionFeedback = questionFeedbackRepository.findByInterview(id);
        return questionFeedback;
    }

    
    /**
     * DELETE  /question-feedbacks/:id : delete the "id" questionFeedback.
     *
     * @param id the id of the questionFeedback to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/question-feedbacks/{id}")
    public ResponseEntity<Void> deleteQuestionFeedback(@PathVariable Long id) {
        log.debug("REST request to delete QuestionFeedback : {}", id);
        questionFeedbackRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
