package com.wissen.interview.web.rest;
import com.wissen.interview.domain.AnswerCheckEvalAreaMap;
import com.wissen.interview.repository.AnswerCheckEvalAreaMapRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AnswerCheckEvalAreaMap.
 */
@RestController
@RequestMapping("/api")
public class AnswerCheckEvalAreaMapResource {

    private final Logger log = LoggerFactory.getLogger(AnswerCheckEvalAreaMapResource.class);

    private static final String ENTITY_NAME = "answerCheckEvalAreaMap";

    private final AnswerCheckEvalAreaMapRepository answerCheckEvalAreaMapRepository;

    public AnswerCheckEvalAreaMapResource(AnswerCheckEvalAreaMapRepository answerCheckEvalAreaMapRepository) {
        this.answerCheckEvalAreaMapRepository = answerCheckEvalAreaMapRepository;
    }

    /**
     * POST  /answer-check-eval-area-maps : Create a new answerCheckEvalAreaMap.
     *
     * @param answerCheckEvalAreaMap the answerCheckEvalAreaMap to create
     * @return the ResponseEntity with status 201 (Created) and with body the new answerCheckEvalAreaMap, or with status 400 (Bad Request) if the answerCheckEvalAreaMap has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/answer-check-eval-area-maps")
    public ResponseEntity<AnswerCheckEvalAreaMap> createAnswerCheckEvalAreaMap(@RequestBody AnswerCheckEvalAreaMap answerCheckEvalAreaMap) throws URISyntaxException {
        log.debug("REST request to save AnswerCheckEvalAreaMap : {}", answerCheckEvalAreaMap);
        if (answerCheckEvalAreaMap.getId() != null) {
            throw new BadRequestAlertException("A new answerCheckEvalAreaMap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnswerCheckEvalAreaMap result = answerCheckEvalAreaMapRepository.save(answerCheckEvalAreaMap);
        return ResponseEntity.created(new URI("/api/answer-check-eval-area-maps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /answer-check-eval-area-maps : Updates an existing answerCheckEvalAreaMap.
     *
     * @param answerCheckEvalAreaMap the answerCheckEvalAreaMap to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated answerCheckEvalAreaMap,
     * or with status 400 (Bad Request) if the answerCheckEvalAreaMap is not valid,
     * or with status 500 (Internal Server Error) if the answerCheckEvalAreaMap couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/answer-check-eval-area-maps")
    public ResponseEntity<AnswerCheckEvalAreaMap> updateAnswerCheckEvalAreaMap(@RequestBody AnswerCheckEvalAreaMap answerCheckEvalAreaMap) throws URISyntaxException {
        log.debug("REST request to update AnswerCheckEvalAreaMap : {}", answerCheckEvalAreaMap);
        if (answerCheckEvalAreaMap.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AnswerCheckEvalAreaMap result = answerCheckEvalAreaMapRepository.save(answerCheckEvalAreaMap);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, answerCheckEvalAreaMap.getId().toString()))
            .body(result);
    }

    /**
     * GET  /answer-check-eval-area-maps : get all the answerCheckEvalAreaMaps.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of answerCheckEvalAreaMaps in body
     */
    @GetMapping("/answer-check-eval-area-maps")
    public List<AnswerCheckEvalAreaMap> getAllAnswerCheckEvalAreaMaps() {
        log.debug("REST request to get all AnswerCheckEvalAreaMaps");
        return answerCheckEvalAreaMapRepository.findAll();
    }

    /**
     * GET  /answer-check-eval-area-maps/:id : get the "id" answerCheckEvalAreaMap.
     *
     * @param id the id of the answerCheckEvalAreaMap to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the answerCheckEvalAreaMap, or with status 404 (Not Found)
     */
    @GetMapping("/answer-check-eval-area-maps/{id}")
    public ResponseEntity<AnswerCheckEvalAreaMap> getAnswerCheckEvalAreaMap(@PathVariable Long id) {
        log.debug("REST request to get AnswerCheckEvalAreaMap : {}", id);
        Optional<AnswerCheckEvalAreaMap> answerCheckEvalAreaMap = answerCheckEvalAreaMapRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(answerCheckEvalAreaMap);
    }

    /**
     * DELETE  /answer-check-eval-area-maps/:id : delete the "id" answerCheckEvalAreaMap.
     *
     * @param id the id of the answerCheckEvalAreaMap to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/answer-check-eval-area-maps/{id}")
    public ResponseEntity<Void> deleteAnswerCheckEvalAreaMap(@PathVariable Long id) {
        log.debug("REST request to delete AnswerCheckEvalAreaMap : {}", id);
        answerCheckEvalAreaMapRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
