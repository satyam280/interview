package com.wissen.interview.web.rest;
import com.wissen.interview.domain.CustomQuestion;
import com.wissen.interview.repository.CustomQuestionRepository;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import com.wissen.interview.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CustomQuestion.
 */
@RestController
@RequestMapping("/api")
public class CustomQuestionResource {

    private final Logger log = LoggerFactory.getLogger(CustomQuestionResource.class);

    private static final String ENTITY_NAME = "customQuestion";

    private final CustomQuestionRepository customQuestionRepository;

    public CustomQuestionResource(CustomQuestionRepository customQuestionRepository) {
        this.customQuestionRepository = customQuestionRepository;
    }

    /**
     * POST  /custom-questions : Create a new customQuestion.
     *
     * @param customQuestion the customQuestion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customQuestion, or with status 400 (Bad Request) if the customQuestion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/custom-questions")
    public ResponseEntity<CustomQuestion> createCustomQuestion(@Valid @RequestBody CustomQuestion customQuestion) throws URISyntaxException {
        log.debug("REST request to save CustomQuestion : {}", customQuestion);
        if (customQuestion.getId() != null) {
            throw new BadRequestAlertException("A new customQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomQuestion result = customQuestionRepository.save(customQuestion);
        return ResponseEntity.created(new URI("/api/custom-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /custom-questions : Updates an existing customQuestion.
     *
     * @param customQuestion the customQuestion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customQuestion,
     * or with status 400 (Bad Request) if the customQuestion is not valid,
     * or with status 500 (Internal Server Error) if the customQuestion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/custom-questions")
    public ResponseEntity<CustomQuestion> updateCustomQuestion(@Valid @RequestBody CustomQuestion customQuestion) throws URISyntaxException {
        log.debug("REST request to update CustomQuestion : {}", customQuestion);
        if (customQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CustomQuestion result = customQuestionRepository.save(customQuestion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customQuestion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /custom-questions : get all the customQuestions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of customQuestions in body
     */
    @GetMapping("/custom-questions")
    public ResponseEntity<List<CustomQuestion>> getAllCustomQuestions(Pageable pageable) {
        log.debug("REST request to get a page of CustomQuestions");
        Page<CustomQuestion> page = customQuestionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/custom-questions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /custom-questions/:id : get the "id" customQuestion.
     *
     * @param id the id of the customQuestion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customQuestion, or with status 404 (Not Found)
     */
    @GetMapping("/custom-questions/{id}")
    public ResponseEntity<CustomQuestion> getCustomQuestion(@PathVariable Long id) {
        log.debug("REST request to get CustomQuestion : {}", id);
        Optional<CustomQuestion> customQuestion = customQuestionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(customQuestion);
    }

    /**
     * DELETE  /custom-questions/:id : delete the "id" customQuestion.
     *
     * @param id the id of the customQuestion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/custom-questions/{id}")
    public ResponseEntity<Void> deleteCustomQuestion(@PathVariable Long id) {
        log.debug("REST request to delete CustomQuestion : {}", id);
        customQuestionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
