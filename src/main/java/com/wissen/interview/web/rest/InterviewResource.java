package com.wissen.interview.web.rest;
import com.wissen.interview.domain.Interview;
import com.wissen.interview.domain.User;
import com.wissen.interview.repository.InterviewRepository;
import com.wissen.interview.repository.UserRepository;
import com.wissen.interview.report.service.MailService;
import com.wissen.interview.service.UserService;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import com.wissen.interview.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Interview.
 */
@RestController
@RequestMapping("/api")
public class InterviewResource {

    private final Logger log = LoggerFactory.getLogger(InterviewResource.class);

    private static final String ENTITY_NAME = "interview";

    private final InterviewRepository interviewRepository;

    private final MailService mailService;

    private final UserRepository userRepository;

    private final UserService userService;

    public InterviewResource(InterviewRepository interviewRepository,MailService mailService,UserRepository userRepository,UserService userService) {
        this.interviewRepository = interviewRepository;
        this.mailService=mailService;
        this.userRepository=userRepository;
        this.userService=userService;
    }

    /**
     * POST  /interviews : Create a new interview.
     *
     * @param interview the interview to create
     * @return the ResponseEntity with status 201 (Created) and with body the new interview, or with status 400 (Bad Request) if the interview has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/interviews")
    public ResponseEntity<Interview> createInterview(@Valid @RequestBody Interview interview) throws URISyntaxException {
        log.debug("REST request to save Interview : {}", interview);
        if (interview.getId() != null) {
            throw new BadRequestAlertException("A new interview cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        log.debug("Getting user email from securityContext and adding it to interview Object");
        DefaultOidcUser defaultOidcUser =(DefaultOidcUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUser(defaultOidcUser.getAttributes());
        interview.setInterviewer(user.getEmail());
        
        Interview result = interviewRepository.save(interview);
        return ResponseEntity.created(new URI("/api/interviews/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /interviews : Updates an existing interview.
     *
     * @param interview the interview to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated interview,
     * or with status 400 (Bad Request) if the interview is not valid,
     * or with status 500 (Internal Server Error) if the interview couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/interviews")
    public ResponseEntity<Interview> updateInterview(@Valid @RequestBody Interview interview) throws URISyntaxException {
        
    	
    	log.debug("REST request to update Interview : {}", interview);
        if (interview.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    
        DefaultOidcUser defaultOidcUser =(DefaultOidcUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUser(defaultOidcUser.getAttributes());
        
        interview.setInterviewer(user.getEmail());
        Interview result = interviewRepository.save(interview);

        mailService.sendFeedbackMail(user, interview.getId());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, interview.getId().toString()))
            .body(result);
    }
    
   
    

    /**
     * GET  /interviews : get all the interviews.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of interviews in body
     */
    @GetMapping("/interviews")
    public ResponseEntity<List<Interview>> getAllInterviews(Pageable pageable) {
        log.debug("REST request to get a page of Interviews");
        Page<Interview> page = interviewRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/interviews");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /interviews/:id : get the "id" interview.
     *
     * @param id the id of the interview to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the interview, or with status 404 (Not Found)
     */
    @GetMapping("/interviews/{id}")
    public ResponseEntity<Interview> getInterview(@PathVariable Long id) {
        log.debug("REST request to get Interview : {}", id);
        Optional<Interview> interview = interviewRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(interview);
    }
    
    @GetMapping("/interviews-by-interviewer/{interviewer}")
    public List<Interview> getPendingInterviews(@PathVariable String interviewer){
    	List<Interview> interviews=interviewRepository.findByInterviewer(interviewer);
        return interviews;    	
    }

    /**
     * DELETE  /interviews/:id : delete the "id" interview.
     *
     * @param id the id of the interview to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/interviews/{id}")
    public ResponseEntity<Void> deleteInterview(@PathVariable Long id) {
        log.debug("REST request to delete Interview : {}", id);
        interviewRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    
    @PostMapping("/interviewss")
    public ResponseEntity<Interview> createInterviw(@Valid @RequestBody Interview interview) throws URISyntaxException {
        log.debug("REST request to save Interview : {}", interview);
        if (interview.getId() != null) {
            throw new BadRequestAlertException("A new interview cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        log.debug("Getting user email from securityContext and adding it to interview Object");
        DefaultOidcUser defaultOidcUser =(DefaultOidcUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUser(defaultOidcUser.getAttributes());
        interview.setInterviewer(user.getEmail());
        
        Interview result = interviewRepository.save(interview);
        return ResponseEntity.created(new URI("/api/interviews/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /interviews : Updates an existing interview.
     *
     * @param interview the interview to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated interview,
     * or with status 400 (Bad Request) if the interview is not valid,
     * or with status 500 (Internal Server Error) if the interview couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/interviewss")
    public ResponseEntity<Interview> updateInterviw(@Valid @RequestBody Interview interview) throws URISyntaxException {
        
    	
    	log.debug("REST request to update Interview : {}", interview);
        if (interview.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    
        DefaultOidcUser defaultOidcUser =(DefaultOidcUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUser(defaultOidcUser.getAttributes());
        
        interview.setInterviewer(user.getEmail());
        Interview result = interviewRepository.save(interview);

//        mailService.sendFeedbackMail(user, interview.getId());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, interview.getId().toString()))
            .body(result);
    }
    
   
    

    /**
     * GET  /interviews : get all the interviews.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of interviews in body
     */
    @GetMapping("/interviewss")
    public ResponseEntity<List<Interview>> getAllInterviws(Pageable pageable) {
        log.debug("REST request to get a page of Interviews");
        Page<Interview> page = interviewRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/interviews");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /interviews/:id : get the "id" interview.
     *
     * @param id the id of the interview to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the interview, or with status 404 (Not Found)
     */
    @GetMapping("/interviewss/{id}")
    public ResponseEntity<Interview> getInterviw(@PathVariable Long id) {
        log.debug("REST request to get Interview : {}", id);
        Optional<Interview> interview = interviewRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(interview);
    }

    /**
     * DELETE  /interviews/:id : delete the "id" interview.
     *
     * @param id the id of the interview to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/interviewss/{id}")
    public ResponseEntity<Void> deleteIntervew(@PathVariable Long id) {
        log.debug("REST request to delete Interview : {}", id);
        interviewRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
