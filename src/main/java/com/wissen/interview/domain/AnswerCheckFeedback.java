package com.wissen.interview.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import com.wissen.interview.domain.enumeration.AnswerCheckStatus;

/**
 * A AnswerCheckFeedback.
 */
@Entity
@Table(name = "answer_check_feedback")
public class AnswerCheckFeedback implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "answer_quality")
    private AnswerCheckStatus answer_quality;

    @ManyToOne
    private Interview interview;

    @ManyToOne
    private AnswerCheck answerCheck;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AnswerCheckStatus getAnswer_quality() {
        return answer_quality;
    }

    public AnswerCheckFeedback answer_quality(AnswerCheckStatus answer_quality) {
        this.answer_quality = answer_quality;
        return this;
    }

    public void setAnswer_quality(AnswerCheckStatus answer_quality) {
        this.answer_quality = answer_quality;
    }

    public Interview getInterview() {
        return interview;
    }

    public AnswerCheckFeedback interview(Interview interview) {
        this.interview = interview;
        return this;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }

    public AnswerCheck getAnswerCheck() {
        return answerCheck;
    }

    public AnswerCheckFeedback answerCheck(AnswerCheck answerCheck) {
        this.answerCheck = answerCheck;
        return this;
    }

    public void setAnswerCheck(AnswerCheck answerCheck) {
        this.answerCheck = answerCheck;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnswerCheckFeedback answerCheckFeedback = (AnswerCheckFeedback) o;
        if (answerCheckFeedback.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), answerCheckFeedback.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "AnswerCheckFeedback [" + (id != null ? "id=" + id + ", " : "")
				+ (answer_quality != null ? "answer_quality=" + answer_quality + ", " : "")
				+ (interview != null ? "interview=" + interview + ", " : "")
				+ (answerCheck != null ? "answerCheck=" + answerCheck : "") + "]";
	}

}
