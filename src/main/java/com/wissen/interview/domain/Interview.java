package com.wissen.interview.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.wissen.interview.domain.enumeration.InterviewStatus;

import com.wissen.interview.domain.enumeration.InterviewResult;

/**
 * A Interview.
 */
@Entity
@Table(name = "interview")
public class Interview implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "interviewer", nullable = false)
    private String interviewer;

    @NotNull
    @Column(name = "candidate", nullable = false)
    private String candidate;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "started_at")
    private ZonedDateTime startedAt;

    @Column(name = "finished_at")
    private ZonedDateTime finishedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private InterviewStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "result")
    private InterviewResult result;

    @Size(max = 2000)
    @Column(name = "feedback", length = 2000)
    private String feedback;

    @Min(value = 0)
    @Column(name = "experience")
    private Integer experience;

    @ManyToOne
    @JsonIgnoreProperties("interviews")
    private Skill skill;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public Interview interviewer(String interviewer) {
        this.interviewer = interviewer;
        return this;
    }

    public void setInterviewer(String interviewer) {
        this.interviewer = interviewer;
    }

    public String getCandidate() {
        return candidate;
    }

    public Interview candidate(String candidate) {
        this.candidate = candidate;
        return this;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Interview createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public Interview startedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public Interview finishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
        return this;
    }

    public void setFinishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public InterviewStatus getStatus() {
        return status;
    }

    public Interview status(InterviewStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(InterviewStatus status) {
        this.status = status;
    }

    public InterviewResult getResult() {
        return result;
    }

    public Interview result(InterviewResult result) {
        this.result = result;
        return this;
    }

    public void setResult(InterviewResult result) {
        this.result = result;
    }

    public String getFeedback() {
        return feedback;
    }

    public Interview feedback(String feedback) {
        this.feedback = feedback;
        return this;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Integer getExperience() {
        return experience;
    }

    public Interview experience(Integer experience) {
        this.experience = experience;
        return this;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Skill getSkill() {
        return skill;
    }

    public Interview skill(Skill skill) {
        this.skill = skill;
        return this;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Interview interview = (Interview) o;
        if (interview.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), interview.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Interview{" +
            "id=" + getId() +
            ", interviewer='" + getInterviewer() + "'" +
            ", candidate='" + getCandidate() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            ", status='" + getStatus() + "'" +
            ", result='" + getResult() + "'" +
            ", feedback='" + getFeedback() + "'" +
            ", experience=" + getExperience() +
            "}";
    }
}
