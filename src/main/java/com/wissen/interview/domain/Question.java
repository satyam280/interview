package com.wissen.interview.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Question.
 */
@Entity
@Table(name = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Min(value = 0)
    @Column(name = "min_experience")
    private Integer min_experience;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description", length = 2000, nullable = false)
    private String description;

    @Size(max = 2000)
    @Column(name = "expected_answers", length = 2000)
    private String expected_answers;

    @Column(name = "sequence")
    private Integer sequence;

    @Column(name = "is_active")
    private Boolean isActive;

    @ManyToOne
    @JsonIgnoreProperties("questions")
    private Skill skill;

    @ManyToOne
    @JsonIgnoreProperties("questions")
    private Topic topic;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMin_experience() {
        return min_experience;
    }

    public Question min_experience(Integer min_experience) {
        this.min_experience = min_experience;
        return this;
    }

    public void setMin_experience(Integer min_experience) {
        this.min_experience = min_experience;
    }

    public String getDescription() {
        return description;
    }

    public Question description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpected_answers() {
        return expected_answers;
    }

    public Question expected_answers(String expected_answers) {
        this.expected_answers = expected_answers;
        return this;
    }

    public void setExpected_answers(String expected_answers) {
        this.expected_answers = expected_answers;
    }

    public Integer getSequence() {
        return sequence;
    }

    public Question sequence(Integer sequence) {
        this.sequence = sequence;
        return this;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public Question isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Skill getSkill() {
        return skill;
    }

    public Question skill(Skill skill) {
        this.skill = skill;
        return this;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Topic getTopic() {
        return topic;
    }

    public Question topic(Topic topic) {
        this.topic = topic;
        return this;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Question)) {
            return false;
        }
        return id != null && id.equals(((Question) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", min_experience=" + getMin_experience() +
            ", description='" + getDescription() + "'" +
            ", expected_answers='" + getExpected_answers() + "'" +
            ", sequence=" + getSequence() +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }
}
