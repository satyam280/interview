package com.wissen.interview.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import com.wissen.interview.domain.enumeration.CommunicationSkills;

import com.wissen.interview.domain.enumeration.MotivationLevel;

/**
 * A SoftSkillsFeedback.
 */
@Entity
@Table(name = "soft_skills_feedback")
public class SoftSkillsFeedback implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "communication")
    private CommunicationSkills communication;

    @Enumerated(EnumType.STRING)
    @Column(name = "motivation_level")
    private MotivationLevel motivationLevel;

    @OneToOne
    @JoinColumn(unique = true)
    private Interview interview;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CommunicationSkills getCommunication() {
        return communication;
    }

    public SoftSkillsFeedback communication(CommunicationSkills communication) {
        this.communication = communication;
        return this;
    }

    public void setCommunication(CommunicationSkills communication) {
        this.communication = communication;
    }

    public MotivationLevel getMotivationLevel() {
        return motivationLevel;
    }

    public SoftSkillsFeedback motivationLevel(MotivationLevel motivationLevel) {
        this.motivationLevel = motivationLevel;
        return this;
    }

    public void setMotivationLevel(MotivationLevel motivationLevel) {
        this.motivationLevel = motivationLevel;
    }

    public Interview getInterview() {
        return interview;
    }

    public SoftSkillsFeedback interview(Interview interview) {
        this.interview = interview;
        return this;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SoftSkillsFeedback softSkillsFeedback = (SoftSkillsFeedback) o;
        if (softSkillsFeedback.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), softSkillsFeedback.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SoftSkillsFeedback{" +
            "id=" + getId() +
            ", communication='" + getCommunication() + "'" +
            ", motivationLevel='" + getMotivationLevel() + "'" +
            "}";
    }
}
