package com.wissen.interview.domain.enumeration;

/**
 * The AnswerQuality enumeration.
 */
public enum AnswerQuality {
    EXCELLENT, SATISFACTORY, UNSATISFACTORY, POOR
}
