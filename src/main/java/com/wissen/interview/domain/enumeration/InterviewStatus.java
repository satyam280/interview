package com.wissen.interview.domain.enumeration;

/**
 * The InterviewStatus enumeration.
 */
public enum InterviewStatus {
    CREATED, STARTED, FINISHED
}
