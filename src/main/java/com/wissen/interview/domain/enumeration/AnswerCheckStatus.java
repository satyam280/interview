package com.wissen.interview.domain.enumeration;

/**
 * The AnswerCheckStatus enumeration.
 */
public enum AnswerCheckStatus {
    PASS, FAIL, NOTSURE
}
