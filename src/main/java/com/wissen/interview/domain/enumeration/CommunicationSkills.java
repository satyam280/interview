package com.wissen.interview.domain.enumeration;

/**
 * The CommunicationSkills enumeration.
 */
public enum CommunicationSkills {
    POOR, SATISFACTORY, GOOD
}
