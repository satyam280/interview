package com.wissen.interview.domain.enumeration;

/**
 * The InterviewResult enumeration.
 */
public enum InterviewResult {
    MUST_HIRE,HIRE,KNOWLEDGE_GAP,REJECT,SELECT
}
