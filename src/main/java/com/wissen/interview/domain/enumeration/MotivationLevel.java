package com.wissen.interview.domain.enumeration;

/**
 * The MotivationLevel enumeration.
 */
public enum MotivationLevel {
    LOW,AVERAGE,HIGH
}
