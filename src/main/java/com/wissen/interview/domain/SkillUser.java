package com.wissen.interview.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A SkillUser.
 */
@Entity
@Table(name = "skill_user")
public class SkillUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Pattern(regexp = ".+\\@.+\\..+")
    @Column(name = "user_email_id", nullable = false)
    private String userEmailId;

    @ManyToOne
    private Skill skill;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public SkillUser userEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
        return this;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public Skill getSkill() {
        return skill;
    }

    public SkillUser skill(Skill skill) {
        this.skill = skill;
        return this;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SkillUser)) {
            return false;
        }
        return id != null && id.equals(((SkillUser) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SkillUser{" +
            "id=" + getId() +
            ", userEmailId='" + getUserEmailId() + "'" +
            "}";
    }
}
