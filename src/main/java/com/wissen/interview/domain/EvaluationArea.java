package com.wissen.interview.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A EvaluationArea.
 */
@Entity
@Table(name = "evaluation_area")
public class EvaluationArea implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public EvaluationArea name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EvaluationArea evaluationArea = (EvaluationArea) o;
        if (evaluationArea.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evaluationArea.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvaluationArea{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
