package com.wissen.interview.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.wissen.interview.domain.enumeration.AnswerQuality;

/**
 * A QuestionFeedback.
 */
@Entity
@Table(name = "question_feedback")
public class QuestionFeedback implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "answer_quality")
	private AnswerQuality answer_quality;

	@Column(name = "started_at")
	private ZonedDateTime startedAt;

	@Column(name = "finished_at")
	private ZonedDateTime finishedAt;

	@Column(name = "duration")
	private Long duration;

	@Size(max = 2000)
	@Column(name = "description", length = 2000)
	private String description;

	@ManyToOne
	@JsonIgnoreProperties("questionFeedbacks")
	private Interview interview;

	@ManyToOne
	@JsonIgnoreProperties("questionFeedbacks")
	private Question question;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AnswerQuality getAnswer_quality() {
		return answer_quality;
	}

	public QuestionFeedback answer_quality(AnswerQuality answer_quality) {
		this.answer_quality = answer_quality;
		return this;
	}

	public void setAnswer_quality(AnswerQuality answer_quality) {
		this.answer_quality = answer_quality;
	}

	public ZonedDateTime getStartedAt() {
		return startedAt;
	}

	public QuestionFeedback startedAt(ZonedDateTime startedAt) {
		this.startedAt = startedAt;
		return this;
	}

	public void setStartedAt(ZonedDateTime startedAt) {
		this.startedAt = startedAt;
	}

	public ZonedDateTime getFinishedAt() {
		return finishedAt;
	}

	public QuestionFeedback finishedAt(ZonedDateTime finishedAt) {
		this.finishedAt = finishedAt;
		return this;
	}

	public void setFinishedAt(ZonedDateTime finishedAt) {
		this.finishedAt = finishedAt;
	}

	public Long getDuration() {
		return duration;
	}

	public QuestionFeedback duration(Long duration) {
		this.duration = duration;
		return this;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public QuestionFeedback description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Interview getInterview() {
		return interview;
	}

	public QuestionFeedback interview(Interview interview) {
		this.interview = interview;
		return this;
	}

	public void setInterview(Interview interview) {
		this.interview = interview;
	}

	public Question getQuestion() {
		return question;
	}

	public QuestionFeedback question(Question question) {
		this.question = question;
		return this;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		QuestionFeedback questionFeedback = (QuestionFeedback) o;
		if (questionFeedback.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), questionFeedback.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "QuestionFeedback{" + "id=" + getId() + ", answer_quality='" + getAnswer_quality() + "'"
				+ ", startedAt='" + getStartedAt() + "'" + ", finishedAt='" + getFinishedAt() + "'" + ", duration="
				+ getDuration() + ", description='" + getDescription() + "'" + "}";
	}
}
