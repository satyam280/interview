package com.wissen.interview.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.wissen.interview.domain.enumeration.AnswerQuality;

/**
 * A CustomQuestion.
 */
@Entity
@Table(name = "custom_question")
public class CustomQuestion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 2000)
    @Column(name = "question_text", length = 2000)
    private String question_text;

    @Enumerated(EnumType.STRING)
    @Column(name = "answer_quality")
    private AnswerQuality answerQuality;

    @Size(max = 2000)
    @Column(name = "answer_text", length = 2000)
    private String answerText;

    @Column(name = "started_at")
    private ZonedDateTime startedAt;

    @Column(name = "finished_at")
    private ZonedDateTime finishedAt;

    @ManyToOne
    private Interview interview;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion_text() {
        return question_text;
    }

    public CustomQuestion question_text(String question_text) {
        this.question_text = question_text;
        return this;
    }

    public void setQuestion_text(String question_text) {
        this.question_text = question_text;
    }

    public AnswerQuality getAnswerQuality() {
        return answerQuality;
    }

    public CustomQuestion answerQuality(AnswerQuality answerQuality) {
        this.answerQuality = answerQuality;
        return this;
    }

    public void setAnswerQuality(AnswerQuality answerQuality) {
        this.answerQuality = answerQuality;
    }

    public String getAnswerText() {
        return answerText;
    }

    public CustomQuestion answerText(String answerText) {
        this.answerText = answerText;
        return this;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public CustomQuestion startedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public CustomQuestion finishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
        return this;
    }

    public void setFinishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Interview getInterview() {
        return interview;
    }

    public CustomQuestion interview(Interview interview) {
        this.interview = interview;
        return this;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CustomQuestion customQuestion = (CustomQuestion) o;
        if (customQuestion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customQuestion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CustomQuestion{" +
            "id=" + getId() +
            ", question_text='" + getQuestion_text() + "'" +
            ", answerQuality='" + getAnswerQuality() + "'" +
            ", answerText='" + getAnswerText() + "'" +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            "}";
    }
}
