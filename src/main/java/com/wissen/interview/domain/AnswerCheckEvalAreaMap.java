package com.wissen.interview.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A AnswerCheckEvalAreaMap.
 */
@Entity
@Table(name = "answer_check_eval_area_map")
public class AnswerCheckEvalAreaMap implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    private AnswerCheck answerCheck;

    @ManyToOne
    private EvaluationArea evaluationArea;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AnswerCheck getAnswerCheck() {
        return answerCheck;
    }

    public AnswerCheckEvalAreaMap answerCheck(AnswerCheck answerCheck) {
        this.answerCheck = answerCheck;
        return this;
    }

    public void setAnswerCheck(AnswerCheck answerCheck) {
        this.answerCheck = answerCheck;
    }

    public EvaluationArea getEvaluationArea() {
        return evaluationArea;
    }

    public AnswerCheckEvalAreaMap evaluationArea(EvaluationArea evaluationArea) {
        this.evaluationArea = evaluationArea;
        return this;
    }

    public void setEvaluationArea(EvaluationArea evaluationArea) {
        this.evaluationArea = evaluationArea;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnswerCheckEvalAreaMap answerCheckEvalAreaMap = (AnswerCheckEvalAreaMap) o;
        if (answerCheckEvalAreaMap.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), answerCheckEvalAreaMap.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AnswerCheckEvalAreaMap{" +
            "id=" + getId() +
            "}";
    }
}
