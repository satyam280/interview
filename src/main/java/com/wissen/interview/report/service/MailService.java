package com.wissen.interview.report.service;

import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.wissen.interview.domain.User;
import com.wissen.interview.interviewrun.InterviewSummary;
import com.wissen.interview.report.repository.InterviewSummaryRepository;

import io.github.jhipster.config.JHipsterProperties;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

	private final Logger log = LoggerFactory.getLogger(MailService.class);

	private static final String USER = "user";

	private static final String INTERVIEW_SUMMARY = "interviewSummary";

	private static final String BASE_URL = "baseUrl";

	private final JHipsterProperties jHipsterProperties;
	
    @Value("${app.devGroupMail}")
    private  String devGroupMail;

    @Value("${app.hrGrpMail}")
    private String hrGrpMail;
    
	@Autowired
	private final JavaMailSender javaMailSender;

	private final MessageSource messageSource;

	private final SpringTemplateEngine templateEngine;

	@Autowired
	private InterviewSummaryRepository interviewSummaryRepository;

	public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
			MessageSource messageSource, SpringTemplateEngine templateEngine) {

		this.jHipsterProperties = jHipsterProperties;
		this.javaMailSender = javaMailSender;
		this.messageSource = messageSource;
		this.templateEngine = templateEngine;
	}

	@Async
	public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
				isHtml, to, subject, content);

		String cc[]=new String[2];
		cc[0]=this.devGroupMail;
		cc[1]=this.hrGrpMail;
		
		// Prepare message using a Spring helper
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
			message.setTo(to);
			message.setCc(cc);
			message.setFrom(jHipsterProperties.getMail().getFrom());
			message.setSubject(subject);
			message.setText(content, isHtml);
			javaMailSender.send(mimeMessage);
			log.debug("Sent email to User '{}'", to);
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to user '{}'", to, e);
			} else {
				log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
			}
		}
	}

	@Async
	public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		String content = templateEngine.process(templateName, context);
		String subject = messageSource.getMessage(titleKey, null, locale);
		sendEmail(user.getEmail(), subject, content, false, true);
	}

	@Async
	public void sendActivationEmail(User user) {
		log.debug("Sending activation email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
	}

	@Async
	public void sendCreationEmail(User user) {
		log.debug("Sending creation email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
	}

	@Async
	public void sendPasswordResetMail(User user) {
		log.debug("Sending password reset email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
	}

	@Async
	public void sendFeedbackMail(User user, Long interviewId) {
        log.debug("called Send Feeback Mail method called from after completeing the feedback");
		Optional<InterviewSummary> interviewSummaryOptional = interviewSummaryRepository
				.getInterviewSummary(interviewId);
		InterviewSummary interviewSummary = interviewSummaryOptional.get();
		if (Objects.nonNull(interviewSummary)) {
            log.debug("calling sendEmailFromFeedbackTemplate called from after completeing the feedback");
			//sendEmailFromFeedbackTemplate(user, "mail/feedbackEmail", "email.feedback.title", interviewSummary);

            sendEmailFromFeedbackTemplate(user, "mail/feedbackEmailTemplate", "email.feedback.title", interviewSummary);
		}
	}

	@Async
	public void sendEmailFromFeedbackTemplate(User user, String templateName, String titleKey,
			InterviewSummary interviewSummary) {
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(INTERVIEW_SUMMARY, interviewSummary);
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		String content = templateEngine.process(templateName, context);
		log.debug(interviewSummary.getInterview().getResult()+" :result");
		String subject = interviewSummary.getInterview().getResult() + " - " + messageSource.getMessage(titleKey,
				new String[] { interviewSummary.getInterview().getCandidate() }, locale);
		
		sendEmail(user.getEmail(), subject, content, false, true);
		
//		log.debug("Sending Email to Interview Developers Group");
//		subject = user.getEmail() + " => " +subject; 
//		sendEmail(devGroupMail,subject,content,false,true);
		log.debug("Email sent to Interview Developers Group");

	}
}
