package com.wissen.interview.report.repository;

import com.wissen.interview.domain.*;
import com.wissen.interview.domain.enumeration.AnswerCheckStatus;
import com.wissen.interview.domain.enumeration.CommunicationSkills;
import com.wissen.interview.domain.enumeration.InterviewResult;
import com.wissen.interview.domain.enumeration.MotivationLevel;
import com.wissen.interview.interviewrun.InterviewSummary;
import com.wissen.interview.interviewrun.QuestionFeedbackDetails;
import com.wissen.interview.repository.InterviewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InterviewSummaryRepository {
    public static final String QUERY = "SELECT * FROM (                                                                               "
        + System.lineSeparator()
        + "  SELECT I.ID INTERVIEW_ID, I.CANDIDATE, I.INTERVIEWER, S.NAME SKILL, I.EXPERIENCE, I.RESULT, I.FEEDBACK,        "
        + System.lineSeparator()
        + "    Q.ID QUESTION_ID, Q.DESCRIPTION QUESTION, QF.ANSWER_QUALITY, QF.DESCRIPTION QUESTION_FEEDBACK,              "
        + System.lineSeparator()
        + "    AC.ID ANSWER_CHECK_ID, AC.NAME ANSWER_CHECK,ACF.ANSWER_QUALITY ANSWER_CHECK_QUALITY, QF.FINISHED_AT QUESTION_TIME,  "
        + System.lineSeparator()
        + "    SF.COMMUNICATION,SF.MOTIVATION_LEVEL"
        + System.lineSeparator()
        + "  FROM                                                                                        "
        + System.lineSeparator()
        + "    INTERVIEW I                                                                               "
        + System.lineSeparator()
        + "    LEFT JOIN SKILL S ON I.SKILL_ID = S.ID                                                    "
        + System.lineSeparator()
        + "    LEFT JOIN QUESTION_FEEDBACK QF ON QF.INTERVIEW_ID = I.ID                                  "
        + System.lineSeparator()
        + "    LEFT JOIN QUESTION Q ON Q.ID = QF.QUESTION_ID                            "
        + System.lineSeparator()
        + "    LEFT JOIN ANSWER_CHECK AC ON AC.QUESTION_ID=Q.ID                                             "
        + System.lineSeparator()
        + "    LEFT JOIN SOFT_SKILLS_FEEDBACK SF ON SF.INTERVIEW_ID=I.ID                                  "
        + System.lineSeparator()
        + "    LEFT JOIN ANSWER_CHECK_FEEDBACK ACF ON ACF.ANSWER_CHECK_ID=AC.ID AND ACF.INTERVIEW_ID=I.ID                                  "
        + System.lineSeparator() + "WHERE I.ID=:interviewId"
        + "  UNION                                                                                       "
        + System.lineSeparator()
        + "  SELECT I.ID INTERVIEW_ID, I.CANDIDATE, I.INTERVIEWER, S.NAME SKILL, I.EXPERIENCE, I.RESULT, I.FEEDBACK,        "
        + System.lineSeparator()
        + "    CQ.ID QUESTION_ID, CQ.QUESTION_TEXT QUESTION, CQ.ANSWER_QUALITY, CQ.ANSWER_TEXT QUESTION_FEEDBACK,           "
        + System.lineSeparator()
        + "    0 ANSWER_CHECK_ID, '' ANSWER_CHECK, '' ANSWER_CHECK_QUALITY, CQ.FINISHED_AT QUESTION_TIME,                "
         + System.lineSeparator()
        + "    SF.COMMUNICATION,SF.MOTIVATION_LEVEL"
        + System.lineSeparator()
        + "  FROM                                                                                        "
        + System.lineSeparator()
        + "    INTERVIEW I                                                                               "
        + System.lineSeparator()
        + "    LEFT JOIN SKILL S ON I.SKILL_ID = S.ID                                                    "
        + System.lineSeparator()
        + "    LEFT JOIN SOFT_SKILLS_FEEDBACK SF ON SF.INTERVIEW_ID=I.ID                                  "
        + System.lineSeparator()
        + "    JOIN CUSTOM_QUESTION CQ ON CQ.INTERVIEW_ID = I.ID                                        "
        + System.lineSeparator() + "WHERE I.ID=:interviewId" + System.lineSeparator()
        + ") as INTERVIEW_SUMMARY ORDER BY QUESTION_ID                                                                        ";

    public static final String EVALUATION_QUERY = "SELECT EA.NAME, ACF.ANSWER_QUALITY, COUNT(*) as COUNT "
        + System.lineSeparator()
        + "  FROM                                                                           "
        + System.lineSeparator()
        + " INTERVIEW I                                                                     "
        + System.lineSeparator()
        + " JOIN ANSWER_CHECK_FEEDBACK ACF ON ACF.INTERVIEW_ID = I.ID                       "
        + System.lineSeparator()
        + " JOIN ANSWER_CHECK AC ON ACF.ANSWER_CHECK_ID = AC.ID                            "
        + System.lineSeparator()
        + "JOIN ANSWER_CHECK_EVAL_AREA_MAP ACEAM ON ACEAM.ANSWER_CHECK_ID = AC.ID           "
        + System.lineSeparator()
        + "JOIN EVALUATION_AREA EA ON ACEAM.EVALUATION_AREA_ID = EA.ID                      "
        + System.lineSeparator()
        + "WHERE I.ID=:interviewId                                                                "
        + System.lineSeparator()
        + "GROUP BY EA.NAME, ACF.ANSWER_QUALITY                                             ";

    private final Logger log = LoggerFactory.getLogger(InterviewSummaryRepository.class);
    private final InterviewRepository interviewRepository;

    @PersistenceContext
    private EntityManager manager;

    public InterviewSummaryRepository(InterviewRepository interviewRepository) {
        this.interviewRepository = interviewRepository;
    }

    public Optional<InterviewSummary> getInterviewSummary(Long interviewId) {
        InterviewSummary summary = null;
        Question question = null;
        QuestionFeedback questionFeedback = null;
        QuestionFeedbackDetails questionFeedbackDetails = null;
        AnswerCheckFeedback checkFeedback = null;
        SoftSkillsFeedback softSkillsFeedback=new SoftSkillsFeedback();
        HashMap<String, List<BigInteger>> evaluationSummary = null;
        try {
            List<Tuple> interviewTuples = this.manager.createNativeQuery(QUERY, Tuple.class)
                .setParameter("interviewId", interviewId).getResultList();

            List<Tuple> evaluationTuples = this.manager.createNativeQuery(EVALUATION_QUERY, Tuple.class)
                .setParameter("interviewId", interviewId).getResultList();

            BigInteger prevInterviewId = BigInteger.valueOf(Long.MIN_VALUE);
            BigInteger prevQuestionId = BigInteger.valueOf(Long.MIN_VALUE);
            BigInteger prevAnswerCheckId = BigInteger.valueOf(Long.MIN_VALUE);
            for (Tuple result : interviewTuples) {
                BigInteger resultInterviewId = (BigInteger) result.get("INTERVIEW_ID");
                if (!prevInterviewId.equals(resultInterviewId)) {
                    prevInterviewId = resultInterviewId;
                    Interview interview = new Interview();
                    summary = new InterviewSummary(interview);
                    interview.setId(resultInterviewId.longValue());
                    interview.setCandidate((String) result.get("CANDIDATE"));
                    interview.setInterviewer((String) result.get("INTERVIEWER"));
                    interview.setSkill(new Skill().name((String) result.get("SKILL")));
                    interview.setExperience((Integer) result.get("EXPERIENCE"));
                    interview.setFeedback((String) result.get("FEEDBACK"));
                    if (result.get("RESULT") != null) {
                        interview.setResult(InterviewResult.valueOf((String) result.get("RESULT")));
                    }
                    interview.setFeedback((String) result.get("FEEDBACK"));
                    summary = new InterviewSummary(interview);
                    softSkillsFeedback.setInterview(interview);
                }
                softSkillsFeedback.setMotivationLevel(MotivationLevel.valueOf((String) result.get("MOTIVATION_LEVEL")));
                softSkillsFeedback.setCommunication(CommunicationSkills.valueOf((String) result.get("COMMUNICATION")));
            
                summary.setSoftSkillsFeedback(softSkillsFeedback);

                BigInteger questionId = (BigInteger) result.get("QUESTION_ID");
                if (!prevQuestionId.equals(questionId)) {
                    prevQuestionId = questionId;
                    question = new Question();
                    questionFeedback = new QuestionFeedback().question(question);
                    questionFeedback.description((String) result.get("QUESTION_FEEDBACK"));
                    questionFeedbackDetails = new QuestionFeedbackDetails(questionFeedback);
                    question.setId(questionId.longValue());
                    question.setDescription((String) result.get("QUESTION"));
                    summary.addQuestion(questionFeedbackDetails);
                }
                BigInteger answerCheckId = (BigInteger) result.get("ANSWER_CHECK_ID");
                if (!prevAnswerCheckId.equals(answerCheckId) && !BigInteger.ZERO.equals(answerCheckId)) {
                    prevAnswerCheckId = answerCheckId;
                    AnswerCheck check = new AnswerCheck();
                    checkFeedback = new AnswerCheckFeedback().answerCheck(check);
                    questionFeedbackDetails.addAnswerCheckFeedback(checkFeedback);
                    check.setId(((BigInteger) result.get("ANSWER_CHECK_ID")).longValue());
                    check.setName((String) result.get("ANSWER_CHECK"));
                    if (result.get("ANSWER_CHECK_QUALITY") != null) {
                        checkFeedback
                            .setAnswer_quality(AnswerCheckStatus.valueOf((String) result.get("ANSWER_CHECK_QUALITY")));
                    }
                }
            }
            setEvaluationMapping(summary, evaluationTuples);
        } catch (Exception e) {
            log.error("Unable to get interview summary", e);
            return Optional.empty();
        }
        if (summary != null) {
            summary.updateStats();
        }
        return Optional.of(summary);
    }

    private void setEvaluationMapping(InterviewSummary summary, List<Tuple> evaluationTuples) {
        HashMap<String, List<BigInteger>> evaluationSummary;
        evaluationSummary = summary.getEvaluationSummary();
        String name;
        for (Tuple evaluationTuple : evaluationTuples) {
            name = (String) evaluationTuple.get("NAME");
            if (AnswerCheckStatus.valueOf((String) evaluationTuple.get("ANSWER_QUALITY")) == AnswerCheckStatus.FAIL) {
                if (evaluationSummary.get(name) == null) {
                    List<BigInteger> countOfFailAndPass = new ArrayList<>();
                    countOfFailAndPass.add(BigInteger.ZERO);
                    countOfFailAndPass.add((BigInteger) evaluationTuple.get("COUNT"));
                    evaluationSummary.put(name, countOfFailAndPass);
                } else {
                    evaluationSummary.get(name).set(1, evaluationSummary.get(name).get(1).add ((BigInteger) evaluationTuple.get("COUNT")));
                }
            } else {
                if (evaluationSummary.get(name) == null) {
                    List<BigInteger> countOfFailAndPass = new ArrayList<>();
                    countOfFailAndPass.add((BigInteger) evaluationTuple.get("COUNT"));
                    countOfFailAndPass.add((BigInteger) evaluationTuple.get("COUNT"));
                    evaluationSummary.put(name, countOfFailAndPass);
                } else {
                    evaluationSummary.get(name).set(0, evaluationSummary.get(name).get(0).add((BigInteger) evaluationTuple.get("COUNT")));
                    evaluationSummary.get(name).set(1, evaluationSummary.get(name).get(1).add((BigInteger) evaluationTuple.get("COUNT")));
                }
            }
        }
        
        
        for (Map.Entry<String, List<BigInteger>> entry : evaluationSummary.entrySet()) {
        	List<BigInteger> failPassRating =  entry.getValue(); 
        	BigInteger greenRating = getGreenRating(failPassRating.get(0).intValue(),failPassRating.get(1).intValue());
        	failPassRating.add(greenRating);
            entry.setValue(failPassRating);
        }
        
    }

    
    // This will add one extra field to list that is number of green stars, used to print on emailFeedbackTemplate
    private BigInteger getGreenRating(int coveredEvaluationAreaChecks, int totalEvaluationAreaChecks) {

		float decimalRating = (5 * coveredEvaluationAreaChecks) / totalEvaluationAreaChecks;
		int rating = Math.round(decimalRating);    	
    	return new BigInteger(rating+"");
	}

	public InterviewRepository getInterviewRepository() {
        return interviewRepository;
    }
}
