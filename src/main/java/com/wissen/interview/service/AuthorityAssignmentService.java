package com.wissen.interview.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wissen.interview.domain.AdminUser;
import com.wissen.interview.repository.AdminUserRepository;
import com.wissen.interview.security.AuthoritiesConstants;

@Service
public class AuthorityAssignmentService {
@Autowired
private AdminUserRepository adminUserRepository;

@Transactional
public Collection<String> AssignAuthority(String email) {
	AdminUser au=null;
	Collection<String> groups=new ArrayList<String>();
	groups.add(AuthoritiesConstants.USER);
	
//	groups.add(AuthoritiesConstants.ADMIN);	
	if (email != null)
		au = adminUserRepository.findByEmailId(email);
	if (au != null) {
		groups.add(AuthoritiesConstants.ADMIN);	
	}
	return groups;
}
}
