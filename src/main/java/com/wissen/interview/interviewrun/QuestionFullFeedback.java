/**
 * 
 */
package com.wissen.interview.interviewrun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.wissen.interview.domain.AnswerCheckFeedback;
import com.wissen.interview.domain.QuestionFeedback;

/**
 * @author Sameer
 *
 */
public final class QuestionFullFeedback {
	private final QuestionFeedback questionFeedback;
	@Override
	public String toString() {
		return "QuestionFullFeedback ["
				+ (questionFeedback != null ? "questionFeedback=" + questionFeedback + ", " : "")
				+ (answerCheckFeedbacks != null ? "answerCheckFeedbacks=" + answerCheckFeedbacks : "") + "]";
	}

	private final List<AnswerCheckFeedback> answerCheckFeedbacks;

	public QuestionFullFeedback(QuestionFeedback questionFeedback, List<AnswerCheckFeedback> answerCheckFeedbacks)  {
		this.questionFeedback = questionFeedback;
		this.answerCheckFeedbacks = new ArrayList<>(answerCheckFeedbacks);
	}

	public QuestionFeedback getQuestionFeedback() {
		return questionFeedback;
	}

	public List<AnswerCheckFeedback> getAnswerCheckFeedbacks() {
		return Collections.unmodifiableList(answerCheckFeedbacks);
	}

}
