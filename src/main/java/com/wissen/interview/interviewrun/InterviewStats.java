package com.wissen.interview.interviewrun;

import java.util.EnumMap;
import java.util.List;

import com.wissen.interview.domain.AnswerCheckFeedback;
import com.wissen.interview.domain.QuestionFeedback;
import com.wissen.interview.domain.enumeration.AnswerCheckStatus;
import com.wissen.interview.domain.enumeration.AnswerQuality;

public class InterviewStats {
	private int numQuestions = 0;
	private EnumMap<AnswerQuality, Integer> questionStats = new EnumMap<>(AnswerQuality.class);
	private int numAnswerChecks;
	private EnumMap<AnswerCheckStatus, Integer> answerCheckStats = new EnumMap<>(AnswerCheckStatus.class);

	void addQuestionFeedback(QuestionFeedbackDetails details) {
		++numQuestions;
		QuestionFeedback questionFeedback = details.getQuestionFeedback();
		AnswerQuality answerQuality = questionFeedback.getAnswer_quality();
		if (answerQuality != null) {
			Integer count = questionStats.get(answerQuality);
			if (count == null) {
				count = 1;
			} else {
				count = count + 1;
			}
			questionStats.put(answerQuality, count);
		}

		List<AnswerCheckFeedback> answerCheckFeedbacks = details.getAnswerCheckFeedbacks();
		for (AnswerCheckFeedback answer : answerCheckFeedbacks) {
			++numAnswerChecks;
			AnswerCheckStatus answerCheckQuality = answer.getAnswer_quality();
			if (answerCheckQuality != null) {
				Integer count = answerCheckStats.get(answerCheckQuality);
				if (count == null) {
					count = 1;
				} else {
					count = count + 1;
				}
				answerCheckStats.put(answerCheckQuality, count);
			}
		}
	}

	public int getNumQuestions() {
		return numQuestions;
	}

	public EnumMap<AnswerQuality, Integer> getQuestionStats() {
		return questionStats;
	}

	public int getNumAnswerChecks() {
		return numAnswerChecks;
	}

	public EnumMap<AnswerCheckStatus, Integer> getAnswerCheckStats() {
		return answerCheckStats;
	}

}
