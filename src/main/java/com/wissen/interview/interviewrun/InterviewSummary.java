/**
 *
 */
package com.wissen.interview.interviewrun;

import com.wissen.interview.domain.Interview;
import com.wissen.interview.domain.SoftSkillsFeedback;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Sameer
 */
public final class InterviewSummary {
    private final Interview interview;
    private final List<QuestionFeedbackDetails> questions;
    private final InterviewStats interviewStats;
    private final HashMap<String, List<BigInteger>> evaluationSummary;
    private SoftSkillsFeedback softSkillsFeedback;

    public InterviewSummary(Interview interview) {
        this.interview = interview;
        this.questions = new ArrayList<>();
        this.interviewStats = new InterviewStats();
        this.evaluationSummary = new HashMap<>();
    }
    

    public Interview getInterview() {
        return interview;
    }

    public List<QuestionFeedbackDetails> getQuestions() {
        return Collections.unmodifiableList(questions);
    }

    public void addQuestion(QuestionFeedbackDetails questionFeedback) {
        questions.add(questionFeedback);
    }

    public void updateStats() {
        for (QuestionFeedbackDetails question : this.questions) {
            interviewStats.addQuestionFeedback(question);
        }
    }

    public HashMap<String, List<BigInteger>> getEvaluationSummary() {
        return evaluationSummary;
    }


	public SoftSkillsFeedback getSoftSkillsFeedback() {
		return softSkillsFeedback;
	}


	public void setSoftSkillsFeedback(SoftSkillsFeedback softSkillsFeedback) {
		this.softSkillsFeedback = softSkillsFeedback;
	}
    
}
