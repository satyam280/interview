package com.wissen.interview.interviewrun;

import com.google.common.collect.Lists;
import com.wissen.interview.domain.*;
import com.wissen.interview.report.repository.InterviewSummaryRepository;
import com.wissen.interview.repository.*;
import com.wissen.interview.report.service.MailService;
import com.wissen.interview.service.UserService;
import com.wissen.interview.web.rest.errors.BadRequestAlertException;
import com.wissen.interview.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Interview run.
 */
@RestController
@RequestMapping("/api")
public class InterviewrunResource {

    private static final String ENTITY_NAME = "interviewrun";
    private final Logger log = LoggerFactory.getLogger(InterviewrunResource.class);
    private final InterviewRepository interviewRepository;
    private final InterviewSummaryRepository interviewSummaryRepository;
    private final QuestionRepository questionRepository;
    private final AnswerCheckRepository answerCheckRepository;
    private final AnswerCheckFeedbackRepository answerCheckFeedbackRepository;
    private final QuestionFeedbackRepository questionFeedbackRepository;
    private final SkillUserRepository skillUserRepository;
    private final UserService userService;


    public InterviewrunResource(InterviewRepository interviewRepository,
                                InterviewSummaryRepository interviewSummaryRepository, QuestionRepository questionRepository,
                                AnswerCheckRepository answerCheckRepository, QuestionFeedbackRepository questionFeedbackRepository,
                                AnswerCheckFeedbackRepository answerCheckFeedbackRepository, MailService mailService,
                                UserService userService, UserRepository userRepository, SkillUserRepository skillUserRepository) {
        this.interviewRepository = interviewRepository;
        this.interviewSummaryRepository = interviewSummaryRepository;
        this.questionRepository = questionRepository;
        this.answerCheckRepository = answerCheckRepository;
        this.questionFeedbackRepository = questionFeedbackRepository;
        this.answerCheckFeedbackRepository = answerCheckFeedbackRepository;
        this.skillUserRepository = skillUserRepository;
        this.userService = userService;
    }

    /**
     * GET /interviewquestions/:interviewid : get the questions for "id"
     * interview.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the
     * interview, or with status 404 (Not Found)
     * @paramid the id of the interview to retrieve
     */
    @GetMapping("/interviewquestions/{interviewId}")
    public ResponseEntity<InterviewQuestions> getInterviewQuestions(@PathVariable Long interviewId) {
        log.debug("REST request to get interview questions : {}", interviewId);

        Optional<InterviewQuestions> interviewQuestions = Optional.empty();
        Optional<Interview> interview = interviewRepository.findById(interviewId);
        if (interview.isPresent()) {
            interviewQuestions = Optional.of(new InterviewQuestions(interview.get()));
            List<Question> questions = questionRepository.findBySkillExperience(interview.get().getExperience(),
                interview.get().getSkill());
            for (Question question : questions) {
                List<AnswerCheck> answerChecks = answerCheckRepository.findByQuestion(question);
                QuestionDetails questionDetails = new QuestionDetails(question);
                for (AnswerCheck check : answerChecks) {
                    questionDetails.addAnswerCheck(check);
                }
                interviewQuestions.get().addQuestion(questionDetails);
            }
        }
        return ResponseUtil.wrapOrNotFound(interviewQuestions);
    }

    /**
     * GET /interviewsummary/:interviewid : get the summary for "id" interview.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the
     * interview, or with status 404 (Not Found)
     * @paramid the id of the interview to retrieve
     */
    @GetMapping("/interviewsummary/{interviewId}")
    public ResponseEntity<InterviewSummary> getInterviewSummary(@PathVariable Long interviewId) {
        log.debug("REST request to get interview summary : {}", interviewId);
		/*
		Optional<InterviewSummary> interviewSummary = Optional.empty();
		Optional<Interview> interview = interviewRepository.findById(interviewId);
		if (interview.isPresent()) {
			interviewSummary = Optional.of(new InterviewSummary(interview.get()));
			List<QuestionFeedback> questionFeedbacks = questionFeedbackRepository.findByInterview(interviewId);
			List<AnswerCheckFeedback> answerCheckFeedbacks = answerCheckFeedbackRepository.findByInterview(interviewId);
			Map<Long, AnswerCheckFeedback> answerCheckFeedbackMap = new HashMap<>();
			for (AnswerCheckFeedback fb : answerCheckFeedbacks) {
				answerCheckFeedbackMap.put(fb.getAnswerCheck().getId(), fb);
			}
			for (QuestionFeedback questionFeedback : questionFeedbacks) {
				Question question = questionFeedback.getQuestion();
				List<AnswerCheck> answerChecks = answerCheckRepository.findByQuestion(question);
				QuestionFeedbackDetails qfDetails = new QuestionFeedbackDetails(questionFeedback);
				for (AnswerCheck c : answerChecks) {
					AnswerCheckFeedback acf = answerCheckFeedbackMap.get(c.getId());
					if (acf != null) {
						qfDetails.addAnswerCheckFeedback(acf);
					}
				}

				interviewSummary.get().addQuestion(qfDetails);
			}
		}*/
        return ResponseUtil.wrapOrNotFound(interviewSummaryRepository.getInterviewSummary(interviewId));
    }

    /**
     * POST /question-full-feedback : Create a new question feedback and answer
     * check feedbacks.
     *
     * @param questionFullFeedback the questionFeedback to create
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new questionFeedback, or with status 400 (Bad Request) if the
     * questionFeedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/question-full-feedback")
    public ResponseEntity<QuestionFeedback> createQuestionFeedback(
        @Valid @RequestBody QuestionFullFeedback questionFullFeedback) throws URISyntaxException {
        log.debug("REST request to save QuestionFeedback : {}", questionFullFeedback);

        QuestionFeedback questionFeedback = questionFullFeedback.getQuestionFeedback();
        if (questionFeedback == null) {
            throw new BadRequestAlertException("QuestionFeedback cannot be null", ENTITY_NAME, "null");
        }


        QuestionFeedback qf1 = questionFeedbackRepository.findByInterviewAndQuestion(questionFeedback.getInterview().getId(), questionFeedback.getQuestion().getId());
        QuestionFeedback result;
        if (qf1 != null) {
            questionFeedbackRepository.deleteById(qf1.getId());

        }
        result = questionFeedbackRepository.save(questionFeedback);
        List<AnswerCheckFeedback> answerCheckFeedbacks = questionFullFeedback.getAnswerCheckFeedbacks();


//        List<AnswerCheckFeedback>feedbacks = 	answerCheckFeedbackRepository.findByInterviewAndAnswerCheck(answerCheckFeedbacks.get(0).getInterview().getId(),answerCheckFeedbacks.get);

        if (answerCheckFeedbacks != null && !answerCheckFeedbacks.isEmpty()) {


            for (AnswerCheckFeedback answerCheckFeedback : answerCheckFeedbacks) {

                AnswerCheckFeedback aCF = answerCheckFeedbackRepository.findByAnswerCheckAndInterview(answerCheckFeedback.getAnswerCheck().getId(), answerCheckFeedback.getInterview().getId());

                if (aCF != null) {
                    answerCheckFeedbackRepository.deleteById(aCF.getId());
                }
                answerCheckFeedbackRepository.save(answerCheckFeedback);
            }
        }


        return ResponseEntity.created(new URI("/api/question-full-feedback/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * GET  /interviewerskills : get all the skills.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of skills in body
     */
    @GetMapping("/interviewerskills")
    public List<Skill> getAllSkillsForInterviewer() {
        log.debug("REST request to get all Skills");
        log.debug("Getting user email from securityContext and adding it to interview Object");
        DefaultOidcUser authenticatedUser = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUser(authenticatedUser.getAttributes());
        List<Skill> userSkills = skillUserRepository.findByUserEmailId(user.getEmail());

        String[] emailSplit = user.getEmail().split("@");
        List<Skill> everybodySkills = null;
        if (emailSplit.length == 2) {
            String domain = emailSplit[1];
            everybodySkills = skillUserRepository.findByUserEmailId("everybody@" + domain);
        }

        if (userSkills == null || userSkills.isEmpty())
                return everybodySkills;

            if (everybodySkills == null || everybodySkills.isEmpty())
                return userSkills;

            List<Skill> combinedList = Lists.newArrayList(userSkills);
            combinedList.addAll(everybodySkills);
            return combinedList;
        }

    }
