/**
 * 
 */
package com.wissen.interview.interviewrun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.wissen.interview.domain.Interview;

/**
 * @author Sameer
 *
 */
public final class InterviewQuestions {
	private final Interview interview;
	private final List<QuestionDetails> questions;

	public InterviewQuestions(Interview interview) {
		this.interview = interview;
		this.questions = new ArrayList<>();
	}

	public Interview getInterview() {
		return interview;
	}

	public List<QuestionDetails> getQuestions() {
		return Collections.unmodifiableList(questions);
	}

	public void addQuestion(QuestionDetails question) {
		questions.add(question);
	}
}
