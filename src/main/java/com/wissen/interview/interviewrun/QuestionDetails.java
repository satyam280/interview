package com.wissen.interview.interviewrun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.wissen.interview.domain.AnswerCheck;
import com.wissen.interview.domain.Question;

public final class QuestionDetails {

	private final Question question;
	private final List<AnswerCheck> answerChecks;

	public QuestionDetails(Question question) {
		this.question = question;
		this.answerChecks = new ArrayList<>();
	}

	public Question getQuestion() {
		return question;
	}

	public List<AnswerCheck> getAnswerChecks() {
		return Collections.unmodifiableList(answerChecks);
	}

	public void addAnswerCheck(AnswerCheck check) {
		this.answerChecks.add(check);
	}

}
