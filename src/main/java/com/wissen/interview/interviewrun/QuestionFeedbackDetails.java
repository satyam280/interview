package com.wissen.interview.interviewrun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wissen.interview.config.AsyncConfiguration;
import com.wissen.interview.domain.AnswerCheck;
import com.wissen.interview.domain.AnswerCheckFeedback;
import com.wissen.interview.domain.Question;
import com.wissen.interview.domain.QuestionFeedback;

public final class QuestionFeedbackDetails {

    private final Logger log = LoggerFactory.getLogger(AsyncConfiguration.class);

	private final QuestionFeedback questionFeedback;
	private final List<AnswerCheckFeedback> answerCheckFeedbacks;
	private int rating;
    private int grayRating;
	
	public void setRating() {
		int answerChecks = answerCheckFeedbacks.size();
		int passChecks = answerCheckFeedbacks.stream().filter((t) -> {
			return t.getAnswer_quality().toString().equals("PASS");
		}).collect(Collectors.toList()).size();

		float decimalRating=0;
		
		if(answerChecks!=0)
        decimalRating = (5 * passChecks) / answerChecks;

		
		this.rating = Math.round(decimalRating);

		log.debug("start");
		log.debug(answerChecks+"");
		log.debug(passChecks+"");
		log.debug(decimalRating+"");
		log.debug(this.rating+"");
        log.debug("end");
		
		System.out.println();

	}

	public int[] getRating() {
			return new int[this.rating];
	}
	
	public int[] getGrayRating() {
		return new int[5 - this.rating];
	}

	public QuestionFeedbackDetails(QuestionFeedback question) {
		this.questionFeedback = question;
		this.answerCheckFeedbacks = new ArrayList<>();
	}

	public QuestionFeedback getQuestionFeedback() {
		return questionFeedback;
	}

	public List<AnswerCheckFeedback> getAnswerCheckFeedbacks() {
		setRating();
		return Collections.unmodifiableList(answerCheckFeedbacks);
	}

	public void addAnswerCheckFeedback(AnswerCheckFeedback check) {
		this.answerCheckFeedbacks.add(check);
	}

}
