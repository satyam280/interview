package com.wissen.interview.repository;

import com.wissen.interview.domain.CustomQuestion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CustomQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomQuestionRepository extends JpaRepository<CustomQuestion, Long> {

}
