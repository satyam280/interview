package com.wissen.interview.repository;

import com.wissen.interview.domain.Skill;
import com.wissen.interview.domain.SkillUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the SkillUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SkillUserRepository extends JpaRepository<SkillUser, Long> {
    @Query("SELECT SU.skill FROM SkillUser SU WHERE SU.userEmailId = :email")
    List<Skill> findByUserEmailId(String email);
}
