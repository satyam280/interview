package com.wissen.interview.repository;

import com.wissen.interview.domain.Interview;
import com.wissen.interview.domain.Question;
import com.wissen.interview.domain.QuestionFeedback;
import com.wissen.interview.domain.Skill;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the QuestionFeedback entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionFeedbackRepository extends JpaRepository<QuestionFeedback, Long> {
	@Query("SELECT q FROM QuestionFeedback q WHERE q.interview.id = :interviewId")
	List<QuestionFeedback> findByInterview(Long interviewId);
	@Query("SELECT q FROM QuestionFeedback q WHERE q.interview.id = :interviewId and q.question.id = :questionId")
	QuestionFeedback findByInterviewAndQuestion(Long interviewId, Long questionId);

}
