package com.wissen.interview.repository;

import com.wissen.interview.domain.AnswerCheck;
import com.wissen.interview.domain.Question;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AnswerCheck entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnswerCheckRepository extends JpaRepository<AnswerCheck, Long> {
	@Query("SELECT a FROM AnswerCheck a WHERE a.question = :question")
	List<AnswerCheck> findByQuestion(Question question);

}
