package com.wissen.interview.repository;

import com.wissen.interview.domain.AnswerCheckFeedback;
import com.wissen.interview.domain.QuestionFeedback;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AnswerCheckFeedback entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnswerCheckFeedbackRepository extends JpaRepository<AnswerCheckFeedback, Long> {
	@Query("SELECT a FROM AnswerCheckFeedback a WHERE a.interview.id = :interviewId")
	List<AnswerCheckFeedback> findByInterview(Long interviewId);

	@Query("SELECT a FROM AnswerCheckFeedback a WHERE a.interview.id = :interviewId and a.answerCheck.id = :answerCheckId")
	AnswerCheckFeedback findByAnswerCheckAndInterview(Long answerCheckId, Long interviewId);
}
