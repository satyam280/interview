package com.wissen.interview.repository;

import com.wissen.interview.domain.Interview;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Interview entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InterviewRepository extends JpaRepository<Interview, Long> {

	@Query("SELECT i FROM Interview i where i.interviewer=:interviewer AND i.result=null")
	List<Interview> findByInterviewer(String interviewer);
}
