package com.wissen.interview.repository;

import com.wissen.interview.domain.EvaluationArea;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EvaluationArea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvaluationAreaRepository extends JpaRepository<EvaluationArea, Long> {



}
