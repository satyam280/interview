package com.wissen.interview.repository;

import com.wissen.interview.domain.AnswerCheck;
import com.wissen.interview.domain.Interview;
import com.wissen.interview.domain.Question;
import com.wissen.interview.domain.Skill;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Question entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
	@Query("SELECT q FROM Question q WHERE q.min_experience <= :experience and q.skill = :skill and q.isActive = true order by q.sequence")
	List<Question> findBySkillExperience(int experience, Skill skill);
}
