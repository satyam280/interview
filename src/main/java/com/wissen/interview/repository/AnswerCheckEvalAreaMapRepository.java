package com.wissen.interview.repository;

import com.wissen.interview.domain.AnswerCheckEvalAreaMap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AnswerCheckEvalAreaMap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnswerCheckEvalAreaMapRepository extends JpaRepository<AnswerCheckEvalAreaMap, Long> {

}
