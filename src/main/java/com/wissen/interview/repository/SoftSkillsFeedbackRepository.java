package com.wissen.interview.repository;

import com.wissen.interview.domain.SoftSkillsFeedback;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SoftSkillsFeedback entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SoftSkillsFeedbackRepository extends JpaRepository<SoftSkillsFeedback, Long> {

	@Query("SELECT q FROM SoftSkillsFeedback q WHERE q.interview.id = :interviewId")
	SoftSkillsFeedback findByInterview(Long interviewId);
}
