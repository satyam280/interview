export interface ICode{
    id:number;
    statement:string;
    defn:string;
    skill_id:number;
    handled:boolean;
    code?:string;
    category?:string[];
}