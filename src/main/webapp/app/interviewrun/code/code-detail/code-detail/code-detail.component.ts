import { Component, OnInit ,Input ,Output, EventEmitter} from '@angular/core';
import { ICode } from '../../code-model/code.model';
import { CodeService } from '../../code-sevice/code-service.service';

@Component({
  selector: 'wissen-code-detail',
  templateUrl: './code-detail.component.html',
  styleUrls: ['./code-detail.component.scss']
})
export class CodeDetailComponent implements OnInit {
  @Input() cq: ICode;
  @Output() nextStep = new EventEmitter();
  @Output() previousStep=new EventEmitter();
  @Output() updateList=new EventEmitter();
  //code:string=(this.cq===undefined)?"":this.cq.code;
  category:string[]=[];
  constructor(private codeService:CodeService) { 
    if(this.cq!==undefined){
      this.ngOnInit();
      //this.category=this.cq.category;
    }
  }

  ngOnInit() {
  }
  save(){
    //this.cq.code=this.code;
    //this.cq.category=this.category;
    this.cq.handled=true;
    console.log(this.cq.category);
    console.log(this.cq.code);
    console.log(this.cq);
    const c=confirm("Do you want to manage more codes?");
    if(c==true){
      this.codeService.updateList(this.cq);
      this.updateList.emit(this.codeService.getHandledStmts());
      this.previousStep.emit();
    }
    else
    {
      this.nextStep.emit();
    }
  }
}
