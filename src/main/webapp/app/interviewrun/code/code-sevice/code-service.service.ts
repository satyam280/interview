import { Injectable } from '@angular/core';
import { ICode } from '../code-model/code.model';

@Injectable({
  providedIn: 'root'
})
export class CodeService {

  constructor() { }

  getCodes():ICode[]{
    return carr; 
  }
  getCode(id:number):ICode{
    return carr.find(code=>code.id===id);
  }
  getCodeByStmt(st:string):ICode{
    return carr.find(code=>code.statement===st);
  }
  getStmts():string[]{
    let v=[];
    for(let i of carr){
      v.push(i.statement);
    }
    return v;
  }
  updateList(ic:ICode){
    let ic1=carr.find(code=>code.id===ic.id);
    let icin1=carr.findIndex(code=>code.id===ic.id);
    carr.splice(icin1,1,ic);
  }
  getHandledStmts():string[]{
    let v=[];
    for(let i of carr){
      if(i.handled===false)
      {
        v.push(i.statement);
      }
    }
    return v;
  }
}
const carr:ICode[]=[
  {
    id:1,
    statement:'Depth of Tree',
    defn:'The height of a binary tree is the number of edges between the trees root and its furthest leaf.The Height of binary tree with single node is taken as zero.',
    skill_id:1,
    handled:false
  },
  {
    id:2,
    statement:'Coin Change Problem',
    defn:'Given a value N, if we want to make change for N cents, and we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins, how many ways can we make the change? The order of coins does not matter.',
    skill_id:1,
    handled:false
  },
  {
    id:3,
    statement:'Most frequent element in an array',
    defn:'Given an array, find the most frequent element in it. If there are multiple elements that appear maximum number of times, print any one of them.',
    skill_id:1,
    handled:false
  }
]