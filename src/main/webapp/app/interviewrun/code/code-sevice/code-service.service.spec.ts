import { TestBed } from '@angular/core/testing';

import { CodeService } from './code-service.service';

describe('CodeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CodeService = TestBed.get(CodeService);
    expect(service).toBeTruthy();
  });
});
