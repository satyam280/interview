import { Component, OnInit ,Input ,Output, EventEmitter} from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { CodeService } from '../code-sevice/code-service.service';
import { ICode } from '../code-model/code.model';

@Component({
  selector: 'wissen-code-container',
  templateUrl: './code-container.component.html',
  styleUrls: ['./code-container.component.scss']
})
export class CodeContainerComponent implements OnInit {
  @Input() interview: IInterview;
  @Input() List:string[];
  @Output() nextStep = new EventEmitter();
  @Output() changeHandle=new EventEmitter();
  //code;
  //category:string;
  codeQts:string[]=[];
  selectedQt:string="";
  selCode:ICode=null;
  constructor(private codeService:CodeService) 
  { 
    // for(let c of this.codeService.getCodes()){
    //   if(c.handled===false){
    //     this.codeQts.push(c.statement);
    //   }
    // }
    this.codeQts=this.List;
  }

  ngOnInit() {
  }

  goBack(){
    window.history.back();
  }
  save(){
    this.selCode=this.codeService.getCodeByStmt(this.selectedQt);
    //this.selCode.handled=true;
    console.log(this.codeService.getCodes());
    console.log(this.codeQts);
    console.log(this.selectedQt);
    //console.log(JSON.stringify(this.selCode));
    this.changeHandle.emit(this.selCode);
    this.nextStep.emit();
  }
  Refresh(){
    //console.log(this.codeService.getCodes());
    let v=[];
        for(let c of this.codeService.getCodes()){
            if(c.handled===false){
                v.push(c.statement);
            }
        }
        console.log("v="+v);
    this.ngOnInit();
  }
}
