import { Component, OnInit, Output, ViewChild, ElementRef } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview/interview.service';
import { ISkill } from 'app/shared/model/skill.model';
import { SkillService } from 'app/entities/skill';
import { AccountService } from 'app/core';
import { InterviewrunService } from './interviewrun.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IACFeedback, IQFeedback } from 'app/shared/model/interviewrun.model';
import { StepperComponent } from 'app/shared/components/stepper/stepper.component';
import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';
import { IAnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';
import { ISoftSkillsFeedback } from '../shared/model/soft-skills-feedback.model';
import { ICode } from './code/code-model/code.model';
import { CodeService } from './code/code-sevice/code-service.service';

@Component({
    selector: 'wissen-interview-update',
    templateUrl: './interviewrun.component.html',
    styleUrls: ['interview.component.scss']
})
export class InterviewrunComponent implements OnInit {
    isLinear = false;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    @ViewChild('stepper') stepper;
    @ViewChild(StepperComponent) stpCmp: StepperComponent;
    interview: any;
    isPending: boolean;
    interviewId: number;
    acs: IAnswerCheckFeedback[] | any = [];
    qs: IQuestionFeedback | any = [];
    softSkill: ISoftSkillsFeedback;

    handledQuestion:ICode={
        id:0,
        statement:'',
        defn:'',
        skill_id:null,
        handled:false
    };
    codeList:string[];
    constructor(
        private _formBuilder: FormBuilder,
        protected interviewrunService: InterviewrunService,
        private location: LocationStrategy,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,private codeService:CodeService
    ) {
        history.pushState(null, null, window.location.href);
        this.location.onPopState(() => {
            history.pushState(null, null, window.location.href);
        });
        this.codeList=this.codeService.getHandledStmts();
        this.activatedRoute.params.subscribe(params => {
            console.log(params);
            if (params['id']) {
                this.interviewId = params['id'];
                this.isPending = true;
                this.interviewrunService.getAnswerCheckFeedbacks(this.interviewId).subscribe(res => {
                    console.log(res.body);
                    this.acs = res.body;
                    console.log(this.acs);
                });
                this.interviewrunService.getQuestionFeedbacks(this.interviewId).subscribe(res => {
                    console.log(res);
                    this.qs = res.body;
                    console.log(this.qs);
                });
                this.interviewrunService.getSoftSkillsFeedback(this.interviewId).subscribe(res => {
                    console.log('softskillfeedu' + res);
                    this.softSkill = res.body;
                    console.log(this.softSkill);
                });
            } else {
                this.isPending = false;
            }
        });
    }

    ngOnInit() {
        console.log(this.interviewId);
        console.log(this.isPending);
        if (this.isPending) {
        }
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', Validators.required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', Validators.required]
        });
    }

    stepperNext(stepper) {
        //  stepper.next();
        console.log('hhd' + stepper);
        this.stpCmp.nextStep();
    }

    stepperPrevious(stepper) {
        //  stepper.next();
        console.log(stepper);
        this.stpCmp.previousStep();
    }

    getInterviewId(ev) {
        this.interview = ev;
        console.log(ev);
        console.log(this.codeList);
        
    }
    setHandled(qt){
        this.handledQuestion=qt;
        console.log(qt);
    }
    updateList(cl){
        this.codeList=cl;
        console.log(cl);
    }
    goToCandidateInformation() {
        console.log('-----------------start---------------------');
        console.log('goint to candidate information');
    }

    goBack() {
        console.log('-----------------start---------------------');
        console.log('goint to candidate information');
    }
}
