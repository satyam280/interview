import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ISkill } from 'app/shared/model/skill.model';
import { InterviewrunService } from '../interviewrun.service';
import { IInterview } from 'app/shared/model/interview.model';
import { SkillService } from 'app/entities/skill';
import { filter, map } from 'rxjs/operators';
import { IQuestion } from 'app/shared/model/question.model';
import { IInterviewQuestions } from 'app/shared/model/interviewrun.model';
import { AnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckFeedback } from '../../shared/model/answer-check-feedback.model';

@Component({
    selector: 'wissen-questions-container',
    templateUrl: './questions-container.component.html',
    styleUrls: ['./questions-container.component.scss']
})
export class QuestionsContainerComponent implements OnInit {
    @Input() interview: IInterview;
    @Output() nextStep = new EventEmitter();
    @Input() isPending: boolean;
    @Input() acs: any;
    @Input() qs: any;
    acf: number[] = [];
    ff: string;
    qf: string;
    question: IQuestion;
    questions: IInterviewQuestions | any = [];
    currentQuestion;
    currentQuestionIndex: number = 0;
    isCustomQuestion: Boolean = false;
    feedback: string;
    formdata = new FData([], '', '');
    allQuestions: IFData[];
    constructor(private interviewrunService: InterviewrunService, private skillService: SkillService) {}

    ngOnInit() {
        console.log(this.interview);
        this.allQuestions = [];

        this.interviewrunService.getInterview$.subscribe(interview => {
            // this.interview = interview;
            // this.skillService
            //     .query()
            //     .pipe(
            //         filter((mayBeOk: HttpResponse<ISkill[]>) => mayBeOk.ok),
            //         map((response: HttpResponse<ISkill[]>) => response.body)
            //     )
            //     .subscribe((res: ISkill[]) => (this.skills = res), (res: HttpErrorResponse) => this.onError(res.message));
            // this.accountService.identity().then(account => {
            //     this.currentAccount = account;
            // });
            this.interviewrunService.getInterviewQuestions(interview.id).subscribe(res => {
                console.log('id of:' + this.isPending);
                console.log('acs:' + this.acs.length);
                console.log('qs:' + this.qs.length);
                this.questions = res.body.questions;
                console.log(res);
                var x = 0;
                console.log(this.questions);
                console.log(this.allQuestions.length);
                this.allQuestions = [];
                while (x < this.questions.length) {
                    if (this.isPending && this.acs.length !== 0) {
                        var t = 0;

                        var y = 0;
                        while (y < this.acs.length) {
                            console.log('acf:' + this.acs[y].answer_quality);
                            console.log(this.acs[y].answerCheck.question.id + ' ' + this.questions[x].question.id);
                            if (
                                this.acs[y].answer_quality === 'PASS' &&
                                this.acs[y].answerCheck.question.id === this.questions[x].question.id
                            ) {
                                console.log('pass');
                                this.acf.push(this.acs[y].answerCheck.id);
                            }
                            y++;
                        }
                        while (t < this.qs.length) {
                            if (this.questions[x].question.id === this.qs[t].question.id) {
                                this.ff = this.qs[t].description;
                                this.qf = this.qs[t].answer_quality;
                            }
                            t++;
                        }
                        this.allQuestions.push(new FData(this.acf, this.ff, this.qf));
                        this.acf = [];
                        this.ff = null;
                        this.qf = null;
                    } else {
                        this.allQuestions.push(new FData([], '', ''));
                    }
                    console.log(this.questions.length);
                    console.log(x);
                    x++;
                }
                console.log(this.allQuestions);
                this.setQuestion(this.currentQuestionIndex);
            });
        });
    }

    setQuestion(index: number) {
        // this.feedback = '';
        this.currentQuestion = this.questions[index];
        console.log('setQuestion' + this.feedback);
    }

    getFormData(p: FData) {
        let x = new FData(p.answerChecks, p.feedback, p.quality);
        console.log(this.allQuestions);
        console.log('p:' + p.feedback);
        console.log('getForm' + this.currentQuestionIndex);
        this.allQuestions.splice(this.currentQuestionIndex, 1, x);
    }

    showCustomQuestion() {
        this.isCustomQuestion = true;
    }

    nextQuestion() {
        console.log(this.allQuestions);
        this.currentQuestionIndex += 1;
        if (this.questions.length === this.currentQuestionIndex) {
            // this.feedback = '';
            this.nextStep.emit('true');
            this.currentQuestionIndex = this.questions.length - 1;
            return;
        }
        this.currentQuestionIndex = this.currentQuestionIndex % this.questions.length;
        // this.feedback = '';
        console.log('nextQuestion' + this.feedback);
        this.setQuestion(this.currentQuestionIndex);
    }

    customQuestionToggle() {
        this.setQuestion(this.currentQuestionIndex);
        this.isCustomQuestion = false;
    }

    previousQuestion() {
        console.log(this.allQuestions);
        this.currentQuestionIndex -= 1;
        // this.feedback = '';
        if (this.questions.length === this.currentQuestionIndex) {
            this.nextStep.emit('true');
            return;
        }
        this.setQuestion(this.currentQuestionIndex);
        console.log('previousQuestion' + this.feedback);
    }
}

interface IFData {
    answerChecks: any[];
    feedback: string;
    quality: string;
}
class FData implements IFData {
    constructor(public answerChecks: any[], public feedback: string, public quality: string) {}
}
