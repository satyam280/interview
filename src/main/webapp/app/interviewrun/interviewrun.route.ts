import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Question } from 'app/shared/model/question.model';
import { QuestionService } from 'app/entities/question/question.service';
import { Interview, IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview/interview.service';
import { InterviewrunComponent } from './interviewrun.component';
import { QuestionDetailComponent } from 'app/entities/question/question-detail.component';
import { IQuestion } from 'app/shared/model/question.model';
import { QuestionResolve } from 'app/entities/question';
import { InterviewrunFinishComponent } from './finish/finish.component';
import { InterviewrunQuestionComponent } from './question/question.component';

@Injectable({ providedIn: 'root' })
export class InterviewResolve implements Resolve<IInterview> {
    constructor(private service: InterviewService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IInterview> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Interview>) => response.ok),
                map((interview: HttpResponse<Interview>) => interview.body)
            );
        }
        return of(new Interview());
    }
}

export const interviewrunRoute: Routes = [
    {
        path: 'startinterview',
        component: InterviewrunComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.interviewrun.home.title'
        },
        canActivate: [UserRouteAccessService],
        resolve: {
            interview: InterviewResolve
        }
    },
    {
        path: 'interviewquestions',
        component: InterviewrunQuestionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.interviewrun.home.title'
        },
        canActivate: [UserRouteAccessService],
        resolve: {
            interview: InterviewResolve
        }
    },
    {
        path: 'finishinterview',
        component: InterviewrunFinishComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.interviewrun.home.title'
        },
        canActivate: [UserRouteAccessService],
        resolve: {
            interview: InterviewResolve
        }
    }
];
