import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { interviewrunRoute } from './interviewrun.route';
import { RouterModule } from '@angular/router';
import { InterviewrunComponent } from './interviewrun.component';
import { JhiLanguageService } from 'ng-jhipster';

import { SharedModule } from 'app/shared';
import { InterviewrunQuestionComponent } from './question/question.component';
import { InterviewrunFinishComponent } from './finish/finish.component';
import { CandidateInformationComponent } from './candidate-information/candidate-information.component';
import { InverviewFeedbackComponent } from './feedback/feedback.component';

import { MatNativeDateModule } from '@angular/material/core';
import { MatFormField } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { QuestionsContainerComponent } from './questions-container/questions-container.component';
import { CustomQuestionComponent } from './custom-question/custom-question.component';
import { SoftSkillFeedbackComponent } from './soft-skill-feedback/soft-skill-feedback.component';
import { CodeContainerComponent } from './code/code-container/code-container.component';
import { CodeDetailComponent } from './code/code-detail/code-detail/code-detail.component';

const ENTITY_STATES = [...interviewrunRoute];

@NgModule({
    imports: [
        SharedModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    exports: [],
    declarations: [
        InterviewrunComponent,
        InterviewrunQuestionComponent,
        InterviewrunFinishComponent,
        CandidateInformationComponent,
        InverviewFeedbackComponent,
        QuestionsContainerComponent,
        CustomQuestionComponent,
        SoftSkillFeedbackComponent,
        CodeContainerComponent,
        CodeDetailComponent
    ],
    entryComponents: [InterviewrunComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewrunModule {}
