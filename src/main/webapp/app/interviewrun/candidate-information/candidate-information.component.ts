import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISkill } from 'app/shared/model/skill.model';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { InterviewService } from 'app/entities/interview';
import { InterviewrunService } from '../interviewrun.service';
import { SkillService } from 'app/entities/skill';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from 'app/core';
import { filter, map } from 'rxjs/operators';

@Component({
    selector: 'wissen-candidate-information',
    templateUrl: './candidate-information.component.html',
    styleUrls: ['./candidate-information.component.scss']
})
export class CandidateInformationComponent implements OnInit {
    @Output() saveCandidateSuccess = new EventEmitter();
    @Output() getInterviewId = new EventEmitter();
    interview: IInterview;
    currentAccount: any;

    isDisabled: boolean;
    isSaving: boolean;

    skills: ISkill[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected interviewService: InterviewService,
        protected interviewrunService: InterviewrunService,
        protected skillService: SkillService,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService,
        protected router: Router
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ interview }) => {
            this.interview = interview;
            console.log('interview: ' + JSON.stringify(interview));
        });
        console.log('interviewid' + this.interview.id);

        this.interviewrunService.getInterviewerSkills().subscribe(res => {
            console.log(res);
            this.skills = res.body;
            console.log(this.skills);
        });
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        if (this.interview.id !== undefined) {
            this.subscribeToSaveResponse(this.interviewService.update(this.interview));
            this.interviewrunService.interview = this.interview;
            this.interviewrunService.getInterview$.next(this.interview);
            // this.saveCandidateSuccess.emit();
            this.getInterviewId.emit(this.interview);
        } else {
            console.log('candidateInformation :: save');

            this.isSaving = true;

            if (this.interview.id !== undefined) {
                this.subscribeToSaveResponse(this.interviewService.update(this.interview));
            } else {
                console.log('Starting an interview');
                if (this.interview.interviewer === undefined) {
                    this.interview.interviewer = this.currentAccount.login;
                }
                if (this.interview.createdAt === undefined) {
                    this.interview.createdAt = moment();
                }
                if (this.interview.startedAt === undefined) {
                    this.interview.startedAt = moment();
                }
                this.subscribeToSaveResponse(this.interviewService.create(this.interview));
            }
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IInterview>>) {
        result.subscribe((res: HttpResponse<IInterview>) => this.onSaveSuccess(res.body), () => this.onSaveError());
    }

    protected onSaveSuccess(saved: IInterview) {
        this.isSaving = false;
        this.interview = saved;
        this.interviewrunService.interview = saved;
        this.interviewrunService.getInterview$.next(saved);
        this.saveCandidateSuccess.emit();
        this.getInterviewId.emit(saved);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSkillById(index: number, item: ISkill) {
        return item.id;
    }
}
