import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { ISoftSkillsFeedback, SoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';
import { SoftSkillsFeedbackService } from 'app/entities/soft-skills-feedback/soft-skills-feedback.service';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview';
import { InterviewrunService } from '../interviewrun.service';

@Component({
    selector: 'wissen-soft-skill-feedback',
    templateUrl: './soft-skill-feedback.component.html',
    styleUrls: ['./soft-skill-feedback.component.scss']
})
export class SoftSkillFeedbackComponent implements OnInit {
    softSkillsFeedback: ISoftSkillsFeedback;
    isSaving: boolean;
    isSaved: boolean;
    @Input() interview: IInterview;
    @Input() isPending: boolean;

    @Input() softSkill: ISoftSkillsFeedback;
    @Output() nextStep = new EventEmitter();

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected softSkillsFeedbackService: SoftSkillsFeedbackService,
        protected interviewService: InterviewService,
        protected activatedRoute: ActivatedRoute,
        protected interviewrunService: InterviewrunService
    ) {}

    ngOnInit() {
        this.isSaved = false;
        this.isSaving = false;
        this.softSkillsFeedback = new SoftSkillsFeedback();
        console.log('pendui:' + this.isPending);
        if (this.isPending === true) {
            this.interviewrunService.getInterview$.subscribe(interview => {
                this.interviewrunService.getSoftSkillsFeedback(interview.id).subscribe(res => {
                    console.log('softskillfeedu' + res);
                    this.softSkill = res.body;
                    this.softSkill;
                    console.log(this.softSkill);
                    if (this.softSkill !== null) {
                        this.softSkillsFeedback = this.softSkill;
                        this.softSkillsFeedback.id = undefined;
                    }
                });
            });
        }
    }

    previousState() {
        window.history.back();
    }

    save() {
        // this.isSaving = true;

        this.softSkillsFeedback.interview = this.interview;
        localStorage.setItem('communication', this.softSkillsFeedback.communication);
        localStorage.setItem('motivation', this.softSkillsFeedback.motivationLevel);

        // this.nextStep.emit();
        if (this.isSaved === true) {
            this.subscribeToSaveResponse1(this.softSkillsFeedbackService.create(this.softSkillsFeedback));
        } else {
            this.isSaved = true;
            this.subscribeToSaveResponse1(this.softSkillsFeedbackService.create(this.softSkillsFeedback));
        }
    }

    protected subscribeToSaveResponse1(result: Observable<HttpResponse<ISoftSkillsFeedback>>) {
        result.subscribe((res: HttpResponse<ISoftSkillsFeedback>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.nextStep.emit();

        // this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInterviewById(index: number, item: IInterview) {
        return item.id;
    }
}
