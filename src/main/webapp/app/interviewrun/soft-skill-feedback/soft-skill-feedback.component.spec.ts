import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftSkillFeedbackComponent } from './soft-skill-feedback.component';

describe('SoftSkillFeedbackComponent', () => {
    let component: SoftSkillFeedbackComponent;
    let fixture: ComponentFixture<SoftSkillFeedbackComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SoftSkillFeedbackComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SoftSkillFeedbackComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
