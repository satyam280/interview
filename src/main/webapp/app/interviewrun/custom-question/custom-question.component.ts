import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InterviewrunService } from '../interviewrun.service';
import { CustomQuestionReq } from 'app/shared/model/custom-question-request.model';

@Component({
    selector: 'wissen-custom-question',
    templateUrl: './custom-question.component.html',
    styleUrls: ['./custom-question.component.scss']
})
export class CustomQuestionComponent implements OnInit {
    question;
    feedback;
    answerQuality: string;
    @Input() interview;
    @Output() nextQuestion = new EventEmitter();
    buttonDisabled: boolean = true;
    constructor(private interviewrunService: InterviewrunService) {}

    ngOnInit() {}

    getSelectedValue(val) {
        this.buttonDisabled = false;
        this.answerQuality = val;
    }

    cancel() {
        this.nextQuestion.emit();
    }

    save() {
        const customQuestionReq: CustomQuestionReq = {
            answerQuality: this.answerQuality,
            answerText: this.feedback,
            finishedAt: null,
            id: null,
            interview: this.interview,
            question_text: this.question,
            startedAt: null
        };
        this.interviewrunService.submitCustomQuestion(customQuestionReq).subscribe(res => {
            this.nextQuestion.emit(res);
        });
    }
}
