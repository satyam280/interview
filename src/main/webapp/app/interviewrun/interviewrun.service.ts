import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInterview, Interview } from 'app/shared/model/interview.model';
import { IInterviewQuestions, IQFeedback, IACFeedback } from 'app/shared/model/interviewrun.model';
import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';
import { QuestionSubmitRequest } from 'app/shared/model/question-submit-request.model';
import { QuestionSubmitResponse } from 'app/shared/model/question-submit-response.model';
import { CustomQuestionReq } from 'app/shared/model/custom-question-request.model';
import { IAnswerCheckFeedback, AnswerCheckStatus } from 'app/shared/model/answer-check-feedback.model';
import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';
import { ISkill } from 'app/shared/model/skill.model';

type EntityResponseType = HttpResponse<IInterviewQuestions>;
type Response1 = HttpResponse<IAnswerCheckFeedback[]>;
type Response2 = HttpResponse<IQuestionFeedback[]>;
type Response4 = HttpResponse<IInterview[]>;
type Response3 = HttpResponse<ISoftSkillsFeedback>;
type Response5 = HttpResponse<ISkill[]>;

@Injectable({ providedIn: 'root' })
export class InterviewrunService {
    public resourceUrl = SERVER_API_URL + 'api/interviewquestions';
    public answerCheckFeedbackUrl = SERVER_API_URL + 'api/answer-check-feedbacks-id';
    public questionFeedbackUrl = SERVER_API_URL + 'api/question-feedbacks-id';
    private submitQuestionUrl = SERVER_API_URL + 'api/question-full-feedback';
    private feedbackSumitUrl = SERVER_API_URL + 'api/interviews';
    private customQuestionUrl = SERVER_API_URL + '/api/custom-questions';
    private interviewerSkillUrl = SERVER_API_URL + 'api/interviewerskills';

    public interview: IInterview;
    public getInterview$: Subject<any> = new Subject<any>();
    public getSoftSkill = SERVER_API_URL + 'api/soft-skills-feedbacks-interview-id';
    public pendingInterviewUrl = SERVER_API_URL + 'api/interviews-by-interviewer';
    constructor(protected http: HttpClient) {}

    getInterviewQuestions(id: number): Observable<EntityResponseType> {
        return this.http.get<IInterviewQuestions>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getPendingInterviews(interviewer: string): Observable<Response4> {
        return this.http.get<IInterview[]>(`${this.pendingInterviewUrl}/${interviewer}`, { observe: 'response' });
    }

    getAnswerCheckFeedbacks(id: number): Observable<Response1> {
        return this.http.get<IAnswerCheckFeedback[]>(`${this.answerCheckFeedbackUrl}/${id}`, { observe: 'response' });
    }

    getQuestionFeedbacks(id: number): Observable<Response2> {
        return this.http.get<IQuestionFeedback[]>(`${this.questionFeedbackUrl}/${id}`, { observe: 'response' });
    }

    getSoftSkillsFeedback(id: number): Observable<Response3> {
        return this.http.get<ISoftSkillsFeedback>(`${this.getSoftSkill}/${id}`, { observe: 'response' });
    }

    submitQuestionFeedback(req: QuestionSubmitRequest): Observable<QuestionSubmitResponse> {
        return this.http.post(`${this.submitQuestionUrl}`, req) as any;
    }

    submitInterviewFeedback(req: Interview): Observable<any> {
        return this.http.put(`${this.feedbackSumitUrl}`, req) as any;
    }

    submitCustomQuestion(req: CustomQuestionReq): Observable<any> {
        return this.http.post(`${this.customQuestionUrl}`, req) as any;
    }

    getInterviewerSkills(): Observable<Response5> {
        return this.http.get<ISkill[]>(`${this.interviewerSkillUrl}`, { observe: 'response' });
    }
}
