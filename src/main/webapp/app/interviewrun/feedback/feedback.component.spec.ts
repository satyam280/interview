import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InverviewFeedbackComponent } from './feedback.component';

describe('InverviewFeedbackComponent', () => {
    let component: InverviewFeedbackComponent;
    let fixture: ComponentFixture<InverviewFeedbackComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [InverviewFeedbackComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(InverviewFeedbackComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
