import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InterviewrunService } from '../interviewrun.service';
import { SoftSkillsFeedbackService } from 'app/entities/soft-skills-feedback/soft-skills-feedback.service';
import { ISoftSkillsFeedback, SoftSkillsFeedback, MotivationLevel, CommunicationSkills } from 'app/shared/model/soft-skills-feedback.model';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IInterview } from 'app/shared/model/interview.model';

@Component({
    selector: 'wissen-inverview-feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.scss']
})
export class InverviewFeedbackComponent implements OnInit {
    @Input() interview: IInterview;
    @Output() nextStep = new EventEmitter();
    feedback: string;
    selected: any;
    softSkillsFeedback: ISoftSkillsFeedback;
    buttonDisabled: boolean = true;
    constructor(private interviewRunService: InterviewrunService, private softSkillsFeedbackService: SoftSkillsFeedbackService) {}

    ngOnInit() {
        this.softSkillsFeedback = new SoftSkillsFeedback();
    }

    setSelection(result: string) {
        this.selected = result;
        this.buttonDisabled = false;
    }

    save() {
        if (localStorage.getItem('communication') === 'POOR') {
            this.softSkillsFeedback.communication = CommunicationSkills.POOR;
        }
        if (localStorage.getItem('communication') === 'SATISFACTORY') {
            this.softSkillsFeedback.communication = CommunicationSkills.SATISFACTORY;
        }
        if (localStorage.getItem('communication') === 'GOOD') {
            this.softSkillsFeedback.communication = CommunicationSkills.GOOD;
        }
        if (localStorage.getItem('motivation') === 'LOW') {
            this.softSkillsFeedback.motivationLevel = MotivationLevel.LOW;
        }
        if (localStorage.getItem('motivation') === 'AVERAGE') {
            this.softSkillsFeedback.motivationLevel = MotivationLevel.AVERAGE;
        }
        if (localStorage.getItem('motivation') === 'HIGH') {
            this.softSkillsFeedback.motivationLevel = MotivationLevel.HIGH;
        }
        this.softSkillsFeedback.interview = this.interview;
        // this.subscribeToSaveResponse(this.softSkillsFeedbackService.create(this.softSkillsFeedback));
        const request = this.interview;
        request.feedback = this.feedback;
        request.result = this.selected;
        this.interviewRunService.submitInterviewFeedback(request).subscribe(response => {
            this.nextStep.emit();
            console.log(response);
        });
    }

    // protected subscribeToSaveResponse(result: Observable<HttpResponse<ISoftSkillsFeedback>>) {
    //     result.subscribe((res: HttpResponse<ISoftSkillsFeedback>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    // }
    // protected onSaveSuccess() {
    //     // this.nextStep.emit();
    //     // this.previousState();

    // }
    // protected onSaveError() {}
}
