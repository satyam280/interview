import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IInterview, Interview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview/interview.service';
import { ISkill } from 'app/shared/model/skill.model';
import { SkillService } from 'app/entities/skill';
import { AccountService } from 'app/core';
import {} from '@angular/router';
import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';
import { AnswerCheckFeedback, AnswerCheckStatus } from 'app/shared/model/answer-check-feedback.model';
import { InterviewrunService } from '../interviewrun.service';
import { IInterviewQuestions } from 'app/shared/model/interviewrun.model';
import { QuestionSubmitRequest } from 'app/shared/model/question-submit-request.model';
import { AnswerQualityEnum } from 'app/shared/model/answer-quality-options.enum';
import { all } from 'q';

@Component({
    selector: 'wissen-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss']
})
export class InterviewrunQuestionComponent implements OnInit {
    @Input() interview: IInterview | Interview;
    @Input() question;
    @Input() feedback;
    @Input() index;
    @Input() allQuestions;
    @Output() nextQuestion = new EventEmitter();
    @Output() previousQuestion = new EventEmitter();
    @Output() changeQuestionView = new EventEmitter();
    @Output() content = new EventEmitter();

    @ViewChild('ansCheck') ansCheck;
    buttonDisabled: boolean = true;

    ac: number[];
    acf: number[];
    questionFeedback: IQuestionFeedback;
    currentAccount: any;
    activeQuestionIndex: number;
    skills: ISkill[];
    questions: IInterviewQuestions | any;
    current: number;
    private _selectedAnswerQuality: string = null;
    selectedAnswerChecks: Array<any>;
    formData = new FData([], '', '');
    setFormData(i: number) {
        if (this.allQuestions.length !== 0) {
            console.log('setFormData:' + this.allQuestions[i].answerChecks);
            this.feedback = this.allQuestions[i].feedback;
            this._selectedAnswerQuality = this.allQuestions[i].quality;
            this.ac = this.allQuestions[i].answerChecks;
            this.cdr.detectChanges();
        }
    }

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected interviewService: InterviewService,
        protected interviewrunService: InterviewrunService,
        protected skillService: SkillService,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService,
        protected router: Router,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        console.log('d2ehiuw');
        this.setFormData(this.index);
        console.log(this.index);
    }

    customQuestion() {
        this.buttonDisabled = true;
        this.changeQuestionView.emit('customQuestion');
    }

    skipQuestion() {
        this.buttonDisabled = true;
        console.log(this.buttonDisabled);
        if (this.index != this.allQuestions.length - 1) {
            this.setFormData(this.index + 1);
        }
        this.nextQuestion.emit();
    }

    getSelectedValue(val) {
        this.buttonDisabled = false;
        this._selectedAnswerQuality = val;
    }
    setSelectedAnswerQuality() {
        this._selectedAnswerQuality = null;
        this.buttonDisabled = true;
    }
    getSelectedAnswerChecks() {
        this.acf = [];
        const selectedAnswerChecks = this.selectedAnswerChecks;
        const allAnswerChecks = this.question.answerChecks;
        const answerCheckFeedbackArray: AnswerCheckFeedback[] = new Array();
        if (allAnswerChecks.length === 0) allAnswerChecks[0] = -100;
        for (var i = 0; i < allAnswerChecks.length; i++) {
            let answer_quality = AnswerCheckStatus.FAIL;
            if (selectedAnswerChecks !== undefined) {
                if (selectedAnswerChecks.includes(allAnswerChecks[i].id)) {
                    answer_quality = AnswerCheckStatus.PASS;
                    if (!this.acf.includes(allAnswerChecks[i].id)) {
                        this.acf.push(allAnswerChecks[i].id);
                    }
                }
            }
            answerCheckFeedbackArray[i] = new AnswerCheckFeedback(null, answer_quality, this.interview, allAnswerChecks[i]);
        }
        console.log(answerCheckFeedbackArray);
        this.ac = this.acf;

        return answerCheckFeedbackArray as any;
    }

    save() {
        this.buttonDisabled = true;

        const req: QuestionSubmitRequest = {
            answerCheckFeedbacks: this.getSelectedAnswerChecks(),
            questionFeedback: {
                answer_quality: this._selectedAnswerQuality,
                description: this.feedback,
                duration: null,
                finishedAt: null,
                id: this.interview.id,
                interview: this.interview as any,
                question: this.question.question,
                startedAt: null
            }
        };
        this.formData.answerChecks = this.ac;
        this.formData.feedback = this.feedback;
        this.formData.quality = this._selectedAnswerQuality;
        // console.log("req:"+QuestionSubmitRequest);
        this.content.emit(this.formData);
        // this.feedback = '';
        this.interviewrunService.submitQuestionFeedback(req).subscribe(response => {
            this.nextQuestion.emit();
        });
        // this.formData={
        //     "answerChecks":[],
        //     "feedback":"",
        //     "quality":""
        // };
        console.log('save' + this.index);
        console.log(this.allQuestions);
        // console.log(this.allQuestions[1]);
        if (this.index != this.allQuestions.length - 1) {
            this.setFormData(this.index + 1);
        }
        console.log('wuhu' + this.formData.answerChecks);
        this.setSelectedAnswerQuality();
    }
    previous() {
        // this.feedback = '';
        this.setFormData(this.index - 1);
        console.log('prev' + this.index);
        console.log(this.allQuestions);
        // console.log(this.allQuestions[1].feedback);
        this.buttonDisabled = true;
        this.previousQuestion.emit();
    }
}

interface IFData {
    answerChecks: any[];
    feedback: string;
    quality: string;
}
class FData implements IFData {
    constructor(public answerChecks: any[], public feedback: string, public quality: string) {}
}
