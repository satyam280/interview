import { Component, OnInit } from '@angular/core';

import { LoginService, AccountService, Account } from 'app/core';
import { Router } from '@angular/router';
import { IInterview, Interview } from 'app/shared/model/interview.model';
import { InterviewrunService } from '../interviewrun/interviewrun.service';
import * as moment from 'moment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { SERVER_API_URL } from 'app/app.constants';

type Response4 = HttpResponse<IInterview[]>;

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    account: Account;
    pendingInterviews: IInterview[] | any;
    isPendingInTimeLimit: boolean = false;
    pendingId: number;
    public pendingInterviewUrl = SERVER_API_URL + 'api/interviews-by-interviewer';

    constructor(
        private accountService: AccountService,
        protected http: HttpClient,
        private interviewRunService: InterviewrunService,
        private loginService: LoginService,
        private router: Router
    ) {}

    ngOnInit() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
            console.log(this.account);
            if (this.isAuthenticated()) {
                this.pendingInterviews = this.interviewRunService.getPendingInterviews(this.account.email).subscribe(res => {
                    console.log(res.body);
                    this.pendingInterviews = res.body;
                    var t = 0;
                    var maxId = 0;
                    console.log(this.pendingInterviews);
                    if (this.pendingInterviews.length > 0) {
                        while (t < this.pendingInterviews.length) {
                            if (moment().diff(this.pendingInterviews[t].createdAt, 'hours', true) < 24) {
                                console.log('yepp');
                                this.isPendingInTimeLimit = true;
                                if (this.pendingInterviews[t].id > maxId) {
                                    maxId = this.pendingInterviews[t].id;
                                }
                            }
                            t++;
                        }
                        if (this.isPendingInTimeLimit === true) {
                            this.pendingId = maxId;
                        }
                    }
                });
            }
        });

        //    this.pendingInterviews.filter((interview)=>{
        //    this.isPendingInTimeLimit= moment().diff(interview.createdAt,'hours',true)<24;
        //    console.log(this.isPendingInTimeLimit);
        //     });
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    getPendingInterviews(interviewer: string): Observable<Response4> {
        return this.http.get<IInterview[]>(`${this.pendingInterviewUrl}/${interviewer}`, { observe: 'response' });
    }

    login() {
        this.loginService.login();
    }

    resume(interviewid: number) {
        this.router.navigate(['startinterview', { id: interviewid }]);
    }
}
