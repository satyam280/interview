import { ISkill } from 'app/shared/model/skill.model';

export interface ISkillUser {
    id?: number;
    userEmailId?: string;
    skill?: ISkill;
}

export class SkillUser implements ISkillUser {
    constructor(public id?: number, public userEmailId?: string, public skill?: ISkill) {}
}
