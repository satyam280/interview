export interface IEvaluationArea {
    id?: number;
    name?: string;
}

export class EvaluationArea implements IEvaluationArea {
    constructor(public id?: number, public name?: string) {}
}
