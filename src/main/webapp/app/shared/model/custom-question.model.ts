import { Moment } from 'moment';
import { IInterview } from 'app/shared/model/interview.model';

export const enum AnswerQuality {
    EXCELLENT = 'EXCELLENT',
    SATISFACTORY = 'SATISFACTORY',
    UNSATISFACTORY = 'UNSATISFACTORY',
    POOR = 'POOR'
}

export interface ICustomQuestion {
    id?: number;
    question_text?: string;
    answerQuality?: AnswerQuality;
    answerText?: string;
    startedAt?: Moment;
    finishedAt?: Moment;
    interview?: IInterview;
}

export class CustomQuestion implements ICustomQuestion {
    constructor(
        public id?: number,
        public question_text?: string,
        public answerQuality?: AnswerQuality,
        public answerText?: string,
        public startedAt?: Moment,
        public finishedAt?: Moment,
        public interview?: IInterview
    ) {}
}
