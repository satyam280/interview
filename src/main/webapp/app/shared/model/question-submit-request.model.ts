export interface QuestionSubmitRequest {
    answerCheckFeedbacks: [
        {
            answerCheck: {
                id: number;
                name: string;
                question: Question;
            };
            answer_quality: string;
            id: number;
            interview: Interview;
        }
    ];
    questionFeedback: {
        answer_quality: string;
        description: string;
        duration: number;
        finishedAt: string;
        id: number;
        interview: Interview;
        question: Question;
        startedAt: string;
    };
}

interface Interview {
    candidate: string;
    createdAt: string;
    experience: number;
    feedback: string;
    finishedAt: string;
    id: number;
    interviewer: string;
    result: string;
    skill: {
        id: number;
        name: string;
    };
    startedAt: string;
    status: string;
}

interface Question {
    description: string;
    expected_answers: string;
    id: 0;
    interview: Interview;
    min_experience: 0;
    skill: {
        id: 0;
        name: string;
    };
    topic: {
        id: 0;
        name: string;
    };
}
