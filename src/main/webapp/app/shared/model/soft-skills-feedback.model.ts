import { IInterview } from 'app/shared/model/interview.model';

export const enum CommunicationSkills {
    POOR = 'POOR',
    SATISFACTORY = 'SATISFACTORY',
    GOOD = 'GOOD'
}

export const enum MotivationLevel {
    LOW = 'LOW',
    AVERAGE = 'AVERAGE',
    HIGH = 'HIGH'
}

export interface ISoftSkillsFeedback {
    id?: number;
    communication?: CommunicationSkills;
    motivationLevel?: MotivationLevel;
    interview?: IInterview;
}

export class SoftSkillsFeedback implements ISoftSkillsFeedback {
    constructor(
        public id?: number,
        public communication?: CommunicationSkills,
        public motivationLevel?: MotivationLevel,
        public interview?: IInterview
    ) {}
}
