export interface InterviewQuestionsResponse {
    questions: Array<InterivewQuestions>;
}

interface InterivewQuestions {
    id: string | number;
    question: string;
    answerOptions: Array<InterviewQuestionsAnswerOptions>;
}

interface InterviewQuestionsAnswerOptions {
    id: string | number;
    label: string;
}
