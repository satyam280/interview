export interface CustomQuestionReq {
    answerQuality: string;
    answerText: string;
    finishedAt: string;
    id: number;
    interview: string;
    question_text: string;
    startedAt: string;
}

interface Interview {
    candidate: string;
    createdAt: string;
    experience: string;
    feedback: string;
    finishedAt: string;
    id: number;
    interviewer: string;
    result: string;
    skill: {
        id: 0;
        name: string;
    };
    startedAt: string;
    status: string;
}
