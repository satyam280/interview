export interface QuestionSubmitResponse {
    answer_quality: string;
    description: string;
    duration: number;
    finishedAt: string;
    id: string;
    interview: {
        candidate: string;
        createdAt: string;
        experience: number;
        feedback: string;
        finishedAt: string;
        id: number;
        interviewer: string;
        result: string;
        skill: {
            id: number;
            name: string;
        };
        startedAt: string;
        status: string;
    };
    question: {
        description: string;
        expected_answers: string;
        id: number;
        min_experience: number;
        skill: {
            id: number;
            name: string;
        };
        topic: {
            id: number;
            name: string;
        };
    };
    startedAt: string;
}
