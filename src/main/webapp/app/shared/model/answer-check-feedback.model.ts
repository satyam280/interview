import { IInterview } from 'app/shared/model/interview.model';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';

export const enum AnswerCheckStatus {
    PASS = 'PASS',
    FAIL = 'FAIL',
    NOTSURE = 'NOTSURE'
}

export interface IAnswerCheckFeedback {
    id?: number;
    answer_quality?: AnswerCheckStatus;
    interview?: IInterview;
    answerCheck?: IAnswerCheck;
}

export class AnswerCheckFeedback implements IAnswerCheckFeedback {
    constructor(
        public id?: number,
        public answer_quality?: AnswerCheckStatus,
        public interview?: IInterview,
        public answerCheck?: IAnswerCheck
    ) {}
}
