import { Moment } from 'moment';
import { ISkill } from 'app/shared/model/skill.model';

export const enum InterviewStatus {
    CREATED = 'CREATED',
    STARTED = 'STARTED',
    FINISHED = 'FINISHED'
}

export const enum InterviewResult {
    MUST_HIRE = 'MUST_HIRE',
    HIRE = 'HIRE',
    KNOWLEDGR_GAP = 'KNOWLEDGE_GAP',
    REJECT = 'REJECT'
}

export interface IInterview {
    id?: number;
    interviewer?: string;
    candidate?: string;
    createdAt?: Moment;
    startedAt?: Moment;
    finishedAt?: Moment;
    status?: InterviewStatus;
    result?: InterviewResult;
    feedback?: string;
    experience?: number;
    skill?: ISkill;
}

export class Interview implements IInterview {
    constructor(
        public id?: number,
        public interviewer?: string,
        public candidate?: string,
        public createdAt?: Moment,
        public startedAt?: Moment,
        public finishedAt?: Moment,
        public status?: InterviewStatus,
        public result?: InterviewResult,
        public feedback?: string,
        public experience?: number,
        public skill?: ISkill
    ) {}
}
