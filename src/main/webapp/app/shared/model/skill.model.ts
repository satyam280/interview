export interface ISkill {
    id?: number;
    name?: string;
}

export class Skill implements ISkill {
    constructor(public id?: number, public name?: string) {}
}
