export enum AnswerQualityEnum {
    EXCELLENT = 'Excellent',
    SATISFACTORY = 'satisfactory',
    UNSATISFACTORY = 'Unsatisfactory',
    POOR = 'Poor'
}
