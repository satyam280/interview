import { IAnswerCheck } from 'app/shared/model/answer-check.model';
import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';

export interface IAnswerCheckEvalAreaMap {
    id?: number;
    answerCheck?: IAnswerCheck;
    evaluationArea?: IEvaluationArea;
}

export class AnswerCheckEvalAreaMap implements IAnswerCheckEvalAreaMap {
    constructor(public id?: number, public answerCheck?: IAnswerCheck, public evaluationArea?: IEvaluationArea) {}
}
