import { Moment } from 'moment';
import { IInterview } from 'app/shared/model/interview.model';
import { IQuestion } from 'app/shared/model/question.model';

export const enum AnswerQuality {
    EXCELLENT = 'EXCELLENT',
    SATISFACTORY = 'SATISFACTORY',
    UNSATISFACTORY = 'UNSATISFACTORY',
    POOR = 'POOR'
}

export interface IQuestionFeedback {
    id?: number;
    answer_quality?: AnswerQuality;
    startedAt?: Moment;
    finishedAt?: Moment;
    duration?: number;
    description?: string;
    interview?: IInterview;
    question?: IQuestion;
}

export class QuestionFeedback implements IQuestionFeedback {
    constructor(
        public id?: number,
        public answer_quality?: AnswerQuality,
        public startedAt?: Moment,
        public finishedAt?: Moment,
        public duration?: number,
        public description?: string,
        public interview?: IInterview,
        public question?: IQuestion
    ) {}
}
