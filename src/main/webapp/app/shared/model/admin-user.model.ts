export interface IAdminUser {
    id?: number;
    emailId?: string;
}

export class AdminUser implements IAdminUser {
    constructor(public id?: number, public emailId?: string) {}
}
