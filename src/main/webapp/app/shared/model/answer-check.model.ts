import { IQuestion } from 'app/shared/model/question.model';

export interface IAnswerCheck {
    id?: number;
    name?: string;
    question?: IQuestion;
}

export class AnswerCheck implements IAnswerCheck {
    constructor(public id?: number, public name?: string, public question?: IQuestion) {}
}
