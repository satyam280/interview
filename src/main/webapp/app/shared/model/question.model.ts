import { ISkill } from 'app/shared/model/skill.model';
import { ITopic } from 'app/shared/model/topic.model';

export interface IQuestion {
    id?: number;
    min_experience?: number;
    description?: string;
    expected_answers?: string;
    sequence?: number;
    isActive?: boolean;
    skill?: ISkill;
    topic?: ITopic;
}

export class Question implements IQuestion {
    constructor(
        public id?: number,
        public min_experience?: number,
        public description?: string,
        public expected_answers?: string,
        public sequence?: number,
        public isActive?: boolean,
        public skill?: ISkill,
        public topic?: ITopic
    ) {
        this.isActive = this.isActive || false;
    }
}
