import { IInterview } from './interview.model';
import { IQuestion } from './question.model';
import { IAnswerCheck } from './answer-check.model';
import { IQuestionFeedback } from './question-feedback.model';
import { IAnswerCheckFeedback } from './answer-check-feedback.model';

export interface IQuestionDetails {
    question?: IQuestion;
    answerChecks?: IAnswerCheck[];
}

export class QuestionDetails implements IQuestionDetails {
    constructor(public question?: IQuestion, public answerChecks?: IAnswerCheck[]) {}
}

export interface IInterviewQuestions {
    interview?: IInterview;
    questions?: IQuestionDetails[];
}

export class InterviewQuestions implements IInterviewQuestions {
    constructor(public interview?: IInterview, public questions?: IQuestionDetails[]) {}
}

export interface IQFeedback {
    questionFeedback: IQuestionFeedback[];
}
export class QFeedback implements IQFeedback {
    constructor(public questionFeedback: IQuestionFeedback[]) {}
}
export interface IACFeedback {
    aCFeedback: IAnswerCheckFeedback[];
}
export class ACFeedback {
    constructor(aCFeedback: IAnswerCheckFeedback[]) {}
}
