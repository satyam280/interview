import { InterviewQuestionsResponse } from '../model/interview-questions-response.model';

export const getInterviewQuestions: InterviewQuestionsResponse = {
    questions: [
        {
            id: 1,
            question: 'What is the latest verison of java?',
            answerOptions: [
                {
                    id: 1,
                    label: '9'
                },
                {
                    id: 2,
                    label: '9'
                },
                {
                    id: 3,
                    label: '9'
                },
                {
                    id: 4,
                    label: '9'
                }
            ]
        },
        {
            id: 1,
            question: 'What is the latest verison of java?',
            answerOptions: [
                {
                    id: 1,
                    label: '9'
                },
                {
                    id: 2,
                    label: '9'
                },
                {
                    id: 3,
                    label: '9'
                },
                {
                    id: 4,
                    label: '9'
                }
            ]
        },
        {
            id: 1,
            question: 'What is the latest verison of java?',
            answerOptions: [
                {
                    id: 1,
                    label: '9'
                },
                {
                    id: 2,
                    label: '9'
                },
                {
                    id: 3,
                    label: '9'
                },
                {
                    id: 4,
                    label: '9'
                }
            ]
        },
        {
            id: 1,
            question: 'What is the latest verison of java?',
            answerOptions: [
                {
                    id: 1,
                    label: '9'
                },
                {
                    id: 2,
                    label: '9'
                },
                {
                    id: 3,
                    label: '9'
                },
                {
                    id: 4,
                    label: '9'
                }
            ]
        },
        {
            id: 1,
            question: 'What is the latest verison of java?',
            answerOptions: [
                {
                    id: 1,
                    label: '9'
                },
                {
                    id: 2,
                    label: '9'
                },
                {
                    id: 3,
                    label: '9'
                },
                {
                    id: 4,
                    label: '9'
                }
            ]
        },
        {
            id: 1,
            question: 'What is the latest verison of java?',
            answerOptions: [
                {
                    id: 1,
                    label: '9'
                },
                {
                    id: 2,
                    label: '9'
                },
                {
                    id: 3,
                    label: '9'
                },
                {
                    id: 4,
                    label: '9'
                }
            ]
        }
    ]
};
