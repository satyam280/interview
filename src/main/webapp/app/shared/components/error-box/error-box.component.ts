import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'wissen-error-box',
    templateUrl: './error-box.component.html',
    styleUrls: ['./error-box.component.scss']
})
export class ErrorBoxComponent {
    message: string;
    constructor(public dialogRef: MatDialogRef<ErrorBoxComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.message = this.data.message || 'An error occured.';
    }

    closeModal(): void {
        this.dialogRef.close();
    }
}
