import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerQualityComponent } from './answer-quality.component';

describe('AnswerQualityComponent', () => {
    let component: AnswerQualityComponent;
    let fixture: ComponentFixture<AnswerQualityComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AnswerQualityComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AnswerQualityComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
