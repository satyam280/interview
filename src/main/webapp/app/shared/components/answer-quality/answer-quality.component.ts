import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AnswerQualityEnum } from 'app/shared/model/answer-quality-options.enum';

@Component({
    selector: 'wissen-answer-quality',
    templateUrl: './answer-quality.component.html',
    styleUrls: ['./answer-quality.component.scss']
})
export class AnswerQualityComponent implements OnInit {
    @Output() selected = new EventEmitter();
    answerQualityOptions: Array<string> = [];

    constructor() {
        for (const aq in AnswerQualityEnum) {
            if (isNaN(Number(aq))) {
                this.answerQualityOptions.push(aq);
            }
        }
    }

    ngOnInit() {}

    setSelectedAnswerQuality(val) {
        this.selected.emit(val);
    }
}
