import { Component, OnInit, OnDestroy, Input, ContentChildren, QueryList, ViewChild, TemplateRef, AfterViewInit } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { StepperStepComponent } from '../stepper-step/stepper-step.component';
import { Subscription } from 'rxjs';
import { MatStepper } from '@angular/material/stepper';
import { InterviewService } from 'app/entities/interview';
import { InterviewrunService } from 'app/interviewrun/interviewrun.service';

@Component({
    selector: 'wissen-stepper',
    templateUrl: './stepper.component.html',
    styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, AfterViewInit, OnDestroy {
    public selectedIndex: number = 0;
    public isMobile: boolean;
    public template: TemplateRef<any>;
    @Input() isLinear: boolean = true;
    @Input() startAtIndex: number;
    @ContentChildren(StepperStepComponent) steps: QueryList<StepperStepComponent>;
    @ViewChild('horizontal') templHorizontal: TemplateRef<any>;
    @ViewChild('vertical') templVertical: TemplateRef<any>;
    @ViewChild('stepper') stepper: MatStepper;
    isVertical: boolean = false;
    private _bpSub: Subscription;

    constructor(private bpObserver: BreakpointObserver, private interview: InterviewrunService) {}

    ngOnInit() {
        this._bpSub = this.bpObserver.observe(['(max-width: 599px)']).subscribe((state: BreakpointState) => {
            if (!state) {
                return;
            }

            this.setMobileStepper(state.matches);
        });

        if (this.startAtIndex) {
            this.selectedIndex = this.startAtIndex;
        }
    }

    ngAfterViewInit() {
        console.log(this.stepper);
    }

    selectionChanged(event: any): void {
        this.selectedIndex = event.selectedIndex;
    }

    setMobileStepper(isMobile: boolean): void {
        this.isMobile = isMobile;
        if (isMobile) {
            this.template = this.templVertical;
            this.isVertical = true;
        } else {
            this.template = this.templHorizontal;
            this.isVertical = false;
        }
        setTimeout(() => {
            // need async call since the ViewChild isn't ready
            // until after this function runs, thus the setTimeout hack
            //    this.stepper.selectedIndex = 1;
        });
    }

    nextStep() {
        this.stepper.next();
    }

    previousStep() {
        this.stepper.previous();
    }

    reset(): void {
        this.stepper.reset();
    }

    ngOnDestroy(): void {
        this._bpSub.unsubscribe();
    }
}
