import { Component, OnInit, Input, ViewChild, Output, TemplateRef, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'wissen-stepper-step',
    templateUrl: './stepper-step.component.html',
    styleUrls: ['./stepper-step.component.scss']
})
export class StepperStepComponent {
    @Input() isOptional: boolean = false;
    @Input() label: string;
    @Input() form: FormGroup;
    @ViewChild('template') template: TemplateRef<any>;
    @Output() formSubmitted: EventEmitter<any> = new EventEmitter();

    constructor() {}

    submit(): void {
        this.formSubmitted.emit(this.form.value);
    }
}
