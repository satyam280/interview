import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ErrorBoxComponent } from '../components/error-box/error-box.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(public dialog: MatDialog) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(err => {
                if (err.status !== 200 && err.status !== 201 && !(err.url && err.url.includes('api/account'))) {
                    const dialogRef = this.dialog.open(ErrorBoxComponent, {
                        width: '250px',
                        data: { message: err.error.title || err.error.detail || err }
                    });

                    dialogRef.afterClosed().subscribe(result => {
                        // do something for interactive modal
                    });
                }

                const error = err.error.message || err.statusText;
                return throwError(error);
            })
        );
    }
}
