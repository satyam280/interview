import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { InterviewSharedLibsModule, InterviewSharedCommonModule, HasAnyAuthorityDirective } from './';
import { StepperStepComponent } from './components/stepper-step/stepper-step.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { MaterialModule } from 'app/interviewrun/material.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from './interceptors/error-modal.interceptor';
import { ErrorBoxComponent } from './components/error-box/error-box.component';
import { AnswerQualityComponent } from './components/answer-quality/answer-quality.component';

@NgModule({
    imports: [InterviewSharedLibsModule, InterviewSharedCommonModule, MaterialModule],
    declarations: [HasAnyAuthorityDirective, StepperStepComponent, StepperComponent, ErrorBoxComponent, AnswerQualityComponent],
    providers: [
        { provide: NgbDateAdapter, useClass: NgbDateMomentAdapter },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    entryComponents: [ErrorBoxComponent],
    exports: [
        InterviewSharedCommonModule,
        HasAnyAuthorityDirective,
        MaterialModule,
        AnswerQualityComponent,
        StepperStepComponent,
        StepperComponent,
        ErrorBoxComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
    static forRoot() {
        return {
            ngModule: SharedModule
        };
    }
}
