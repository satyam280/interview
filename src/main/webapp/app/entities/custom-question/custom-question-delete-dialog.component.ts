import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICustomQuestion } from 'app/shared/model/custom-question.model';
import { CustomQuestionService } from './custom-question.service';

@Component({
    selector: 'jhi-custom-question-delete-dialog',
    templateUrl: './custom-question-delete-dialog.component.html'
})
export class CustomQuestionDeleteDialogComponent {
    customQuestion: ICustomQuestion;

    constructor(
        protected customQuestionService: CustomQuestionService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.customQuestionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'customQuestionListModification',
                content: 'Deleted an customQuestion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-custom-question-delete-popup',
    template: ''
})
export class CustomQuestionDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ customQuestion }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CustomQuestionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.customQuestion = customQuestion;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/custom-question', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/custom-question', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
