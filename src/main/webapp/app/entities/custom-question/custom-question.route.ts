import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CustomQuestion } from 'app/shared/model/custom-question.model';
import { CustomQuestionService } from './custom-question.service';
import { CustomQuestionComponent } from './custom-question.component';
import { CustomQuestionDetailComponent } from './custom-question-detail.component';
import { CustomQuestionUpdateComponent } from './custom-question-update.component';
import { CustomQuestionDeletePopupComponent } from './custom-question-delete-dialog.component';
import { ICustomQuestion } from 'app/shared/model/custom-question.model';

@Injectable({ providedIn: 'root' })
export class CustomQuestionResolve implements Resolve<ICustomQuestion> {
    constructor(private service: CustomQuestionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICustomQuestion> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CustomQuestion>) => response.ok),
                map((customQuestion: HttpResponse<CustomQuestion>) => customQuestion.body)
            );
        }
        return of(new CustomQuestion());
    }
}

export const customQuestionRoute: Routes = [
    {
        path: '',
        component: CustomQuestionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.customQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CustomQuestionDetailComponent,
        resolve: {
            customQuestion: CustomQuestionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.customQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CustomQuestionUpdateComponent,
        resolve: {
            customQuestion: CustomQuestionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.customQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CustomQuestionUpdateComponent,
        resolve: {
            customQuestion: CustomQuestionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.customQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const customQuestionPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CustomQuestionDeletePopupComponent,
        resolve: {
            customQuestion: CustomQuestionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.customQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
