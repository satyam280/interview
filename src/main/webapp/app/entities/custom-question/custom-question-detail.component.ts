import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICustomQuestion } from 'app/shared/model/custom-question.model';

@Component({
    selector: 'jhi-custom-question-detail',
    templateUrl: './custom-question-detail.component.html'
})
export class CustomQuestionDetailComponent implements OnInit {
    customQuestion: ICustomQuestion;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ customQuestion }) => {
            this.customQuestion = customQuestion;
        });
    }

    previousState() {
        window.history.back();
    }
}
