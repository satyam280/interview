import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ICustomQuestion } from 'app/shared/model/custom-question.model';
import { CustomQuestionService } from './custom-question.service';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview';

@Component({
    selector: 'jhi-custom-question-update',
    templateUrl: './custom-question-update.component.html'
})
export class CustomQuestionUpdateComponent implements OnInit {
    customQuestion: ICustomQuestion;
    isSaving: boolean;

    interviews: IInterview[];
    startedAt: string;
    finishedAt: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected customQuestionService: CustomQuestionService,
        protected interviewService: InterviewService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ customQuestion }) => {
            this.customQuestion = customQuestion;
            this.startedAt = this.customQuestion.startedAt != null ? this.customQuestion.startedAt.format(DATE_TIME_FORMAT) : null;
            this.finishedAt = this.customQuestion.finishedAt != null ? this.customQuestion.finishedAt.format(DATE_TIME_FORMAT) : null;
        });
        this.interviewService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IInterview[]>) => mayBeOk.ok),
                map((response: HttpResponse<IInterview[]>) => response.body)
            )
            .subscribe((res: IInterview[]) => (this.interviews = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.customQuestion.startedAt = this.startedAt != null ? moment(this.startedAt, DATE_TIME_FORMAT) : null;
        this.customQuestion.finishedAt = this.finishedAt != null ? moment(this.finishedAt, DATE_TIME_FORMAT) : null;
        if (this.customQuestion.id !== undefined) {
            this.subscribeToSaveResponse(this.customQuestionService.update(this.customQuestion));
        } else {
            this.subscribeToSaveResponse(this.customQuestionService.create(this.customQuestion));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomQuestion>>) {
        result.subscribe((res: HttpResponse<ICustomQuestion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInterviewById(index: number, item: IInterview) {
        return item.id;
    }
}
