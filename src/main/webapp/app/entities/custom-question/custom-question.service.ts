import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICustomQuestion } from 'app/shared/model/custom-question.model';

type EntityResponseType = HttpResponse<ICustomQuestion>;
type EntityArrayResponseType = HttpResponse<ICustomQuestion[]>;

@Injectable({ providedIn: 'root' })
export class CustomQuestionService {
    public resourceUrl = SERVER_API_URL + 'api/custom-questions';

    constructor(protected http: HttpClient) {}

    create(customQuestion: ICustomQuestion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(customQuestion);
        return this.http
            .post<ICustomQuestion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(customQuestion: ICustomQuestion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(customQuestion);
        return this.http
            .put<ICustomQuestion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ICustomQuestion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ICustomQuestion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(customQuestion: ICustomQuestion): ICustomQuestion {
        const copy: ICustomQuestion = Object.assign({}, customQuestion, {
            startedAt: customQuestion.startedAt != null && customQuestion.startedAt.isValid() ? customQuestion.startedAt.toJSON() : null,
            finishedAt: customQuestion.finishedAt != null && customQuestion.finishedAt.isValid() ? customQuestion.finishedAt.toJSON() : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.startedAt = res.body.startedAt != null ? moment(res.body.startedAt) : null;
            res.body.finishedAt = res.body.finishedAt != null ? moment(res.body.finishedAt) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((customQuestion: ICustomQuestion) => {
                customQuestion.startedAt = customQuestion.startedAt != null ? moment(customQuestion.startedAt) : null;
                customQuestion.finishedAt = customQuestion.finishedAt != null ? moment(customQuestion.finishedAt) : null;
            });
        }
        return res;
    }
}
