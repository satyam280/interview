import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ScreenComponent } from './screen/screen/screen.component';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ScreenDetailsComponent } from './screen/screen-details/screen-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { AutoCompleteModule } from '@syncfusion/ej2-angular-dropdowns';
import { CheckBoxModule  } from '@syncfusion/ej2-angular-buttons';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { UserRouteAccessService } from 'app/core';
import { DemoMaterialModule } from './screen/material-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseModule } from './screen/collapse';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
    imports: [BrowserModule,NgbModule,FontAwesomeModule,DragDropModule,MDBBootstrapModule.forRoot(),FormsModule,AutoCompleteModule,DropDownListModule,CheckBoxModule,
        DemoMaterialModule,BrowserAnimationsModule,CollapseModule,ClickOutsideModule,
        RouterModule.forChild([
            {
                path: 'skill',
                loadChildren: () => import('./skill/skill.module').then(m => m.InterviewSkillModule)
            },
            {
                path: 'topic',
                loadChildren: () => import('./topic/topic.module').then(m => m.InterviewTopicModule)
            },
            {
                path: 'question',
                loadChildren: () => import('./question/question.module').then(m => m.InterviewQuestionModule)
            },
            {
                path: 'answer-check',
                loadChildren: () => import('./answer-check/answer-check.module').then(m => m.InterviewAnswerCheckModule)
            },
            {
                path: 'question',
                loadChildren: () => import('./question/question.module').then(m => m.InterviewQuestionModule)
            },
            {
                path: 'question',
                loadChildren: () => import('./question/question.module').then(m => m.InterviewQuestionModule)
            },
            {
                path: 'question',
                loadChildren: () => import('./question/question.module').then(m => m.InterviewQuestionModule)
            },
            {
                path: 'question',
                loadChildren: () => import('./question/question.module').then(m => m.InterviewQuestionModule)
            },
            {
                path: 'question',
                loadChildren: () => import('./question/question.module').then(m => m.InterviewQuestionModule)
            },
            {
                path: 'interview',
                loadChildren: () => import('./interview/interview.module').then(m => m.InterviewInterviewModule)
            },
            {
                path: 'interview',
                loadChildren: () => import('./interview/interview.module').then(m => m.InterviewInterviewModule)
            },
            {
                path: 'question-feedback',
                loadChildren: () => import('./question-feedback/question-feedback.module').then(m => m.InterviewQuestionFeedbackModule)
            },
            {
                path: 'answer-check-feedback',
                loadChildren: () => import('./answer-check-feedback/answer-check-feedback.module').then(m => m.InterviewAnswerCheckFeedbackModule)
            },
            {
                path: 'question-feedback',
                loadChildren: () => import('./question-feedback/question-feedback.module').then(m => m.InterviewQuestionFeedbackModule)
            },
            {
                path: 'interview',
                loadChildren: () => import('./interview/interview.module').then(m => m.InterviewInterviewModule)
            },
            {
                path: 'custom-question',
                loadChildren: () => import('./custom-question/custom-question.module').then(m => m.InterviewCustomQuestionModule)
            },
            {
                path: 'evaluation-area',
                loadChildren: () => import('./evaluation-area/evaluation-area.module').then(m => m.InterviewEvaluationAreaModule)
            },
            {
                path: 'answer-check-eval-area-map',
                loadChildren: () => import('./answer-check-eval-area-map/answer-check-eval-area-map.module').then(m => m.InterviewAnswerCheckEvalAreaMapModule)
            },
            {
                path: 'soft-skills-feedback',
                loadChildren: () => import('./soft-skills-feedback/soft-skills-feedback.module').then(m => m.InterviewSoftSkillsFeedbackModule)
            },
            {
                path: 'admin-user',
                loadChildren: () => import('./admin-user/admin-user.module').then(m => m.InterviewAdminUserModule)
            },
            {
                path:'screen',
                component:ScreenComponent,
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'global.menu.entities.screen'
                },
                canActivate: [UserRouteAccessService]
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [ScreenComponent, ScreenDetailsComponent],
    entryComponents: [],
    providers: [Title],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewEntityModule {}
