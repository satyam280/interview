import { Component, OnInit, HostListener, ViewChild, ElementRef, Renderer2, ViewChildren } from '@angular/core';
import { ISkill } from 'app/shared/model/skill.model';
import { SkillService } from 'app/entities/skill';
import { JhiAlertService, JhiLanguageService } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { filter,map } from 'rxjs/operators';
import { JhiLanguageHelper } from 'app/core';
import {ModalDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'wissen-screen',
  templateUrl: './screen.component.html'
})
export class ScreenComponent implements OnInit {

  skillList:ISkill[]=[];
  iconList:string[]=[];
  currentSkill:ISkill;
  newSkill:ISkill={
    id:null,name:null
  };
  @ViewChild('sn') snbar:ElementRef;
  //@ViewChildren('ic') icl:Array<ElementRef>=[];
  @ViewChild(ModalDirective) mod:ModalDirective;
  pos:number;
  constructor(private skillService:SkillService,protected jhiAlertService: JhiAlertService,private rend:Renderer2
  ,private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) 
  { 
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
          this.languageService.changeLanguage(languageKey);
      }
  });
  }

  ngOnInit() {
    let contflui=document.getElementsByClassName("jh-card")[0];
    contflui.setAttribute("style","background-color:snow");
    let bdy=document.getElementById('wsc');
    console.log(bdy.offsetHeight+"\t"+bdy.offsetWidth);
    console.log(contflui);
    this.skillService
            .query()
            .pipe(
                filter((res: HttpResponse<ISkill[]>) => res.ok),
                map((res: HttpResponse<ISkill[]>) => res.body)
            )
            .subscribe(
                (res: ISkill[]) => {
                    this.skillList = res;
                    this.currentSkill=this.skillList[0];
                    this.pos=this.currentSkill.id;
                    for(let i in res){
                      let ini="";
                      if(res[i].name!=="C++"){
                        ini+="fa-";
                        ini+=(res[i].name.toLowerCase());
                      }
                      else{
                        ini+="fa-codiepie";
                      }
                      this.iconList.push(ini);
                    }
                    console.log(this.iconList);
                   
                    // for(let el of this.icl){
                    //   //this.rend.addClass(el.nativeElement,this.iconList[el.nativeElement.id])
                    //   console.log(el.nativeElement.id);
                    // }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    let elem1=document.getElementsByClassName('modal-backdrop')[0];
    console.log(elem1);
    if(elem1!==undefined){
    elem1.remove();
    }
  }
  show(){
    console.log(this.skillList);
  }
  change(sk:ISkill){
    //console.log(sk);
    this.currentSkill=sk;
    this.pos=this.currentSkill.id;
  }
  isActive(sk:ISkill){
    return (this.currentSkill.id===sk.id) &&(this.currentSkill.name===sk.name);
  }
  @HostListener('window:scroll', ['$evt']) 
        sc(ev) {
          //console.log("Scroll Event");
          this.pos=window.pageYOffset;
          if(this.pos>=40){
            this.rend.setStyle(this.snbar.nativeElement,'top','0px');
          }
          else
          {
            this.rend.setStyle(this.snbar.nativeElement,'top','60.5px');
          }
          //console.log(this.pos);
        }
  onClick(e:Event){
    console.log('Clicked:');//, e.srcElement);
    console.log((<Element>e.target));
    let catchM=(<Element>e.target);
    if(catchM.id=="CreateSkillModal")
    {
      if(catchM.classList.contains('shake')){
        catchM.classList.remove('shake');
        setTimeout(()=>{
          catchM.classList.add('shake');
        },100);
      }
      if(catchM.classList.contains('zoomIn')){
        catchM.classList.remove('zoomIn');
        catchM.classList.add('shake');
      }
      
    }
  }
  showCNSK(){
    let modEl=document.getElementById('CreateSkillModal');
    if(modEl.classList.contains('zoomOut')===true){
      modEl.classList.remove('zoomOut');
      modEl.classList.add('zoomIn');
    }
    this.mod.show();
    let elem1=document.getElementsByClassName('modal-backdrop')[0];
    console.log("Modal backdrop ");
    console.log(elem1);
    elem1.addEventListener("click",()=>{
      let modEl=document.getElementById('CreateSkillModal');
      if(modEl.classList.contains('shake')){
        modEl.classList.remove('shake');
        setTimeout(()=>{
          modEl.classList.add('shake');
        },100);
      }
      if(modEl.classList.contains('zoomIn')){
        modEl.classList.remove('zoomIn');
        modEl.classList.add('shake');
      }
      console.log("BackDrop click on Create New Skill");
    })
  }
  closeCNSK(){
    let modEl=document.getElementById('CreateSkillModal');
    modEl.classList.remove('zoomIn');
    modEl.classList.add('zoomOut');
    this.mod.hide();
  }
  saveNewSK(){
    //this.subscribeToSaveResponse(this.skillService.create(this.newSkill));
    console.log(this.newSkill);
  }
  trackSkillById(index: number, item: ISkill) {
    return item.id;
}
protected onError(errorMessage: string) {
  this.jhiAlertService.error(errorMessage, null, null);
}
}
