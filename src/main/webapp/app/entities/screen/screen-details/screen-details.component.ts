import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, HostListener, Renderer2, ViewChildren,QueryList, Inject } from '@angular/core';
import { QuestionService } from 'app/entities/question';
import { JhiAlertService } from 'ng-jhipster';
import { IQuestion } from 'app/shared/model/question.model';
import { ITEMS_PER_PAGE } from 'app/shared';
import { HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ISkill } from 'app/shared/model/skill.model';
import { CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckService } from 'app/entities/answer-check';
import {ModalDirective} from 'angular-bootstrap-md';
import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';
import { EvaluationAreaService } from 'app/entities/evaluation-area/evaluation-area.service';
import { filter, map } from 'rxjs/operators';
import { AutoCompleteComponent } from '@syncfusion/ej2-angular-dropdowns';
import { AnswerCheckEvalAreaMapService } from 'app/entities/answer-check-eval-area-map/answer-check-eval-area-map.service';
import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';
import { Observable } from 'rxjs';
import { SkillService } from 'app/entities/skill';
import { TopicService } from 'app/entities/topic';
import { ITopic } from 'app/shared/model/topic.model';
import { Router } from '@angular/router';

@Component({
  selector: 'wissen-screen-details',
  templateUrl: './screen-details.component.html',
  styleUrls: ['./screen-details.component.scss']
})
export class ScreenDetailsComponent implements OnInit,OnChanges {
  @Input() cs:ISkill;
  questions: IQuestion[];
  itemsPerPage: number;
  page: any;
  _opened: boolean = false;
  pos:number;
  c:number=0;
  allowed:boolean=true;
  already_opened:boolean[]=[];
  hide:boolean[]=[];

  isSaving:boolean;

  @ViewChildren(ModalDirective) modal:QueryList<ModalDirective>;
  @ViewChildren('sample') AutoCompleteObj: QueryList<AutoCompleteComponent>;
  //@ViewChildren('hd') hideSpans:QueryList<ElementRef>;
  editQuestion:IQuestion=
  { id:null,
    min_experience: null,
    description: null,
    expected_answers: null,
    sequence: null,
    isActive: null,
    skill:{
      id:null,name:null
    },
    topic:{
      id:null,name:null
    }
  };
  answerChecks:IAnswerCheck[];
  acArray:Array<IAnswerCheck[]>=[];

  evaluationAreas: IEvaluationArea[]=[];
  answerCheckEvalAreaMaps: IAnswerCheckEvalAreaMap[];
  partSkills:ISkill[]=[];
  partTopics:ITopic[]=[];
  public evalnames:string[]=[];
  public selnames:string="";
  public fields: Object = { value: 'name' };
  public value: string = '';
  public valarr:string[]=[];
  public handleall:Array<string[]>=[];
  public waterMark: string = 'e.g. Data Structures';
  public eval2dArray:Array<IEvaluationArea[]>=[];
  public Fetched:Array<string[]>=[];
  public deletePreFetched:Array<IAnswerCheckEvalAreaMap>=[];

  isQCollapsedarr:boolean[]=[];
  isACollapsedarr:boolean[]=[];
  
  isNewACollarr:boolean[]=[];
  newAC:IAnswerCheck={
    name:null,question:null
  }

  newQuestion:IQuestion=
  { id:null,
    min_experience: null,
    description: null,
    expected_answers: null,
    sequence: null,
    isActive: null,
    skill:{
      id:null,name:null
    },
    topic:{
      id:null,name:null
    }
  };
  isReadOnlyT:boolean;
  delQuestion:IQuestion;

  hasACs:boolean[]=[];
  constructor(protected questionService: QuestionService,private answerCheckService:AnswerCheckService,public evaluationAreaService: EvaluationAreaService,
    protected answerCheckEvalAreaMapService: AnswerCheckEvalAreaMapService,protected skillService: SkillService,protected topicService: TopicService,
    protected jhiAlertService: JhiAlertService,private rend:Renderer2,private router:Router) {
      this.questions = [];
  }
  loadAll(){
    this.isSaving=false;
    this.questionService
            .query()
            .subscribe(
                (res: HttpResponse<IQuestion[]>) => {
                  this.paginateQuestions(res.body,this.cs)
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    this.evaluationAreaService
            .query()
            .pipe(
                filter((res: HttpResponse<IEvaluationArea[]>) => res.ok),
                map((res: HttpResponse<IEvaluationArea[]>) => res.body)
            )
            .subscribe(
                (res: IEvaluationArea[]) => {
                    this.evaluationAreas = res;
                    for(let y of this.evaluationAreas){
                      this.evalnames.push(y.name);
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
      this.skillService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISkill[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISkill[]>) => response.body)
            )
            .subscribe((res: ISkill[]) => (this.partSkills = res), (res: HttpErrorResponse) => this.onError(res.message));
      this.topicService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITopic[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITopic[]>) => response.body)
            )
            .subscribe((res: ITopic[]) => (this.partTopics = res), (res: HttpErrorResponse) => this.onError(res.message));
  }
  ngOnInit() {
    this.loadAll();
  }
  ngOnChanges(){
    this.loadAll();
  }
  public showModal(question:IQuestion):void{
    console.log("Initially "+this.modal.toArray()[0].isShown);
    let modEl=document.getElementById('frameModalTop');
    if(modEl.classList.contains('zoomOut')===true){
      modEl.classList.remove('zoomOut');
      modEl.classList.add('zoomIn');
    }
    this.editQuestion=question;

    this.newAC.question=this.editQuestion;
    console.log(JSON.stringify(this.newAC))

    console.log(this.acArray[question.sequence-1]);
    //console.log(JSON.stringify(this.editQuestion)+"\t"+JSON.stringify(question));
    if(this.acArray[question.sequence-1].length===0)
    {this.answerCheckService
            .query({
              size:100
            })
            .subscribe(
                (res: HttpResponse<IAnswerCheck[]>) => {
                  this.paginateAnswerChecks(res.body, question,this.evaluationAreas)
                  console.log(this.hasACs[question.sequence-1])
                  if(this.hasACs[question.sequence-1]===false){
                    setTimeout(()=>{
                      this.modal.toArray()[0].show();
                      let elem1=document.getElementsByClassName('modal-backdrop')[0];
                      console.log("Modal backdrop ");
                      console.log(elem1);
                      elem1.addEventListener("click",()=>{
                        let modEl=document.getElementById('frameModalTop');
                        if(modEl.classList.contains('shake')){
                          modEl.classList.remove('shake');
                          setTimeout(()=>{
                            modEl.classList.add('shake');
                          },100);
                        }
                        if(modEl.classList.contains('zoomIn')){
                          modEl.classList.remove('zoomIn');
                          modEl.classList.add('shake');
                        }
                        console.log("BackDrop click");
                      });
                    },500);
                  }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );   
    }
    console.log(this.evaluationAreas);
    let dumvalar:string[]=[];
    for(let v=0;v<this.evaluationAreas.length;v++){
      let es:string="";
      dumvalar.push(es);
    }
    this.valarr=dumvalar;
    console.log(this.handleall.length+"\t"+this.valarr.length);
    if(!this.already_opened[question.sequence-1])
    {this.answerCheckEvalAreaMapService
            .query()
            .pipe(
                filter((res: HttpResponse<IAnswerCheckEvalAreaMap[]>) => res.ok),
                map((res: HttpResponse<IAnswerCheckEvalAreaMap[]>) => res.body)
            )
            .subscribe(
                (res: IAnswerCheckEvalAreaMap[]) => {
                  this.answerCheckEvalAreaMaps = res;
                  let dumFetched:Array<string[]>=[];  
                  for(let i in this.acArray[question.sequence-1]){
                      dumFetched.push([]);
                      for(let j of res){
                        if(j.answerCheck.id===this.acArray[question.sequence-1][i].id && j.answerCheck.name===this.acArray[question.sequence-1][i].name){
                            dumFetched[i].push(j.evaluationArea.name);
                            const checkel=this.eval2dArray[i].find(el=>el.name===j.evaluationArea.name);
                            console.log("checkind "+JSON.stringify(checkel));
                            this.eval2dArray[i]=this.eval2dArray[i].filter(el=>{
                              return (el.id!==checkel.id && el.name!==checkel.name)
                            });
                            this.handleall[i].push(j.evaluationArea.name);
                        }
                      }
                    }
                    this.Fetched=dumFetched;
                    console.log(this.handleall);
                    console.log(JSON.stringify(this.Fetched));
                    //console.log(JSON.stringify(this.answerCheckEvalAreaMaps));
                    if(this.Fetched.length>0){
                      setTimeout(()=>{
                        this.modal.toArray()[0].show();
                        let elem1=document.getElementsByClassName('modal-backdrop')[0];
                        let elem2=document.getElementsByClassName('modal-open')[0];
                        let elem3=document.getElementsByClassName('modal-dialog-scrollable')[0];
                        console.log("Modal backdrop ");
                        console.log(elem1);
                        console.log(elem2);
                        console.log(elem3);
                        elem1.addEventListener("click",()=>{
                          let modEl=document.getElementById('frameModalTop');
                          // if(!modEl.classList.contains('shake')){
                          //     modEl.classList.add('shake');
                          // }
                          if(modEl.classList.contains('shake')){
                            modEl.classList.remove('shake');
                            setTimeout(()=>{
                              modEl.classList.add('shake');
                            },100);
                          }
                          if(modEl.classList.contains('zoomIn')){
                            modEl.classList.remove('zoomIn');
                            modEl.classList.add('shake');
                          }
                          console.log("BackDrop click");
                        });
                      },500);
                      this.already_opened[question.sequence-1]=true;
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );}
            if(this.already_opened[question.sequence-1]){
              setTimeout(()=>{
                this.modal.toArray()[0].show();
                let elem1=document.getElementsByClassName('modal-backdrop')[0];
                console.log("Modal backdrop ");
                console.log(elem1);
                elem1.addEventListener("click",()=>{
                  let modEl=document.getElementById('frameModalTop');
                  // if(!modEl.classList.contains('shake')){
                  //     modEl.classList.add('shake');
                  // }
                  if(modEl.classList.contains('shake')){
                    modEl.classList.remove('shake');
                    setTimeout(()=>{
                      modEl.classList.add('shake');
                    },100);
                  }
                  if(modEl.classList.contains('zoomIn')){
                    modEl.classList.remove('zoomIn');
                    modEl.classList.add('shake');
                  }
                  console.log("BackDrop click");
                });
              },500);
            }
            
  }
  onChange(args: any,i:number): void {
    let valueEle: HTMLInputElement = document.getElementsByClassName('e-input')[i] as HTMLInputElement;
    // make empty the input value when typed characters is 'null' in input element
    // console.log("i = "+i);
    // console.log(this.eval2dArray);
    // console.log(this.eval2dArray[i]);
    if (this.AutoCompleteObj.toArray()[i].value === "null" || this.AutoCompleteObj.toArray()[i].value === null || this.AutoCompleteObj.toArray()[i].value === "") {
        valueEle.value = '';
    }
    if(this.valarr[i]!==null && this.evalnames.includes(this.valarr[i]))
    {
      console.log("value is "+this.valarr[i]);
      this.handleall[i].push(this.valarr[i]);
      console.log("Handelall Array : \n"+JSON.stringify(this.handleall));
      const exactEL = this.eval2dArray[i].find(el=>el.name===this.valarr[i]);
      console.log("Exact element = "+JSON.stringify(exactEL));
      this.eval2dArray[i]=this.eval2dArray[i].filter(el=>{
        return (el.id!==exactEL.id && el.name!==exactEL.name)
      })
      console.log("Changed 2d array");
      console.log("specific array "+JSON.stringify(this.eval2dArray[i]));
      console.log("Changed "+JSON.stringify(this.eval2dArray));
      // if(index>-1){
      //   this.eval2dArray[i].splice(index,1);
      // }
    }
  }
  removeFromList(val:string,i:number,ac:IAnswerCheck){
    let el=document.getElementById(val);
    console.log(el.parentElement);
    this.rend.removeChild(el.parentElement,el);
    let evobj:IEvaluationArea=this.evaluationAreas.find(el=>el.name===val);
    console.log("Deleted obj "+JSON.stringify(evobj));
    if(this.Fetched[i].includes(val)){
      console.log("Deleting prfetched object")
      let deleteMap:IAnswerCheckEvalAreaMap=this.answerCheckEvalAreaMaps.find(ele=>{
        return ((ele.answerCheck.id===ac.id) && (ele.answerCheck.name===ac.name)) && 
        ((ele.evaluationArea.id===evobj.id) && (ele.evaluationArea.name===evobj.name))
      });
      console.log(JSON.stringify(deleteMap));
      this.deletePreFetched.push(deleteMap);
    }
    //this.handleall[i].filter(st=>st!==val);
    const index = this.handleall[i].indexOf(val, 0);
    console.log("Handled val = "+val+"inside alert boxes "+index)
    if (index > -1) {
      this.handleall[i].splice(index, 1);
    }
    console.log("Remaining Array\n"+this.handleall[i]);
    console.log(this.handleall); 
    let lastid:number=((this.eval2dArray[i])[this.eval2dArray[i].length-1]).id;
    let newel:IEvaluationArea={
      id:lastid+1,
      name:val
    };
    this.eval2dArray[i].push(newel);
    console.log("Restored array "+JSON.stringify(this.eval2dArray[i]));    
  }
  closeModal(question:IQuestion){
    let modEl=document.getElementById('frameModalTop');
    modEl.classList.remove('zoomIn');
    //this.rend.addClass(this.modal,'slideOutRight animated');
    modEl.classList.add('zoomOut');
    this.modal.toArray()[0].hide();
    console.log('checking after close'+this.modal.toArray()[0].closed.closed);
  }
  onClick(e:Event){
    console.log('Clicked:');//, e.srcElement);
    console.log((<Element>e.target));
    let catchM=(<Element>e.target);
    if(catchM.id=="frameModalTop" || catchM.id=="CreateQuestionModal" || catchM.id=="DeleteQuestionModal")
    {
      if(catchM.classList.contains('shake')){
        catchM.classList.remove('shake');
        setTimeout(()=>{
          catchM.classList.add('shake');
        },100);
      }
      if(catchM.classList.contains('zoomIn')){
        catchM.classList.remove('zoomIn');
        catchM.classList.add('shake');
      }
      
    }
  }
  onClickedOutside(e: Event) {
    console.log('Clicked outside:', e.srcElement);
    console.log((<Element>e.srcElement).id);
    console.log((<Element>e.target).textContent);
    
    // let modEl=document.getElementById('frameModalTop');
    // if(!modEl.classList.contains('shake')){
    //   modEl.classList.add('shake');
    // }
  }
  saveQuestion(){
    this.isSaving=true;
    let noModif:number=0;
    let totalSize:number=0;
    console.log(JSON.stringify(this.editQuestion));
    //console.log(JSON.stringify(this.answerCheckEvalAreaMaps));
    console.log("Handel new "+this.handleall+" "+this.handleall.length);
    console.log("Prefetched Delete "+JSON.stringify(this.deletePreFetched));
    for(let elem of this.deletePreFetched){
      this.subscribeToSaveResponse(this.answerCheckEvalAreaMapService.delete(elem.id));
    }
    for(let i in this.handleall){
      for(let j of this.handleall[i]){
        let evobj:IEvaluationArea=this.evaluationAreas.find(el=>el.name===j);
        let addMap:IAnswerCheckEvalAreaMap={
          answerCheck:this.acArray[this.editQuestion.sequence-1][i],
          evaluationArea:evobj
        }
        //console.log(JSON.stringify(addMap));
        let duplicateIndex=this.answerCheckEvalAreaMaps.findIndex(el=>{
          return ((el.answerCheck.id===addMap.answerCheck.id) && (el.answerCheck.name===addMap.answerCheck.name)) 
          && ((el.evaluationArea.name===addMap.evaluationArea.name));
        });
        console.log("duplicate at "+duplicateIndex);
        const intersectionIndex:number=this.deletePreFetched.findIndex(el=>{
          return (((el.answerCheck.id===addMap.answerCheck.id) && (el.answerCheck.name===addMap.answerCheck.name))
          &&((el.evaluationArea.id===addMap.evaluationArea.id) && (el.evaluationArea.name===addMap.evaluationArea.name)));
        });
        //console.log("intersection index "+intersectionIndex+" for common element "+JSON.stringify(addMap));
        if(intersectionIndex>-1){
          console.log("intersection index "+intersectionIndex+" for common element "+JSON.stringify(addMap));
          this.subscribeToSaveResponse(this.answerCheckEvalAreaMapService.create(addMap));
        }
        if(duplicateIndex===-1){
          console.log(addMap);
          //let lastSeenId:number=this.answerCheckEvalAreaMaps[this.answerCheckEvalAreaMaps.length-1].id;
          this.subscribeToSaveResponse(this.answerCheckEvalAreaMapService.create(addMap));
          // addMap.id=lastSeenId;
          // this.answerCheckEvalAreaMaps.push(addMap);
        }
        else if(duplicateIndex>-1){
          noModif++;
        }
      }
      totalSize+=this.handleall[i].length;
    }
    if(noModif===totalSize){
      console.log("No changes made");
    }
    this.modal.toArray()[0].hide();
  }
  drop(event: CdkDragDrop<IQuestion[]>) {
    moveItemInArray(this.questions, event.previousIndex, event.currentIndex);
    console.log(event.previousIndex+"\t"+event.currentIndex);
    //console.log(JSON.stringify(this.questions[event.previousIndex])+"\t"+JSON.stringify(this.questions[event.currentIndex]));
    console.log(JSON.stringify(event.container.data[event.currentIndex])+"\t"+JSON.stringify(event.container.data[event.previousIndex]));
    console.log(JSON.stringify(event.container.data));
    for(let ind in event.container.data){
      console.log((Number.parseInt(ind)+1)+"  "+JSON.stringify(event.container.data[ind]));
    }
    let conf=confirm("Do u wanna run");
    if(conf===true){
    for(let k in event.container.data){
      let upqt:IQuestion=event.container.data[k];
      upqt.sequence=Number.parseInt(k)+1;
      this.questionService.update(upqt).subscribe();
    }
    setTimeout(()=>{},2000);
    window.location.reload();
    }

    // let swi=this.questions[event.previousIndex].sequence;
    // this.questions[event.previousIndex].sequence=this.questions[event.currentIndex].sequence;
    // this.questions[event.currentIndex].sequence=swi;
    // this.questionService.update(this.questions[event.previousIndex]).subscribe();
    // this.questionService.update(this.questions[event.currentIndex]).subscribe();
    // console.log("Changed");
    // console.log(JSON.stringify(this.questions[event.previousIndex])+"\t"+JSON.stringify(this.questions[event.currentIndex]));
    //this.ngOnChanges();
  }
  toggleDiv(question:IQuestion){
    this.hide[question.id-1]=!this.hide[question.id-1];
  }
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnswerCheckEvalAreaMap>>) {
    result.subscribe(
        (res: HttpResponse<IAnswerCheckEvalAreaMap>) => this.onSaveSuccess(),
        (res: HttpErrorResponse) => this.onSaveError()
    );
  }
  toggleQCollapse(question:IQuestion,event:Event){
    this.isQCollapsedarr[question.id-1]=!this.isQCollapsedarr[question.id-1];
  }
  toggleACollapse(question:IQuestion,event:Event){
    this.isACollapsedarr[question.id-1]=!this.isACollapsedarr[question.id-1];
  }
  toggleNewACCollapse(question:IQuestion,event:Event){
    this.isNewACollarr[question.id-1]=!this.isNewACollarr[question.id-1];
  }
  addAC(){
    console.log(this.newAC);
    this.answerCheckService.create(this.newAC).subscribe();
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/screen']);
      }); 

  }
  showCNQ(){
    let modEl=document.getElementById('CreateQuestionModal');
    if(modEl.classList.contains('zoomOut')===true){
      modEl.classList.remove('zoomOut');
      modEl.classList.add('zoomIn');
    }
    this.modal.toArray()[1].show();
    let elem1=document.getElementsByClassName('modal-backdrop')[0];
    console.log("Modal backdrop ");
    console.log(elem1);
    elem1.addEventListener("click",()=>{
      let modEl=document.getElementById('CreateQuestionModal');
      if(modEl.classList.contains('shake')){
        modEl.classList.remove('shake');
        setTimeout(()=>{
          modEl.classList.add('shake');
        },100);
      }
      if(modEl.classList.contains('zoomIn')){
        modEl.classList.remove('zoomIn');
        modEl.classList.add('shake');
      }
      console.log("BackDrop click on Create New Question");
    })
  }
  closeCNQ(){
    let modEl=document.getElementById('CreateQuestionModal');
    modEl.classList.remove('zoomIn');
    modEl.classList.add('zoomOut');
    this.modal.toArray()[1].hide();
  }
  saveNewQ(){
    this.questionService.create(this.newQuestion).subscribe();
    console.log(this.newQuestion);
    this.modal.toArray()[1].hide();
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/screen']);
      });
  }
  deleteQuestion(question:IQuestion){
    this.delQuestion=question;
    let modEl=document.getElementById('DeleteQuestionModal');
    if(modEl.classList.contains('zoomOut')===true){
      modEl.classList.remove('zoomOut');
      modEl.classList.add('zoomIn');
    }
    console.log(this.cs);
    this.modal.toArray()[2].show();
    let elem1=document.getElementsByClassName('modal-backdrop')[0];
    console.log("Modal backdrop ");
    console.log(elem1);
    elem1.addEventListener("click",()=>{
      let modEl=document.getElementById('DeleteQuestionModal');
      if(modEl.classList.contains('shake')){
        modEl.classList.remove('shake');
        setTimeout(()=>{
          modEl.classList.add('shake');
        },100);
      }
      if(modEl.classList.contains('zoomIn')){
        modEl.classList.remove('zoomIn');
        modEl.classList.add('shake');
      }
      console.log("BackDrop click on Delete Question");
    })
  }
  closeDQ(){
    let modEl=document.getElementById('DeleteQuestionModal');
    modEl.classList.remove('zoomIn');
    modEl.classList.add('zoomOut');
    this.modal.toArray()[2].hide();
  }
  confirmDelete(delQuestion:IQuestion){
    this.questionService.delete(delQuestion.id).subscribe();
    this.modal.toArray()[2].hide();
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/screen']);
      }); 
  }
    protected onSaveSuccess() {
        this.isSaving = false;
    }

    protected onSaveError() {
        this.isSaving = false;
    }
  protected paginateQuestions(data: IQuestion[],cs:ISkill) {
    let q:IQuestion[]=[];
    let dac:Array<IAnswerCheck[]>=[];
    let dumalop:boolean[]=[];
    let dumhide:boolean[]=[];
    let dumQ:boolean[]=[];
    let dumA:boolean[]=[];
    let dumnewAC:boolean[]=[];
    let dumHasACs:boolean[]=[];
    for (let i = 0; i < data.length; i++) {
      if(data[i].skill.id===cs.id){  
        q.push(data[i]);
        dac.push([]);
        dumalop.push(false);
        dumhide.push(false);
        dumQ.push(false);
        dumA.push(false);
        dumnewAC.push(false);
        dumHasACs.push(false);
      }
    }
    // for(let j=0;j<q.length;j++){
    //   this.already_opened[j]=false;
    // }
    this.already_opened=dumalop;
    this.hide=dumhide;
    this.isQCollapsedarr=dumQ;
    this.isACollapsedarr=dumA;
    this.isNewACollarr=dumnewAC;
    this.hasACs=dumHasACs;
    console.log("already opened array "+JSON.stringify(this.already_opened));
    q.sort((q1:IQuestion,q2:IQuestion)=>{
      if(q1.sequence<q2.sequence){
        return -1; 
      }
      else if(q1.sequence>q2.sequence){
        return 1;
      }
      return 0;
    })
    this.questions=q;
    this.acArray=dac;
    console.log(JSON.stringify(this.cs)+"\t"+JSON.stringify(cs));
    console.log(this.acArray);
    this.newQuestion.skill=cs;
    if(q.length>0){
      this.isReadOnlyT=true;
      this.newQuestion.topic=q[0].topic;
    }
    else{
      if(this.newQuestion.topic.id!==null){
        this.isReadOnlyT=false;
        this.newQuestion.topic={
          id:null,name:null
        };
      }
    }
    console.log("New Question "+JSON.stringify(this.newQuestion));
}
  protected paginateAnswerChecks(data: IAnswerCheck[], question:IQuestion,evnames:IEvaluationArea[]) {
    let ans:IAnswerCheck[]=[];
    let dum2dArray:Array<IEvaluationArea[]>=[];
    for (let i = 0; i < data.length; i++) {
      if(data[i].question.id===question.id)  
      {
        ans.push(data[i]);
      }
    }
    //this.answerChecks=ans;
    this.acArray[question.sequence-1]=ans;
    let dharr:Array<string[]>=[];
    for(let v=0;v<ans.length;v++){
      dharr.push([]);
      dum2dArray.push(evnames);
    }
    this.handleall=dharr;
    this.eval2dArray=dum2dArray;
    console.log("Answer Check called");
    console.log("AC's for Question "+question.id+" are "+JSON.stringify(this.acArray[question.sequence-1]))
    if(this.acArray[question.sequence-1].length>0){
      this.hasACs[question.sequence-1]=true;
    }
    else{
      this.hasACs[question.sequence-1]=false;
    }
  }
protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
}
trackSkillById(index: number, item: ISkill) {
  return item.id;
}

trackTopicById(index: number, item: ITopic) {
  return item.id;
}
}
