import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckService } from './answer-check.service';

@Component({
    selector: 'jhi-answer-check-delete-dialog',
    templateUrl: './answer-check-delete-dialog.component.html'
})
export class AnswerCheckDeleteDialogComponent {
    answerCheck: IAnswerCheck;

    constructor(
        protected answerCheckService: AnswerCheckService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.answerCheckService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'answerCheckListModification',
                content: 'Deleted an answerCheck'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-answer-check-delete-popup',
    template: ''
})
export class AnswerCheckDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answerCheck }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AnswerCheckDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.answerCheck = answerCheck;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/answer-check', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/answer-check', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
