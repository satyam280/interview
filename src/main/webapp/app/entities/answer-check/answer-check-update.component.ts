import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckService } from './answer-check.service';
import { IQuestion } from 'app/shared/model/question.model';
import { QuestionService } from 'app/entities/question';

@Component({
    selector: 'jhi-answer-check-update',
    templateUrl: './answer-check-update.component.html'
})
export class AnswerCheckUpdateComponent implements OnInit {
    answerCheck: IAnswerCheck;
    isSaving: boolean;

    questions: IQuestion[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected answerCheckService: AnswerCheckService,
        protected questionService: QuestionService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ answerCheck }) => {
            this.answerCheck = answerCheck;
        });
        this.questionService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IQuestion[]>) => mayBeOk.ok),
                map((response: HttpResponse<IQuestion[]>) => response.body)
            )
            .subscribe((res: IQuestion[]) => (this.questions = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.answerCheck.id !== undefined) {
            this.subscribeToSaveResponse(this.answerCheckService.update(this.answerCheck));
        } else {
            this.subscribeToSaveResponse(this.answerCheckService.create(this.answerCheck));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnswerCheck>>) {
        result.subscribe((res: HttpResponse<IAnswerCheck>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackQuestionById(index: number, item: IQuestion) {
        return item.id;
    }
}
