import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';

type EntityResponseType = HttpResponse<IAnswerCheck>;
type EntityArrayResponseType = HttpResponse<IAnswerCheck[]>;

@Injectable({ providedIn: 'root' })
export class AnswerCheckService {
    public resourceUrl = SERVER_API_URL + 'api/answer-checks';

    constructor(protected http: HttpClient) {}

    create(answerCheck: IAnswerCheck): Observable<EntityResponseType> {
        return this.http.post<IAnswerCheck>(this.resourceUrl, answerCheck, { observe: 'response' });
    }

    update(answerCheck: IAnswerCheck): Observable<EntityResponseType> {
        return this.http.put<IAnswerCheck>(this.resourceUrl, answerCheck, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAnswerCheck>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAnswerCheck[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
