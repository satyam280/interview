import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAnswerCheck } from 'app/shared/model/answer-check.model';

@Component({
    selector: 'jhi-answer-check-detail',
    templateUrl: './answer-check-detail.component.html'
})
export class AnswerCheckDetailComponent implements OnInit {
    answerCheck: IAnswerCheck;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answerCheck }) => {
            this.answerCheck = answerCheck;
        });
    }

    previousState() {
        window.history.back();
    }
}
