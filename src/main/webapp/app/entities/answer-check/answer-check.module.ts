import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    AnswerCheckComponent,
    AnswerCheckDetailComponent,
    AnswerCheckUpdateComponent,
    AnswerCheckDeletePopupComponent,
    AnswerCheckDeleteDialogComponent,
    answerCheckRoute,
    answerCheckPopupRoute
} from './';

const ENTITY_STATES = [...answerCheckRoute, ...answerCheckPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AnswerCheckComponent,
        AnswerCheckDetailComponent,
        AnswerCheckUpdateComponent,
        AnswerCheckDeleteDialogComponent,
        AnswerCheckDeletePopupComponent
    ],
    entryComponents: [AnswerCheckComponent, AnswerCheckUpdateComponent, AnswerCheckDeleteDialogComponent, AnswerCheckDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewAnswerCheckModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
