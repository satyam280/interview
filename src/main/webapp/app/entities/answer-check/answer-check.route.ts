import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckService } from './answer-check.service';
import { AnswerCheckComponent } from './answer-check.component';
import { AnswerCheckDetailComponent } from './answer-check-detail.component';
import { AnswerCheckUpdateComponent } from './answer-check-update.component';
import { AnswerCheckDeletePopupComponent } from './answer-check-delete-dialog.component';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';

@Injectable({ providedIn: 'root' })
export class AnswerCheckResolve implements Resolve<IAnswerCheck> {
    constructor(private service: AnswerCheckService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAnswerCheck> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<AnswerCheck>) => response.ok),
                map((answerCheck: HttpResponse<AnswerCheck>) => answerCheck.body)
            );
        }
        return of(new AnswerCheck());
    }
}

export const answerCheckRoute: Routes = [
    {
        path: '',
        component: AnswerCheckComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheck.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AnswerCheckDetailComponent,
        resolve: {
            answerCheck: AnswerCheckResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheck.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AnswerCheckUpdateComponent,
        resolve: {
            answerCheck: AnswerCheckResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheck.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AnswerCheckUpdateComponent,
        resolve: {
            answerCheck: AnswerCheckResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheck.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const answerCheckPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AnswerCheckDeletePopupComponent,
        resolve: {
            answerCheck: AnswerCheckResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheck.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
