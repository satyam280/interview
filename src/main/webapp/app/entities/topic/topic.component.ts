import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITopic } from 'app/shared/model/topic.model';
import { AccountService } from 'app/core';
import { TopicService } from './topic.service';

@Component({
    selector: 'jhi-topic',
    templateUrl: './topic.component.html'
})
export class TopicComponent implements OnInit, OnDestroy {
    topics: ITopic[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected topicService: TopicService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.topicService
            .query()
            .pipe(
                filter((res: HttpResponse<ITopic[]>) => res.ok),
                map((res: HttpResponse<ITopic[]>) => res.body)
            )
            .subscribe(
                (res: ITopic[]) => {
                    this.topics = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInTopics();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ITopic) {
        return item.id;
    }

    registerChangeInTopics() {
        this.eventSubscriber = this.eventManager.subscribe('topicListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
