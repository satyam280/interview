import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';
import { AnswerCheckFeedbackService } from './answer-check-feedback.service';
import { AnswerCheckFeedbackComponent } from './answer-check-feedback.component';
import { AnswerCheckFeedbackDetailComponent } from './answer-check-feedback-detail.component';
import { AnswerCheckFeedbackUpdateComponent } from './answer-check-feedback-update.component';
import { AnswerCheckFeedbackDeletePopupComponent } from './answer-check-feedback-delete-dialog.component';
import { IAnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';

@Injectable({ providedIn: 'root' })
export class AnswerCheckFeedbackResolve implements Resolve<IAnswerCheckFeedback> {
    constructor(private service: AnswerCheckFeedbackService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAnswerCheckFeedback> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<AnswerCheckFeedback>) => response.ok),
                map((answerCheckFeedback: HttpResponse<AnswerCheckFeedback>) => answerCheckFeedback.body)
            );
        }
        return of(new AnswerCheckFeedback());
    }
}

export const answerCheckFeedbackRoute: Routes = [
    {
        path: '',
        component: AnswerCheckFeedbackComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AnswerCheckFeedbackDetailComponent,
        resolve: {
            answerCheckFeedback: AnswerCheckFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AnswerCheckFeedbackUpdateComponent,
        resolve: {
            answerCheckFeedback: AnswerCheckFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AnswerCheckFeedbackUpdateComponent,
        resolve: {
            answerCheckFeedback: AnswerCheckFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const answerCheckFeedbackPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AnswerCheckFeedbackDeletePopupComponent,
        resolve: {
            answerCheckFeedback: AnswerCheckFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckFeedback.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
