import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';
import { AnswerCheckFeedbackService } from './answer-check-feedback.service';

@Component({
    selector: 'jhi-answer-check-feedback-delete-dialog',
    templateUrl: './answer-check-feedback-delete-dialog.component.html'
})
export class AnswerCheckFeedbackDeleteDialogComponent {
    answerCheckFeedback: IAnswerCheckFeedback;

    constructor(
        protected answerCheckFeedbackService: AnswerCheckFeedbackService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.answerCheckFeedbackService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'answerCheckFeedbackListModification',
                content: 'Deleted an answerCheckFeedback'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-answer-check-feedback-delete-popup',
    template: ''
})
export class AnswerCheckFeedbackDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answerCheckFeedback }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AnswerCheckFeedbackDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.answerCheckFeedback = answerCheckFeedback;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/answer-check-feedback', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/answer-check-feedback', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
