import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';
import { AnswerCheckFeedbackService } from './answer-check-feedback.service';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckService } from 'app/entities/answer-check';

@Component({
    selector: 'jhi-answer-check-feedback-update',
    templateUrl: './answer-check-feedback-update.component.html'
})
export class AnswerCheckFeedbackUpdateComponent implements OnInit {
    answerCheckFeedback: IAnswerCheckFeedback;
    isSaving: boolean;

    interviews: IInterview[];

    answerchecks: IAnswerCheck[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected answerCheckFeedbackService: AnswerCheckFeedbackService,
        protected interviewService: InterviewService,
        protected answerCheckService: AnswerCheckService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ answerCheckFeedback }) => {
            this.answerCheckFeedback = answerCheckFeedback;
        });
        this.interviewService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IInterview[]>) => mayBeOk.ok),
                map((response: HttpResponse<IInterview[]>) => response.body)
            )
            .subscribe((res: IInterview[]) => (this.interviews = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.answerCheckService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IAnswerCheck[]>) => mayBeOk.ok),
                map((response: HttpResponse<IAnswerCheck[]>) => response.body)
            )
            .subscribe((res: IAnswerCheck[]) => (this.answerchecks = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.answerCheckFeedback.id !== undefined) {
            this.subscribeToSaveResponse(this.answerCheckFeedbackService.update(this.answerCheckFeedback));
        } else {
            this.subscribeToSaveResponse(this.answerCheckFeedbackService.create(this.answerCheckFeedback));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnswerCheckFeedback>>) {
        result.subscribe((res: HttpResponse<IAnswerCheckFeedback>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInterviewById(index: number, item: IInterview) {
        return item.id;
    }

    trackAnswerCheckById(index: number, item: IAnswerCheck) {
        return item.id;
    }
}
