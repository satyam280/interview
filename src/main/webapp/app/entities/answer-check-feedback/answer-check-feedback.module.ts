import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    AnswerCheckFeedbackComponent,
    AnswerCheckFeedbackDetailComponent,
    AnswerCheckFeedbackUpdateComponent,
    AnswerCheckFeedbackDeletePopupComponent,
    AnswerCheckFeedbackDeleteDialogComponent,
    answerCheckFeedbackRoute,
    answerCheckFeedbackPopupRoute
} from './';

const ENTITY_STATES = [...answerCheckFeedbackRoute, ...answerCheckFeedbackPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AnswerCheckFeedbackComponent,
        AnswerCheckFeedbackDetailComponent,
        AnswerCheckFeedbackUpdateComponent,
        AnswerCheckFeedbackDeleteDialogComponent,
        AnswerCheckFeedbackDeletePopupComponent
    ],
    entryComponents: [
        AnswerCheckFeedbackComponent,
        AnswerCheckFeedbackUpdateComponent,
        AnswerCheckFeedbackDeleteDialogComponent,
        AnswerCheckFeedbackDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewAnswerCheckFeedbackModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
