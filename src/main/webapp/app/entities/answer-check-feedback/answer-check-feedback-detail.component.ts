import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAnswerCheckFeedback } from 'app/shared/model/answer-check-feedback.model';

@Component({
    selector: 'jhi-answer-check-feedback-detail',
    templateUrl: './answer-check-feedback-detail.component.html'
})
export class AnswerCheckFeedbackDetailComponent implements OnInit {
    answerCheckFeedback: IAnswerCheckFeedback;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answerCheckFeedback }) => {
            this.answerCheckFeedback = answerCheckFeedback;
        });
    }

    previousState() {
        window.history.back();
    }
}
