import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';
import { EvaluationAreaService } from './evaluation-area.service';

@Component({
    selector: 'jhi-evaluation-area-update',
    templateUrl: './evaluation-area-update.component.html'
})
export class EvaluationAreaUpdateComponent implements OnInit {
    evaluationArea: IEvaluationArea;
    isSaving: boolean;

    constructor(protected evaluationAreaService: EvaluationAreaService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ evaluationArea }) => {
            this.evaluationArea = evaluationArea;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.evaluationArea.id !== undefined) {
            this.subscribeToSaveResponse(this.evaluationAreaService.update(this.evaluationArea));
        } else {
            this.subscribeToSaveResponse(this.evaluationAreaService.create(this.evaluationArea));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IEvaluationArea>>) {
        result.subscribe((res: HttpResponse<IEvaluationArea>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
