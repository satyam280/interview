import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';
import { EvaluationAreaService } from './evaluation-area.service';

@Component({
    selector: 'jhi-evaluation-area-delete-dialog',
    templateUrl: './evaluation-area-delete-dialog.component.html'
})
export class EvaluationAreaDeleteDialogComponent {
    evaluationArea: IEvaluationArea;

    constructor(
        protected evaluationAreaService: EvaluationAreaService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.evaluationAreaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'evaluationAreaListModification',
                content: 'Deleted an evaluationArea'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-evaluation-area-delete-popup',
    template: ''
})
export class EvaluationAreaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ evaluationArea }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(EvaluationAreaDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.evaluationArea = evaluationArea;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/evaluation-area', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/evaluation-area', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
