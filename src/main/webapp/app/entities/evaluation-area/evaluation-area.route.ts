import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { EvaluationArea } from 'app/shared/model/evaluation-area.model';
import { EvaluationAreaService } from './evaluation-area.service';
import { EvaluationAreaComponent } from './evaluation-area.component';
import { EvaluationAreaDetailComponent } from './evaluation-area-detail.component';
import { EvaluationAreaUpdateComponent } from './evaluation-area-update.component';
import { EvaluationAreaDeletePopupComponent } from './evaluation-area-delete-dialog.component';
import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';

@Injectable({ providedIn: 'root' })
export class EvaluationAreaResolve implements Resolve<IEvaluationArea> {
    constructor(private service: EvaluationAreaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEvaluationArea> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<EvaluationArea>) => response.ok),
                map((evaluationArea: HttpResponse<EvaluationArea>) => evaluationArea.body)
            );
        }
        return of(new EvaluationArea());
    }
}

export const evaluationAreaRoute: Routes = [
    {
        path: '',
        component: EvaluationAreaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.evaluationArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: EvaluationAreaDetailComponent,
        resolve: {
            evaluationArea: EvaluationAreaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.evaluationArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: EvaluationAreaUpdateComponent,
        resolve: {
            evaluationArea: EvaluationAreaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.evaluationArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: EvaluationAreaUpdateComponent,
        resolve: {
            evaluationArea: EvaluationAreaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.evaluationArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const evaluationAreaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: EvaluationAreaDeletePopupComponent,
        resolve: {
            evaluationArea: EvaluationAreaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.evaluationArea.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
