import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';

type EntityResponseType = HttpResponse<IEvaluationArea>;
type EntityArrayResponseType = HttpResponse<IEvaluationArea[]>;

@Injectable({ providedIn: 'root' })
export class EvaluationAreaService {
    public resourceUrl = SERVER_API_URL + 'api/evaluation-areas';

    constructor(protected http: HttpClient) {}

    create(evaluationArea: IEvaluationArea): Observable<EntityResponseType> {
        return this.http.post<IEvaluationArea>(this.resourceUrl, evaluationArea, { observe: 'response' });
    }

    update(evaluationArea: IEvaluationArea): Observable<EntityResponseType> {
        return this.http.put<IEvaluationArea>(this.resourceUrl, evaluationArea, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IEvaluationArea>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IEvaluationArea[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
