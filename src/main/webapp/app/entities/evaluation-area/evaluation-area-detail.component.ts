import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';

@Component({
    selector: 'jhi-evaluation-area-detail',
    templateUrl: './evaluation-area-detail.component.html'
})
export class EvaluationAreaDetailComponent implements OnInit {
    evaluationArea: IEvaluationArea;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ evaluationArea }) => {
            this.evaluationArea = evaluationArea;
        });
    }

    previousState() {
        window.history.back();
    }
}
