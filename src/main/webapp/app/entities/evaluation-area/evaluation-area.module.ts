import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    EvaluationAreaComponent,
    EvaluationAreaDetailComponent,
    EvaluationAreaUpdateComponent,
    EvaluationAreaDeletePopupComponent,
    EvaluationAreaDeleteDialogComponent,
    evaluationAreaRoute,
    evaluationAreaPopupRoute
} from './';

const ENTITY_STATES = [...evaluationAreaRoute, ...evaluationAreaPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EvaluationAreaComponent,
        EvaluationAreaDetailComponent,
        EvaluationAreaUpdateComponent,
        EvaluationAreaDeleteDialogComponent,
        EvaluationAreaDeletePopupComponent
    ],
    entryComponents: [
        EvaluationAreaComponent,
        EvaluationAreaUpdateComponent,
        EvaluationAreaDeleteDialogComponent,
        EvaluationAreaDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewEvaluationAreaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
