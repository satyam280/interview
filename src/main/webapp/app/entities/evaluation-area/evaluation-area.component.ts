import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';
import { AccountService } from 'app/core';
import { EvaluationAreaService } from './evaluation-area.service';

@Component({
    selector: 'jhi-evaluation-area',
    templateUrl: './evaluation-area.component.html'
})
export class EvaluationAreaComponent implements OnInit, OnDestroy {
    evaluationAreas: IEvaluationArea[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected evaluationAreaService: EvaluationAreaService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.evaluationAreaService
            .query()
            .pipe(
                filter((res: HttpResponse<IEvaluationArea[]>) => res.ok),
                map((res: HttpResponse<IEvaluationArea[]>) => res.body)
            )
            .subscribe(
                (res: IEvaluationArea[]) => {
                    this.evaluationAreas = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInEvaluationAreas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IEvaluationArea) {
        return item.id;
    }

    registerChangeInEvaluationAreas() {
        this.eventSubscriber = this.eventManager.subscribe('evaluationAreaListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
