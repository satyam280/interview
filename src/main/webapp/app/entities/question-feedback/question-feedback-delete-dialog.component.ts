import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';
import { QuestionFeedbackService } from './question-feedback.service';

@Component({
    selector: 'jhi-question-feedback-delete-dialog',
    templateUrl: './question-feedback-delete-dialog.component.html'
})
export class QuestionFeedbackDeleteDialogComponent {
    questionFeedback: IQuestionFeedback;

    constructor(
        protected questionFeedbackService: QuestionFeedbackService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.questionFeedbackService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'questionFeedbackListModification',
                content: 'Deleted an questionFeedback'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-question-feedback-delete-popup',
    template: ''
})
export class QuestionFeedbackDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ questionFeedback }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(QuestionFeedbackDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.questionFeedback = questionFeedback;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/question-feedback', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/question-feedback', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
