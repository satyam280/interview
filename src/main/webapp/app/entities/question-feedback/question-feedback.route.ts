import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { QuestionFeedback } from 'app/shared/model/question-feedback.model';
import { QuestionFeedbackService } from './question-feedback.service';
import { QuestionFeedbackComponent } from './question-feedback.component';
import { QuestionFeedbackDetailComponent } from './question-feedback-detail.component';
import { QuestionFeedbackUpdateComponent } from './question-feedback-update.component';
import { QuestionFeedbackDeletePopupComponent } from './question-feedback-delete-dialog.component';
import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';

@Injectable({ providedIn: 'root' })
export class QuestionFeedbackResolve implements Resolve<IQuestionFeedback> {
    constructor(private service: QuestionFeedbackService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IQuestionFeedback> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<QuestionFeedback>) => response.ok),
                map((questionFeedback: HttpResponse<QuestionFeedback>) => questionFeedback.body)
            );
        }
        return of(new QuestionFeedback());
    }
}

export const questionFeedbackRoute: Routes = [
    {
        path: '',
        component: QuestionFeedbackComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.questionFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: QuestionFeedbackDetailComponent,
        resolve: {
            questionFeedback: QuestionFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.questionFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: QuestionFeedbackUpdateComponent,
        resolve: {
            questionFeedback: QuestionFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.questionFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: QuestionFeedbackUpdateComponent,
        resolve: {
            questionFeedback: QuestionFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.questionFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const questionFeedbackPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: QuestionFeedbackDeletePopupComponent,
        resolve: {
            questionFeedback: QuestionFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.questionFeedback.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
