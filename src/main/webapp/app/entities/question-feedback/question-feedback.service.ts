import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';

type EntityResponseType = HttpResponse<IQuestionFeedback>;
type EntityArrayResponseType = HttpResponse<IQuestionFeedback[]>;

@Injectable({ providedIn: 'root' })
export class QuestionFeedbackService {
    public resourceUrl = SERVER_API_URL + 'api/question-feedbacks';

    constructor(protected http: HttpClient) {}

    create(questionFeedback: IQuestionFeedback): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(questionFeedback);
        return this.http
            .post<IQuestionFeedback>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(questionFeedback: IQuestionFeedback): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(questionFeedback);
        return this.http
            .put<IQuestionFeedback>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IQuestionFeedback>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IQuestionFeedback[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(questionFeedback: IQuestionFeedback): IQuestionFeedback {
        const copy: IQuestionFeedback = Object.assign({}, questionFeedback, {
            startedAt:
                questionFeedback.startedAt != null && questionFeedback.startedAt.isValid() ? questionFeedback.startedAt.toJSON() : null,
            finishedAt:
                questionFeedback.finishedAt != null && questionFeedback.finishedAt.isValid() ? questionFeedback.finishedAt.toJSON() : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.startedAt = res.body.startedAt != null ? moment(res.body.startedAt) : null;
            res.body.finishedAt = res.body.finishedAt != null ? moment(res.body.finishedAt) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((questionFeedback: IQuestionFeedback) => {
                questionFeedback.startedAt = questionFeedback.startedAt != null ? moment(questionFeedback.startedAt) : null;
                questionFeedback.finishedAt = questionFeedback.finishedAt != null ? moment(questionFeedback.finishedAt) : null;
            });
        }
        return res;
    }
}
