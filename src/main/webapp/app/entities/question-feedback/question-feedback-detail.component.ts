import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';

@Component({
    selector: 'jhi-question-feedback-detail',
    templateUrl: './question-feedback-detail.component.html'
})
export class QuestionFeedbackDetailComponent implements OnInit {
    questionFeedback: IQuestionFeedback;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ questionFeedback }) => {
            this.questionFeedback = questionFeedback;
        });
    }

    previousState() {
        window.history.back();
    }
}
