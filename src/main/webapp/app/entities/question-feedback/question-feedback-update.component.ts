import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IQuestionFeedback } from 'app/shared/model/question-feedback.model';
import { QuestionFeedbackService } from './question-feedback.service';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview';
import { IQuestion } from 'app/shared/model/question.model';
import { QuestionService } from 'app/entities/question';

@Component({
    selector: 'jhi-question-feedback-update',
    templateUrl: './question-feedback-update.component.html'
})
export class QuestionFeedbackUpdateComponent implements OnInit {
    questionFeedback: IQuestionFeedback;
    isSaving: boolean;

    interviews: IInterview[];

    questions: IQuestion[];
    startedAt: string;
    finishedAt: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected questionFeedbackService: QuestionFeedbackService,
        protected interviewService: InterviewService,
        protected questionService: QuestionService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ questionFeedback }) => {
            this.questionFeedback = questionFeedback;
            this.startedAt = this.questionFeedback.startedAt != null ? this.questionFeedback.startedAt.format(DATE_TIME_FORMAT) : null;
            this.finishedAt = this.questionFeedback.finishedAt != null ? this.questionFeedback.finishedAt.format(DATE_TIME_FORMAT) : null;
        });
        this.interviewService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IInterview[]>) => mayBeOk.ok),
                map((response: HttpResponse<IInterview[]>) => response.body)
            )
            .subscribe((res: IInterview[]) => (this.interviews = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.questionService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IQuestion[]>) => mayBeOk.ok),
                map((response: HttpResponse<IQuestion[]>) => response.body)
            )
            .subscribe((res: IQuestion[]) => (this.questions = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.questionFeedback.startedAt = this.startedAt != null ? moment(this.startedAt, DATE_TIME_FORMAT) : null;
        this.questionFeedback.finishedAt = this.finishedAt != null ? moment(this.finishedAt, DATE_TIME_FORMAT) : null;
        if (this.questionFeedback.id !== undefined) {
            this.subscribeToSaveResponse(this.questionFeedbackService.update(this.questionFeedback));
        } else {
            this.subscribeToSaveResponse(this.questionFeedbackService.create(this.questionFeedback));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuestionFeedback>>) {
        result.subscribe((res: HttpResponse<IQuestionFeedback>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInterviewById(index: number, item: IInterview) {
        return item.id;
    }

    trackQuestionById(index: number, item: IQuestion) {
        return item.id;
    }
}
