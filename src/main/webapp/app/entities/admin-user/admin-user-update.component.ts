import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IAdminUser, AdminUser } from 'app/shared/model/admin-user.model';
import { AdminUserService } from './admin-user.service';

@Component({
    selector: 'jhi-admin-user-update',
    templateUrl: './admin-user-update.component.html'
})
export class AdminUserUpdateComponent implements OnInit {
    isSaving: boolean;
    adminUser: AdminUser;

    constructor(protected adminUserService: AdminUserService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

    ngOnInit() {
        this.isSaving = false;
        // this.activatedRoute.data.subscribe(({ adminUser }) => {
        // this.updateForm(adminUser);
        // });
        this.adminUser = new AdminUser();
    }

    // updateForm(adminUser: IAdminUser) {
    //     this.editForm.patchValue({
    //         id: adminUser.id,
    //         emailId: adminUser.emailId
    //     });
    // }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        // const adminUser = this.createFromForm();
        console.log(this.adminUser);
        if (this.adminUser.id !== undefined) {
            this.subscribeToSaveResponse(this.adminUserService.update(this.adminUser));
        } else {
            this.subscribeToSaveResponse(this.adminUserService.create(this.adminUser));
        }
    }

    // private createFromForm(): IAdminUser {
    // console.log('id ' + this.editForm.get(['id']).value);
    // console.log('id ' + this.editForm.get(['emailId']).value);

    // return {
    // ...new AdminUser(),
    // id: this.editForm.get(['id']).value,
    // emailId: this.editForm.get(['emailId']).value
    // };
    // }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAdminUser>>) {
        result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
