import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AdminUser } from 'app/shared/model/admin-user.model';
import { AdminUserService } from './admin-user.service';
import { AdminUserComponent } from './admin-user.component';
import { AdminUserDetailComponent } from './admin-user-detail.component';
import { AdminUserUpdateComponent } from './admin-user-update.component';
import { AdminUserDeletePopupComponent } from './admin-user-delete-dialog.component';
import { IAdminUser } from 'app/shared/model/admin-user.model';

@Injectable({ providedIn: 'root' })
export class AdminUserResolve implements Resolve<IAdminUser> {
    constructor(private service: AdminUserService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAdminUser> {
        const id = route.params['id'];
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<AdminUser>) => response.ok),
                map((adminUser: HttpResponse<AdminUser>) => adminUser.body)
            );
        }
        return of(new AdminUser());
    }
}

export const adminUserRoute: Routes = [
    {
        path: '',
        component: AdminUserComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.adminUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AdminUserDetailComponent,
        resolve: {
            adminUser: AdminUserResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.adminUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AdminUserUpdateComponent,
        resolve: {
            adminUser: AdminUserResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.adminUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AdminUserUpdateComponent,
        resolve: {
            adminUser: AdminUserResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.adminUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const adminUserPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AdminUserDeletePopupComponent,
        resolve: {
            adminUser: AdminUserResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.adminUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
