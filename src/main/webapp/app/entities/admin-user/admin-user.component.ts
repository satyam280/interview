import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAdminUser } from 'app/shared/model/admin-user.model';
import { AccountService } from 'app/core';
import { AdminUserService } from './admin-user.service';

@Component({
    selector: 'jhi-admin-user',
    templateUrl: './admin-user.component.html'
})
export class AdminUserComponent implements OnInit, OnDestroy {
    adminUsers: IAdminUser[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected adminUserService: AdminUserService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.adminUserService
            .query()
            .pipe(
                filter((res: HttpResponse<IAdminUser[]>) => res.ok),
                map((res: HttpResponse<IAdminUser[]>) => res.body)
            )
            .subscribe(
                (res: IAdminUser[]) => {
                    this.adminUsers = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAdminUsers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAdminUser) {
        return item.id;
    }

    registerChangeInAdminUsers() {
        this.eventSubscriber = this.eventManager.subscribe('adminUserListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
