import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    AdminUserComponent,
    AdminUserDetailComponent,
    AdminUserUpdateComponent,
    AdminUserDeletePopupComponent,
    AdminUserDeleteDialogComponent,
    adminUserRoute,
    adminUserPopupRoute
} from './';

const ENTITY_STATES = [...adminUserRoute, ...adminUserPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AdminUserComponent,
        AdminUserDetailComponent,
        AdminUserUpdateComponent,
        AdminUserDeleteDialogComponent,
        AdminUserDeletePopupComponent
    ],
    entryComponents: [AdminUserComponent, AdminUserUpdateComponent, AdminUserDeleteDialogComponent, AdminUserDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewAdminUserModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
