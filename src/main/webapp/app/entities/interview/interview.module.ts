import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    InterviewComponent,
    InterviewDetailComponent,
    InterviewUpdateComponent,
    InterviewDeletePopupComponent,
    InterviewDeleteDialogComponent,
    interviewRoute,
    interviewPopupRoute
} from './';

const ENTITY_STATES = [...interviewRoute, ...interviewPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        InterviewComponent,
        InterviewDetailComponent,
        InterviewUpdateComponent,
        InterviewDeleteDialogComponent,
        InterviewDeletePopupComponent
    ],
    entryComponents: [InterviewComponent, InterviewUpdateComponent, InterviewDeleteDialogComponent, InterviewDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewInterviewModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
