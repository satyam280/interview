import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInterview } from 'app/shared/model/interview.model';

type EntityResponseType = HttpResponse<IInterview>;
type EntityArrayResponseType = HttpResponse<IInterview[]>;

@Injectable({ providedIn: 'root' })
export class InterviewService {
    public resourceUrl = SERVER_API_URL + 'api/interviewss';

    constructor(protected http: HttpClient) {}

    create(interview: IInterview): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(interview);
        return this.http
            .post<IInterview>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(interview: IInterview): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(interview);
        return this.http
            .put<IInterview>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IInterview>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IInterview[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(interview: IInterview): IInterview {
        const copy: IInterview = Object.assign({}, interview, {
            createdAt: interview.createdAt != null && interview.createdAt.isValid() ? interview.createdAt.toJSON() : null,
            startedAt: interview.startedAt != null && interview.startedAt.isValid() ? interview.startedAt.toJSON() : null,
            finishedAt: interview.finishedAt != null && interview.finishedAt.isValid() ? interview.finishedAt.toJSON() : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
            res.body.startedAt = res.body.startedAt != null ? moment(res.body.startedAt) : null;
            res.body.finishedAt = res.body.finishedAt != null ? moment(res.body.finishedAt) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((interview: IInterview) => {
                interview.createdAt = interview.createdAt != null ? moment(interview.createdAt) : null;
                interview.startedAt = interview.startedAt != null ? moment(interview.startedAt) : null;
                interview.finishedAt = interview.finishedAt != null ? moment(interview.finishedAt) : null;
            });
        }
        return res;
    }
}
