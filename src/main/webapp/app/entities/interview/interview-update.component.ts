import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from './interview.service';
import { ISkill } from 'app/shared/model/skill.model';
import { SkillService } from 'app/entities/skill';

@Component({
    selector: 'jhi-interview-update',
    templateUrl: './interview-update.component.html'
})
export class InterviewUpdateComponent implements OnInit {
    interview: IInterview;
    isSaving: boolean;

    skills: ISkill[];
    createdAt: string;
    startedAt: string;
    finishedAt: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected interviewService: InterviewService,
        protected skillService: SkillService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ interview }) => {
            this.interview = interview;
            this.createdAt = this.interview.createdAt != null ? this.interview.createdAt.format(DATE_TIME_FORMAT) : null;
            this.startedAt = this.interview.startedAt != null ? this.interview.startedAt.format(DATE_TIME_FORMAT) : null;
            this.finishedAt = this.interview.finishedAt != null ? this.interview.finishedAt.format(DATE_TIME_FORMAT) : null;
        });
        this.skillService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISkill[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISkill[]>) => response.body)
            )
            .subscribe((res: ISkill[]) => (this.skills = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.interview.createdAt = this.createdAt != null ? moment(this.createdAt, DATE_TIME_FORMAT) : null;
        this.interview.startedAt = this.startedAt != null ? moment(this.startedAt, DATE_TIME_FORMAT) : null;
        this.interview.finishedAt = this.finishedAt != null ? moment(this.finishedAt, DATE_TIME_FORMAT) : null;
        if (this.interview.id !== undefined) {
            this.subscribeToSaveResponse(this.interviewService.update(this.interview));
        } else {
            this.subscribeToSaveResponse(this.interviewService.create(this.interview));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IInterview>>) {
        result.subscribe((res: HttpResponse<IInterview>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSkillById(index: number, item: ISkill) {
        return item.id;
    }
}
