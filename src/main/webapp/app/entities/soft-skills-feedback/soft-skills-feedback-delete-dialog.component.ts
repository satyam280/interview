import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';
import { SoftSkillsFeedbackService } from './soft-skills-feedback.service';

@Component({
    selector: 'jhi-soft-skills-feedback-delete-dialog',
    templateUrl: './soft-skills-feedback-delete-dialog.component.html'
})
export class SoftSkillsFeedbackDeleteDialogComponent {
    softSkillsFeedback: ISoftSkillsFeedback;

    constructor(
        protected softSkillsFeedbackService: SoftSkillsFeedbackService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.softSkillsFeedbackService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'softSkillsFeedbackListModification',
                content: 'Deleted an softSkillsFeedback'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-soft-skills-feedback-delete-popup',
    template: ''
})
export class SoftSkillsFeedbackDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ softSkillsFeedback }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SoftSkillsFeedbackDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.softSkillsFeedback = softSkillsFeedback;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/soft-skills-feedback', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/soft-skills-feedback', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
