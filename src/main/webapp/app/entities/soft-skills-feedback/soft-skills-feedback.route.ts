import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';
import { SoftSkillsFeedbackService } from './soft-skills-feedback.service';
import { SoftSkillsFeedbackComponent } from './soft-skills-feedback.component';
import { SoftSkillsFeedbackDetailComponent } from './soft-skills-feedback-detail.component';
import { SoftSkillsFeedbackUpdateComponent } from './soft-skills-feedback-update.component';
import { SoftSkillsFeedbackDeletePopupComponent } from './soft-skills-feedback-delete-dialog.component';
import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';

@Injectable({ providedIn: 'root' })
export class SoftSkillsFeedbackResolve implements Resolve<ISoftSkillsFeedback> {
    constructor(private service: SoftSkillsFeedbackService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISoftSkillsFeedback> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<SoftSkillsFeedback>) => response.ok),
                map((softSkillsFeedback: HttpResponse<SoftSkillsFeedback>) => softSkillsFeedback.body)
            );
        }
        return of(new SoftSkillsFeedback());
    }
}

export const softSkillsFeedbackRoute: Routes = [
    {
        path: '',
        component: SoftSkillsFeedbackComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.softSkillsFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: SoftSkillsFeedbackDetailComponent,
        resolve: {
            softSkillsFeedback: SoftSkillsFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.softSkillsFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: SoftSkillsFeedbackUpdateComponent,
        resolve: {
            softSkillsFeedback: SoftSkillsFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.softSkillsFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: SoftSkillsFeedbackUpdateComponent,
        resolve: {
            softSkillsFeedback: SoftSkillsFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.softSkillsFeedback.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const softSkillsFeedbackPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: SoftSkillsFeedbackDeletePopupComponent,
        resolve: {
            softSkillsFeedback: SoftSkillsFeedbackResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.softSkillsFeedback.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
