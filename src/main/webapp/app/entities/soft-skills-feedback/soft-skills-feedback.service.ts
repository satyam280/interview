import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';

type EntityResponseType = HttpResponse<ISoftSkillsFeedback>;
type EntityArrayResponseType = HttpResponse<ISoftSkillsFeedback[]>;

@Injectable({ providedIn: 'root' })
export class SoftSkillsFeedbackService {
    public resourceUrl = SERVER_API_URL + 'api/soft-skills-feedbacks';

    constructor(protected http: HttpClient) {}

    create(softSkillsFeedback: ISoftSkillsFeedback): Observable<EntityResponseType> {
        return this.http.post<ISoftSkillsFeedback>(this.resourceUrl, softSkillsFeedback, { observe: 'response' });
    }

    update(softSkillsFeedback: ISoftSkillsFeedback): Observable<EntityResponseType> {
        return this.http.put<ISoftSkillsFeedback>(this.resourceUrl, softSkillsFeedback, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISoftSkillsFeedback>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISoftSkillsFeedback[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
