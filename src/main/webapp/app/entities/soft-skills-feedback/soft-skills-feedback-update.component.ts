import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';
import { SoftSkillsFeedbackService } from './soft-skills-feedback.service';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview';

@Component({
    selector: 'jhi-soft-skills-feedback-update',
    templateUrl: './soft-skills-feedback-update.component.html'
})
export class SoftSkillsFeedbackUpdateComponent implements OnInit {
    softSkillsFeedback: ISoftSkillsFeedback;
    isSaving: boolean;

    interviews: IInterview[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected softSkillsFeedbackService: SoftSkillsFeedbackService,
        protected interviewService: InterviewService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ softSkillsFeedback }) => {
            this.softSkillsFeedback = softSkillsFeedback;
        });
        this.interviewService
            .query({ filter: 'softskillsfeedback-is-null' })
            .pipe(
                filter((mayBeOk: HttpResponse<IInterview[]>) => mayBeOk.ok),
                map((response: HttpResponse<IInterview[]>) => response.body)
            )
            .subscribe(
                (res: IInterview[]) => {
                    if (!this.softSkillsFeedback.interview || !this.softSkillsFeedback.interview.id) {
                        this.interviews = res;
                    } else {
                        this.interviewService
                            .find(this.softSkillsFeedback.interview.id)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<IInterview>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<IInterview>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: IInterview) => (this.interviews = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.softSkillsFeedback.id !== undefined) {
            this.subscribeToSaveResponse(this.softSkillsFeedbackService.update(this.softSkillsFeedback));
        } else {
            this.subscribeToSaveResponse(this.softSkillsFeedbackService.create(this.softSkillsFeedback));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISoftSkillsFeedback>>) {
        result.subscribe((res: HttpResponse<ISoftSkillsFeedback>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInterviewById(index: number, item: IInterview) {
        return item.id;
    }
}
