import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';

@Component({
    selector: 'jhi-soft-skills-feedback-detail',
    templateUrl: './soft-skills-feedback-detail.component.html'
})
export class SoftSkillsFeedbackDetailComponent implements OnInit {
    softSkillsFeedback: ISoftSkillsFeedback;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ softSkillsFeedback }) => {
            this.softSkillsFeedback = softSkillsFeedback;
        });
    }

    previousState() {
        window.history.back();
    }
}
