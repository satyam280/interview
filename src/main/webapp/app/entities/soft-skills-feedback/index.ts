export * from './soft-skills-feedback.service';
export * from './soft-skills-feedback-update.component';
export * from './soft-skills-feedback-delete-dialog.component';
export * from './soft-skills-feedback-detail.component';
export * from './soft-skills-feedback.component';
export * from './soft-skills-feedback.route';
