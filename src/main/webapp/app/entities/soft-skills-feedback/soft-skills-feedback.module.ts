import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    SoftSkillsFeedbackComponent,
    SoftSkillsFeedbackDetailComponent,
    SoftSkillsFeedbackUpdateComponent,
    SoftSkillsFeedbackDeletePopupComponent,
    SoftSkillsFeedbackDeleteDialogComponent,
    softSkillsFeedbackRoute,
    softSkillsFeedbackPopupRoute
} from './';

const ENTITY_STATES = [...softSkillsFeedbackRoute, ...softSkillsFeedbackPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SoftSkillsFeedbackComponent,
        SoftSkillsFeedbackDetailComponent,
        SoftSkillsFeedbackUpdateComponent,
        SoftSkillsFeedbackDeleteDialogComponent,
        SoftSkillsFeedbackDeletePopupComponent
    ],
    entryComponents: [
        SoftSkillsFeedbackComponent,
        SoftSkillsFeedbackUpdateComponent,
        SoftSkillsFeedbackDeleteDialogComponent,
        SoftSkillsFeedbackDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewSoftSkillsFeedbackModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
