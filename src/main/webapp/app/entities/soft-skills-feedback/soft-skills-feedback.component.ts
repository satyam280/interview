import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISoftSkillsFeedback } from 'app/shared/model/soft-skills-feedback.model';
import { AccountService } from 'app/core';
import { SoftSkillsFeedbackService } from './soft-skills-feedback.service';

@Component({
    selector: 'jhi-soft-skills-feedback',
    templateUrl: './soft-skills-feedback.component.html'
})
export class SoftSkillsFeedbackComponent implements OnInit, OnDestroy {
    softSkillsFeedbacks: ISoftSkillsFeedback[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected softSkillsFeedbackService: SoftSkillsFeedbackService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.softSkillsFeedbackService
            .query()
            .pipe(
                filter((res: HttpResponse<ISoftSkillsFeedback[]>) => res.ok),
                map((res: HttpResponse<ISoftSkillsFeedback[]>) => res.body)
            )
            .subscribe(
                (res: ISoftSkillsFeedback[]) => {
                    this.softSkillsFeedbacks = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSoftSkillsFeedbacks();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISoftSkillsFeedback) {
        return item.id;
    }

    registerChangeInSoftSkillsFeedbacks() {
        this.eventSubscriber = this.eventManager.subscribe('softSkillsFeedbackListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
