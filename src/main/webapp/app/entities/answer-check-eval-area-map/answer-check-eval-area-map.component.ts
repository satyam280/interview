import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';
import { AccountService } from 'app/core';
import { AnswerCheckEvalAreaMapService } from './answer-check-eval-area-map.service';

@Component({
    selector: 'jhi-answer-check-eval-area-map',
    templateUrl: './answer-check-eval-area-map.component.html'
})
export class AnswerCheckEvalAreaMapComponent implements OnInit, OnDestroy {
    answerCheckEvalAreaMaps: IAnswerCheckEvalAreaMap[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected answerCheckEvalAreaMapService: AnswerCheckEvalAreaMapService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.answerCheckEvalAreaMapService
            .query()
            .pipe(
                filter((res: HttpResponse<IAnswerCheckEvalAreaMap[]>) => res.ok),
                map((res: HttpResponse<IAnswerCheckEvalAreaMap[]>) => res.body)
            )
            .subscribe(
                (res: IAnswerCheckEvalAreaMap[]) => {
                    this.answerCheckEvalAreaMaps = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAnswerCheckEvalAreaMaps();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAnswerCheckEvalAreaMap) {
        return item.id;
    }

    registerChangeInAnswerCheckEvalAreaMaps() {
        this.eventSubscriber = this.eventManager.subscribe('answerCheckEvalAreaMapListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
