import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';
import { AnswerCheckEvalAreaMapService } from './answer-check-eval-area-map.service';

@Component({
    selector: 'jhi-answer-check-eval-area-map-delete-dialog',
    templateUrl: './answer-check-eval-area-map-delete-dialog.component.html'
})
export class AnswerCheckEvalAreaMapDeleteDialogComponent {
    answerCheckEvalAreaMap: IAnswerCheckEvalAreaMap;

    constructor(
        protected answerCheckEvalAreaMapService: AnswerCheckEvalAreaMapService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.answerCheckEvalAreaMapService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'answerCheckEvalAreaMapListModification',
                content: 'Deleted an answerCheckEvalAreaMap'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-answer-check-eval-area-map-delete-popup',
    template: ''
})
export class AnswerCheckEvalAreaMapDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answerCheckEvalAreaMap }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AnswerCheckEvalAreaMapDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.answerCheckEvalAreaMap = answerCheckEvalAreaMap;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/answer-check-eval-area-map', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/answer-check-eval-area-map', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
