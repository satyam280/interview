import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';
import { AnswerCheckEvalAreaMapService } from './answer-check-eval-area-map.service';
import { AnswerCheckEvalAreaMapComponent } from './answer-check-eval-area-map.component';
import { AnswerCheckEvalAreaMapDetailComponent } from './answer-check-eval-area-map-detail.component';
import { AnswerCheckEvalAreaMapUpdateComponent } from './answer-check-eval-area-map-update.component';
import { AnswerCheckEvalAreaMapDeletePopupComponent } from './answer-check-eval-area-map-delete-dialog.component';
import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';

@Injectable({ providedIn: 'root' })
export class AnswerCheckEvalAreaMapResolve implements Resolve<IAnswerCheckEvalAreaMap> {
    constructor(private service: AnswerCheckEvalAreaMapService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAnswerCheckEvalAreaMap> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<AnswerCheckEvalAreaMap>) => response.ok),
                map((answerCheckEvalAreaMap: HttpResponse<AnswerCheckEvalAreaMap>) => answerCheckEvalAreaMap.body)
            );
        }
        return of(new AnswerCheckEvalAreaMap());
    }
}

export const answerCheckEvalAreaMapRoute: Routes = [
    {
        path: '',
        component: AnswerCheckEvalAreaMapComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckEvalAreaMap.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AnswerCheckEvalAreaMapDetailComponent,
        resolve: {
            answerCheckEvalAreaMap: AnswerCheckEvalAreaMapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckEvalAreaMap.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AnswerCheckEvalAreaMapUpdateComponent,
        resolve: {
            answerCheckEvalAreaMap: AnswerCheckEvalAreaMapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckEvalAreaMap.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AnswerCheckEvalAreaMapUpdateComponent,
        resolve: {
            answerCheckEvalAreaMap: AnswerCheckEvalAreaMapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckEvalAreaMap.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const answerCheckEvalAreaMapPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AnswerCheckEvalAreaMapDeletePopupComponent,
        resolve: {
            answerCheckEvalAreaMap: AnswerCheckEvalAreaMapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'interviewApp.answerCheckEvalAreaMap.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
