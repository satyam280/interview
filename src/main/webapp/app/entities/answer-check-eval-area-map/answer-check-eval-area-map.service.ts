import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';

type EntityResponseType = HttpResponse<IAnswerCheckEvalAreaMap>;
type EntityArrayResponseType = HttpResponse<IAnswerCheckEvalAreaMap[]>;

@Injectable({ providedIn: 'root' })
export class AnswerCheckEvalAreaMapService {
    public resourceUrl = SERVER_API_URL + 'api/answer-check-eval-area-maps';

    constructor(protected http: HttpClient) {}

    create(answerCheckEvalAreaMap: IAnswerCheckEvalAreaMap): Observable<EntityResponseType> {
        return this.http.post<IAnswerCheckEvalAreaMap>(this.resourceUrl, answerCheckEvalAreaMap, { observe: 'response' });
    }

    update(answerCheckEvalAreaMap: IAnswerCheckEvalAreaMap): Observable<EntityResponseType> {
        return this.http.put<IAnswerCheckEvalAreaMap>(this.resourceUrl, answerCheckEvalAreaMap, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAnswerCheckEvalAreaMap>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAnswerCheckEvalAreaMap[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
