import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';

@Component({
    selector: 'jhi-answer-check-eval-area-map-detail',
    templateUrl: './answer-check-eval-area-map-detail.component.html'
})
export class AnswerCheckEvalAreaMapDetailComponent implements OnInit {
    answerCheckEvalAreaMap: IAnswerCheckEvalAreaMap;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answerCheckEvalAreaMap }) => {
            this.answerCheckEvalAreaMap = answerCheckEvalAreaMap;
        });
    }

    previousState() {
        window.history.back();
    }
}
