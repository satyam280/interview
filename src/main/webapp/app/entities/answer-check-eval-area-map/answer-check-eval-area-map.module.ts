import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SharedModule } from 'app/shared';
import {
    AnswerCheckEvalAreaMapComponent,
    AnswerCheckEvalAreaMapDetailComponent,
    AnswerCheckEvalAreaMapUpdateComponent,
    AnswerCheckEvalAreaMapDeletePopupComponent,
    AnswerCheckEvalAreaMapDeleteDialogComponent,
    answerCheckEvalAreaMapRoute,
    answerCheckEvalAreaMapPopupRoute
} from './';

const ENTITY_STATES = [...answerCheckEvalAreaMapRoute, ...answerCheckEvalAreaMapPopupRoute];

@NgModule({
    imports: [SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AnswerCheckEvalAreaMapComponent,
        AnswerCheckEvalAreaMapDetailComponent,
        AnswerCheckEvalAreaMapUpdateComponent,
        AnswerCheckEvalAreaMapDeleteDialogComponent,
        AnswerCheckEvalAreaMapDeletePopupComponent
    ],
    entryComponents: [
        AnswerCheckEvalAreaMapComponent,
        AnswerCheckEvalAreaMapUpdateComponent,
        AnswerCheckEvalAreaMapDeleteDialogComponent,
        AnswerCheckEvalAreaMapDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterviewAnswerCheckEvalAreaMapModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
