import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAnswerCheckEvalAreaMap } from 'app/shared/model/answer-check-eval-area-map.model';
import { AnswerCheckEvalAreaMapService } from './answer-check-eval-area-map.service';
import { IAnswerCheck } from 'app/shared/model/answer-check.model';
import { AnswerCheckService } from 'app/entities/answer-check';
import { IEvaluationArea } from 'app/shared/model/evaluation-area.model';
import { EvaluationAreaService } from 'app/entities/evaluation-area';

@Component({
    selector: 'jhi-answer-check-eval-area-map-update',
    templateUrl: './answer-check-eval-area-map-update.component.html'
})
export class AnswerCheckEvalAreaMapUpdateComponent implements OnInit {
    answerCheckEvalAreaMap: IAnswerCheckEvalAreaMap;
    isSaving: boolean;

    answerchecks: IAnswerCheck[];

    evaluationareas: IEvaluationArea[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected answerCheckEvalAreaMapService: AnswerCheckEvalAreaMapService,
        protected answerCheckService: AnswerCheckService,
        protected evaluationAreaService: EvaluationAreaService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ answerCheckEvalAreaMap }) => {
            this.answerCheckEvalAreaMap = answerCheckEvalAreaMap;
        });
        this.answerCheckService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IAnswerCheck[]>) => mayBeOk.ok),
                map((response: HttpResponse<IAnswerCheck[]>) => response.body)
            )
            .subscribe((res: IAnswerCheck[]) => (this.answerchecks = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.evaluationAreaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEvaluationArea[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEvaluationArea[]>) => response.body)
            )
            .subscribe((res: IEvaluationArea[]) => (this.evaluationareas = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.answerCheckEvalAreaMap.id !== undefined) {
            this.subscribeToSaveResponse(this.answerCheckEvalAreaMapService.update(this.answerCheckEvalAreaMap));
        } else {
            this.subscribeToSaveResponse(this.answerCheckEvalAreaMapService.create(this.answerCheckEvalAreaMap));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnswerCheckEvalAreaMap>>) {
        result.subscribe(
            (res: HttpResponse<IAnswerCheckEvalAreaMap>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAnswerCheckById(index: number, item: IAnswerCheck) {
        return item.id;
    }

    trackEvaluationAreaById(index: number, item: IEvaluationArea) {
        return item.id;
    }
}
